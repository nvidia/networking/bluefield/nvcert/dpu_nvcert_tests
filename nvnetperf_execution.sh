#!/bin/bash

# Array to hold the test_execution_list and their names
test_execution_list=(
    "./tests/traffic_tests/rdma_test.py,RDMA OVS,rdma-ovs"
    "./tests/traffic_tests/tcp_test.py,tcp-ovs,TCP OVS,tcp-ovs"
    "./tests/traffic_tests/gpu_direct_rdma_test.py,GPU Direct RDMA (GDR),gdr"
)

# Array to store exit statuses
declare -A exit_statuses

start_time=$(date +%s)
current_date=$(date +"%Y-%m-%d_%H-%M-%S")
logs_dir_root="./logs"
current_logs_dir=$logs_dir_root/log_$current_date
# Default file path if -i or --inventory is not provided
inventory_file_path="hosts.yaml"


if [ ! -d "$logs_dir_root" ]; then
  mkdir -p "$logs_dir_root"
  chmod 666 -R $logs_dir_root
fi
mkdir -p "$current_logs_dir"

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
  case "$1" in
    -i)
      if [[ -n "$2" && "$2" != -* ]]; then
        inventory_file_path="$2"
        shift 2
      else
        echo "Error: -i requires a file path."
        exit 1
      fi
      ;;
    --inventory)
      if [[ -n "$2" && "$2" != -* ]]; then
        inventory_file_path="$2"
        shift 2
      else
        echo "Error: --inventory requires a file path."
        exit 1
      fi
      ;;
    *)
      echo "Invalid option: $1"
      exit 1
      ;;
  esac
done

# Check if the file exists
if [[ -f "$inventory_file_path" ]]; then
  # Copy the file to the logs directory
  cp "$inventory_file_path" $current_logs_dir
  echo "Inventory file \"$inventory_file_path\" copied to '$current_logs_dir' directory."
else
  echo "Inventory file does not exist: $inventory_file_path"
  exit 1
fi

# Function to calculate and format elapsed time
calculate_elapsed_time() {
	local start_time=$1
	local end_time=$2
	local elapsed_time=$((end_time - start_time))

	local hours=$((elapsed_time / 3600))
	local minutes=$(( (elapsed_time % 3600) / 60 ))
	local seconds=$((elapsed_time % 60))

	printf "%d:%02d:%02d\n" $hours $minutes $seconds
}

# Custom logging function
log() {
    local LEVEL=$1
    shift
    local MESSAGE="$@"
    local TIMESTAMP=$(date +"%Y-%m-%d %H:%M:%S")

    if [ -n "$MESSAGE" ]; then
        # Output to terminal
        printf "%s [%s] %s\n" "$TIMESTAMP" "$LEVEL" "$MESSAGE"
        
        # Output to log file
        printf "%s [%s] %s\n" "$TIMESTAMP" "$LEVEL" "$MESSAGE" >> "$current_logs_dir/summary.log"

        # Output to syslog
        logger -p "user.${LEVEL,,}" "$MESSAGE"
    else
        # Output a blank line to terminal and log file
        printf "\n"
        printf "\n" >> "$current_logs_dir/summary.log"
    fi
}

# Run each test and capture the exit status
for entry in "${test_execution_list[@]}"; do
    # Split the entry into script filename, descriptive name, and log filename
    script=$(echo $entry | cut -d',' -f1)
    test_name=$(echo $entry | cut -d',' -f2)
    log_filename=$(echo $entry | cut -d',' -f3)
    log_file=$current_logs_dir/${log_filename}.log
	
	script_start_time=$(date +%s)

	# this is for a newer version with the logs.
	command_line="time $script "$@" -i $inventory_file_path -f $current_logs_dir/${log_filename}_verbose.log"
    
    log ""
    log "INFO" "******************************************"
    log "INFO" "Starting test $test_name."
    log "INFO" "Executing $command_line."
    log "INFO" "Log file: $log_file"
    log ""
 
    # Execute the script and time it, while sending the outputs to both the terminal and the log file
    { $command_line ; } 2>&1 | tee $log_file
    status=${PIPESTATUS[0]}
    exit_statuses[$entry]=$status
    
    # Output the exit status to the log file
    echo "Exit status: $status" >> "$log_file"
	
    if [ $status -ne 0 ]; then
        log "ERROR" "Test \"$test_name\" exited with status $status"
    else
        log "INFO" "Test \"$test_name\" passed"
    fi
	script_end_time=$(date +%s)
	elapsed_time_formatted=$(calculate_elapsed_time $script_start_time $script_end_time)
	
	log "INFO" "Test time: $elapsed_time_formatted"
    log "INFO" "******************************************"
    log ""

done

chmod 777 -R $current_logs_dir

# Calculate the elapsed time
end_time=$(date +%s)
elapsed_time_formatted=$(calculate_elapsed_time $start_time $end_time)

log ""
log "INFO" "============ Execution summary ============"

for entry in "${test_execution_list[@]}"; do
    test_name=$(echo $entry | cut -d',' -f2)
    
    if [ ${exit_statuses[$entry]} -ne 0 ]; then
        log "ERROR" "Test \"$test_name\" failed with status ${exit_statuses[$entry]}"
    else
        log "INFO" "Test \"$test_name\" passed"
    fi
done

log "INFO" "Elapsed time: $elapsed_time_formatted"
log "INFO" "Log files and summary located at $current_logs_dir/"
log "INFO" "==========================================="
