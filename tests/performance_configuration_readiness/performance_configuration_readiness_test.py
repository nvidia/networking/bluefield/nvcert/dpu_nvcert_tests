#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

from pathlib import Path
import sys
import os

from tests.performance_configuration_readiness.wrappers import host_pci_check, system_hw_inventory_check, networking_config_check, boot_bios_check, gpu_hw_inventory_check, system_health_check
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    from tabulate import tabulate
    from common.basic_classes import nvnetperf_base_class as nbc
    from tests.performance_configuration_readiness.wrappers import gpu_hw_inventory_check
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class PerformanceConfigurationReadiness(nbc.NVNetPerfBaseClass):
    """
    Performance configuration readiness.
        1. Run performance configurations checks such as: Host PCI Check, System HW Inventory check, Boot/BIOS Settings check...etc.
        2. Return detailed performance status about the setup before running tests.
    """
    test_suite = {
        "selected_tuning_checks": None
    }

    hosts_list = []
    def __init__(self):
        """
        Initializes the performance configuration readiness with two hosts.

        """
        super().__init__()
        self.logger.info("Beginning Performance Configuration Readiness!")
        self.hosts_list = []
        self.test_name = "performance_config_readiness"
        self.test_run_name = self.test_name
        self.title = "Summary: Performance Configuration Readiness Results"

    def args_test(self):
        super().args_test()
        # TODO add validation. and modify test names.
        self.parser.add_argument("--selected_tuning_checks", default=None, type=str,
                        help="Performance Configurations Readiness: A list of checks the user wants to run  (splited by a comma). If None, all checks are run.\
                            Tuning Checks: host_pci_check, system_hw_inventory_check, networking_config_check, boot_bios_check, gpu_hw_inventory_check, system_health_check\
                                ex: --selected_tuning_checks 'host_pci_check, system_hw_inventory_check'")

    def args_parsed(self):
        super().args_parsed()
        self.selected_tuning_checks = self.args.selected_tuning_checks


    def test_setup(self):
        super().test_setup(loopback_supported=True, ib_supported=True)
        if self.enable_passwordless_access:
            self.server_handler.setup_host_ssh_keys(self.node_group_dict)
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                self.hosts_list.append(host)

    def test_logic(self):
        self.run_performance_checks(self.selected_tuning_checks)
        return self.test_status

    def run_performance_checks(self, selected_checks=None):
        """
        Run Performance configuration readiness on hosts and DPUs (if applicable).

        This method performs PCI checks and analyzes system configurations
        to ensure the system is optimized from a performance perspective.
        If any performance issues are detected (e.g., incorrect PCI settings),
        it will mark the system as non-optimized and set the exit status accordingly.

        Parameters:
            selected_checks (str, optional): A list of checks the user wants to run. If None, all checks are run.
                Example: 'host_pci_check, system_hw_inventory_chec'

        Returns:
            test_status (ExitStatus): Updates the global test_status based on test results.
        """
        fail_count = 0

        # Define all the available checks
        all_checks = {
            "host_pci_check": lambda: host_pci_check.host_pci_check(self.hosts_list),
            "system_hw_inventory_check": lambda: system_hw_inventory_check.check_cable_phy_matching(self.hosts_list),
            "networking_config_check": lambda: networking_config_check.cache_memory_check_test(self.hosts_list),
            "boot_bios_check": lambda: boot_bios_check.check_bios_config_test(self.hosts_list),
            "gpu_hw_inventory_check": lambda: gpu_hw_inventory_check.check_gpu_freq_clocks(self.hosts_list),
            "system_health_check": lambda: system_health_check.check_os_status(self.hosts_list),
        }

        # If user did not specify any checks, run all by default
        if selected_checks is None:
            selected_checks = list(all_checks.keys())
        else:
            # Split the comma-separated string into a list of checks
            selected_checks = [check.strip() for check in selected_checks.split(",")]

        # Perform the selected checks and gather results
        check_results = {check_name: all_checks[check_name]() for check_name in selected_checks if check_name in all_checks}

        # Loop through each check result and log failures if found
        for check_name, result in check_results.items():
            if not result[1]:  # If test result is False, log failure and update exit_code
                self.logger.error(f"{check_name} failed during performance pre-test static analysis.")
                self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value

        # Log the details of each test
        terminal_width = os.get_terminal_size().columns
        self.logger.info(f"\n\n{self.title.center(int(terminal_width/2))} \n")
        for check_name, result in check_results.items():
            if result[0]:
                test_data = f"{check_name} Data:"
                self.logger.info(f"\n{test_data.center(int(terminal_width/2))}\n{tabulate(result[0], headers='keys', tablefmt='orgtbl')}\n")
                if len(result[0]) > 1:
                    for res in result[0]:
                        status = True if res["Status"] == constants.TestStatus.PASSED.value else False
                        threshold = {
                            "expected": res["Expected"],
                            "info": res["Test Notes"]
                        }
                        res.pop("Expected")
                        res.pop("Test Notes")
                        res.pop("Status")
                        self.subtests_list = self.json_helper.create_json_subtest(check_name, res, threshold, status)
            else:
                fail_count += 1
                self.logger.error(f"\n{check_name} failed or returned no data!")

        if fail_count > 0:
            self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
        else:
            self.test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value

        # Handle GPU-specific logging if it's part of the selected checks
        if 'GPU Frequency Clocks' in selected_checks and not check_results['GPU Frequency Clocks'][0]:
            self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
            self.logger.error(f"\nError: No Available GPUs on the system!")

    def set_variables_from_file(self, inventory, params):
        super().set_variables_from_file(inventory, params)
        self.selected_tuning_checks = params["tests"][self.test_run_name]["selected_tuning_checks"]

if __name__ == "__main__":
    test = PerformanceConfigurationReadiness()
    test.args_execute()
    exit(test.execute())