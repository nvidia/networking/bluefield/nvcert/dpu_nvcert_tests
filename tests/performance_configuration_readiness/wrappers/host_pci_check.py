#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

"""
PCI Tunning Instructions Check part of Performance configuration readiness
1. Run PCI check for type, MaxReadReq and Relax ordering as part of performance configurations checks.
2. Return detailed performance status about the setup PCI before running tests.
"""

import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import logging
    from tabulate import tabulate
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


# Global variables
logger = logging.getLogger("nvnetperf")


logger.info("Beginning PCI Tunning Instructions Check part of Performance configuration readiness!")

def host_pci_check(hosts_list):
    """
    Perform PCI tunning instructions check on both hosts and log the results.

    Returns:
        bool: True if all checks pass, False otherwise.
    """
    TEST_INFO = "Checking PCI settings like PCI Type, MaxReadReq, and Relaxed Ordering\nEnsures optimal bandwidth and improved overall system performance."
    results = []

    logger.info("Beginning PCI Tunning Instructions Check part of Performance configuration readiness!")
    for host in hosts_list:
        for pci in host.pcie_address:
            extra_config = ""
            pci_type_data = get_pci_type(host, pci)
            pci_max_read_data = get_max_read_req_value(host, pci)
            pci_max_payload_data = get_max_payload_size(host, pci)
            pci_relax_ordering_data = get_relax_ordering_value(host, pci)
            pci_ats_data = get_ats_status(host, pci)
            pci_max_acc_out_read_data = get_max_acc_value(host, pci)
            pci_aspm_data = get_aspm_status(host, pci)
            pci_acsctl_data = get_acsctl_status(host, pci)

            status = pci_type_data[1] and pci_max_read_data[1] and pci_max_payload_data[1] and pci_relax_ordering_data[1]  and pci_ats_data[1] and pci_max_acc_out_read_data[1] and pci_aspm_data[1] and pci_acsctl_data[1]
            if not status:
                if not pci_type_data[1]:
                    extra_config = f"{extra_config}\nPCI GEN is not correct for the system card."
                if not pci_max_read_data[1]:
                    extra_config = f"{extra_config}\nSet MaxReadReq to the correct value using `setpci -s <device-pci> 68.w=5BC`"
                if not pci_max_payload_data[1]:
                    extra_config = f"{extra_config}\nMaxPayload <256 indicates warning, Changes needed from BIOS."
                if not pci_relax_ordering_data[1]:
                    extra_config = f"{extra_config}\nRelax Ordering not configured"
                if not pci_ats_data[1]:
                    extra_config = f"{extra_config}\nATS is not Enabled"
                if not pci_max_acc_out_read_data[1]:
                    extra_config = f"{extra_config}\nMaxAccOutRead not match the expected 0x0"
                if not pci_aspm_data[1]:
                    extra_config = f"{extra_config}\nASPM is not Disabled"
                if not pci_acsctl_data[1]:
                    extra_config = f"{extra_config}\ACSCtl is not Disabled"
            results.append({
                "Host": host.oob_address, 
                "PCI Address": pci, 
                "PCI Type": pci_type_data[0],
                "PCI MaxReadReq Value": pci_max_read_data[0],
                "PCI MaxPayload Size": pci_max_payload_data[0],
                "PCI Relax Ordering": pci_relax_ordering_data[0],
                "PCI ATS Status": pci_ats_data[0],
                "PCI MaxAccOutRead": pci_max_acc_out_read_data[0],
                "PCI ASPM Status": pci_aspm_data[0],
                "PCI ACSCtl Status": pci_acsctl_data[0],
                "Expected": "PCI Gen match Card Gen, MaxReadReq set to MAX(4096), MaxPayload is system dependent >=256, Relax Ordering is Enabled.\n\
                    ATS Enabled, MaxAccOutRead value= 0x0 if AdvancedPciSettings is Enabled else None, ASPM is Disabled, ACSCtl is Disabled",
                "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value,
                "Test Notes": f"{TEST_INFO}{extra_config}"
            })

    if results:
        logger.info(f"\nPCI Check Data:\n{tabulate(results, headers='keys', tablefmt='orgtbl')}\n")
        if all(r['Status'] == constants.TestStatus.PASSED.value for r in results):
            return results, True
        else:
            logger.error(f"Error: PCI Data are not good from performance prespective.")
            return results, False
    else:
        logger.error(f"Error: Failed to get PCI Data")
        return results, False

def get_pci_type(host, pci):
    """
    Get PCI type and log the results.

    Args:
        host: Host data.
        pci: PCI address to check.

    Returns:
        tuple: PCI type and bool status (True for success, False for failure).
    """
    TEST_INFO = f"PCI Gen Type: affect data transfere rates between the CPU, memory, and PCI devices.\nPCIe Gen 3: Offers up to 8 GT/s, PCIe Gen 4: Offers up to 16 GT/s, PCIe Gen 5: Offers up to 32 GT/s (Gigatransfers per second) per lane.",
    pci_data = []
    status = False
    pci_gen_type = host.sudo(f"mlxlink -d {pci} --port_type pcie | grep -i 'speed'").strip().split(":")[1].strip()
    pci_gen_type = pci_gen_type.split("(")[0].strip() if "(" in pci_gen_type else pci_gen_type
    card_gen = get_pci_card_type(host, pci)
    if pci_gen_type and card_gen:
        if card_gen.lower() in pci_gen_type.lower():
            status = True
    else:
        status = False

    pci_data.append({"PCI Address": pci, "PCI Gen Type": pci_gen_type, "Card Gen": card_gen, "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})
    if status:
        logger.info(f"\nHost - {host.oob_address} PCI Data:\n{tabulate(pci_data, headers='keys', tablefmt='orgtbl')}\n")
    else:
        logger.error(f"\nError: Failed to get PCI Data for Host - {host.oob_address}:\n{tabulate(pci_data, headers='keys', tablefmt='orgtbl')}\n")
    return pci_gen_type, status

def get_pci_card_type(host, pci):
    """
    Determine the PCI card generation type based on the card version.

    Args:
        host: The host object to execute commands on.
        pci: The PCI address for which the card type is being checked.

    Returns:
        str: The generation type (Gen4 or Gen5) or an empty string if not found.
    """
    card_type = host.sudo(f"mst status -v | grep -i {pci} | awk '{{print $1}}'").strip().split("(")[0]

    # Define a dictionary for mapping card types and versions
    card_mapping = {
        constants.CardVersion.CONNECTX.value: {
            "6": constants.CardGenType.CX6.value,
            "7": constants.CardGenType.CX7.value,
            "8": constants.CardGenType.CX8.value
        },
        constants.CardVersion.BLUEFIELD.value: {
            "2": constants.CardGenType.BF2.value,
            "3": constants.CardGenType.BF3.value
        }
    }
    # Ensure card_type is a string
    if not isinstance(card_type, str) or not card_type:
        return None

    # Loop through the dictionary to determine the card generation
    for version, gen_mapping in card_mapping.items():
         # Convert version to a string if it's a tuple
        if isinstance(version, tuple):
            version = str(version[0])  # Take the first element of the tuple

        if version in str(card_type).lower():
            for key, gen_type in gen_mapping.items():
                if isinstance(gen_type, tuple):
                    gen_type = str(gen_type[0])
                if key in card_type:
                    return gen_type
    # Return None if no card type found
    return None

def get_max_read_req_value(host, pci):
    """
    Get PCI MaxReadReq value and log the results.

    Args:
        host: Host data.
        pci: PCI address to check.

    Returns:
        tuple: MaxReadReq value and bool status (True for success, False for failure).
    """
    TEST_INFO = f"MaxReadReq: defines the maximum size of a single read request that a PCIe device can make to the host memory.\n Set it using `setpci -s <pci> CAP_EXP+08.w=5000:f000`",

    pci_data = []
    expected = 4096 
    max_read_value = host.sudo(f"lspci -vvv -s {pci} | grep -i 'MaxReadReq'").split("MaxReadReq")[1].split("bytes")[0].strip()
    status = True if int(max_read_value) == expected else False
    if not status:
        logger.error(f"Host - {host.oob_address} MaxRead Results does not match the critiria")
    pci_data.append({"PCI Address": pci, "MaxReadReq": max_read_value, "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})

    logger.info(f"Host - {host.oob_address} MaxReadReq Result:\n{tabulate(pci_data, headers='keys', tablefmt='orgtbl')}")

    return max_read_value, status

def get_max_payload_size(host, pci):
    """
    Get PCI MaxPayload value and log the results.

    Args:
        host: Host data.
        pci: PCI address to check.

    Returns:
        tuple: MaxPayload value and bool status (True for success, False for failure).
    """
    TEST_INFO = f"MaxPayload: defines the maximum size of a single payload size that a PCIe device can make.\n From nic side 512, but it's system dependent, some systems support only 256.",

    pci_data = []
    min_expected = 256 
    max_payload_size = host.sudo(f"lspci -vvv -s {pci} | grep -i 'MaxPayload'").split("MaxPayload")[1].split("bytes")[0].strip()
    status = True if int(max_payload_size) > min_expected else False
    if not status:
        logger.error(f"Host - {host.oob_address} MaxPayload Size < 256")
    pci_data.append({"PCI Address": pci, "MaxPayload": max_payload_size, "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})

    logger.info(f"Host - {host.oob_address} MaxPayload Result:\n{tabulate(pci_data, headers='keys', tablefmt='orgtbl')}")

    return max_payload_size, status

def get_relax_ordering_value(host, pci):
    """
    Get PCI Relax Ordering value and log the results.

    Args:
        host: Host data.
        pci: PCI address to check.

    Returns:
        tuple: Relax Ordering value and bolol status (True for success, False for failure).
    """
    TEST_INFO = "Relaxed Ordering: allows PCIe devices to reorder transactions instead of strictly maintaining the order in which they were issued.",

    pci_data = []
    relax_ordering_value = bin(int(host.sudo(f"setpci -s {pci} 68.w").strip(), 16))[2:]
    value = "Disabled"
    status = False
    if relax_ordering_value and int(relax_ordering_value) != 0:
        if int(relax_ordering_value[-5]) == 1:
            value = "Enabeled"
            status = True
        elif relax_ordering_value[-5] == 0:
            TEST_INFO = f"{TEST_INFO}\nSet it using: `setpci -s $PCI_FUNCTION 68.w=5BC`"
            value = "Disabled"
            status = False
    else:
        TEST_INFO = f"{TEST_INFO}\nSetup is not showing results for Relax Ordering"
        value = "None"
    pci_data.append({"PCI Address": pci, "Relax Ordering": value, "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})

    logger.info(f"Host - {host.oob_address} PCI Relax Ordering Data:\n{tabulate(pci_data, headers='keys', tablefmt='orgtbl')}")
    return value, status


def get_ats_status(host, pci):
    """
    Get ATS Status value and log the results.

    Args:
        host: Host data.
        pci: PCI address to check.

    """
    TEST_INFO = f"ATS improves performance by allowing devices to cache address translations."
    ats_status = "Enabled"
    status = constants.TestStatus.PASSED.value
    pci_data = []
    status = True if "True" in host.sudo(f"mlxconfig  -d {pci} q | grep ATS_ENABLED | awk '{{print $NF}}'").strip() else False

    if not status:
        logger.error(f"Host - {host.oob_address} ATS is disabled")
        ats_status = "Disabled"

    pci_data.append({"PCI Address": pci, "ATS": ats_status, "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})
    logger.info(f"Host - {host.oob_address} ATS Status:\n{tabulate(pci_data, headers='keys', tablefmt='orgtbl')}")

    return  ats_status, status

def get_max_acc_value(host, pci):
    """
    Get MAX_ACC_OUT_READ value

    Args:
        host: Host data.
        pci: PCI address to check.

    """
    TEST_INFO = f"MAX_ACC_OUT_READ, Default is 0x0 If Advanced_PCI_SETTINGS is Enabled"
    acc_expected = "0x0"
    acc_value = "None"
    status = True
    data = []
    adv_pci_settings = "Enabled" if "True" in host.sudo(f"mlxconfig  -d {pci} q | grep ADVANCED_PCI_SETTINGS | awk '{{print $NF}}'").strip() else "Disabled"
    if adv_pci_settings == "Enabled":
        acc_value = host.sudo(f"mlxconfig  -d {pci} q | grep MAX_ACC_OUT_READ | awk '{{print $NF}}'").strip()
        if acc_value:
            if acc_value == acc_expected:
                status = True 
            else:
                logger.error(f"Host - {host.oob_address} ACC is not matching the expectation!")
                status = False
    else:
        acc_value = "None"
        status = True

    data.append({"PCI Address": pci, "AdvancedPciSettings": adv_pci_settings, "MaxAccOutRead": acc_value, "Status":  constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})
    logger.info(f"Host - {host.oob_address} ACC Results\n{tabulate(data, headers='keys', tablefmt='orgtbl')}")

    return acc_value, status


def get_aspm_status(host, pci):
    """
    Get ASPM Status and log the results.

    Args:
        host: Host data.
        pci: PCI address to check.
    

    """
    TEST_INFO = f"ASPM: Active State Power Management (power-saving features)"

    data = []
    expected = "Disabled" 

    aspm_status = host.sudo(f"lspci -vvv -s {pci} | grep -i 'LnkCtl' | grep -i 'ASPM'").split("ASPM")[1].split(";")[0].strip()
    status = True if aspm_status == expected else False
    if not status:
        logger.error(f"Host - {host.oob_address} ASPM is Enabled")
    data.append({"PCI Address": pci, "ASPM Status": aspm_status, "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})

    logger.info(f"Host - {host.oob_address} ASPM Result:\n{tabulate(data, headers='keys', tablefmt='orgtbl')}")

    return aspm_status, status

def get_acsctl_status(host, pci):
    """
    Get ACSCtl Status and log the results.

    Args:
        host: Host data.
        pci: PCI address to check.
    

    """
    TEST_INFO = f"ACSCtl need to be Disabled"

    data = []

    acsctl_res = host.sudo(f"lspci -vvv -s {pci} | grep -i 'ACSCtl'").strip()
    if "SrcValid-" in acsctl_res:
        acsctl_status = "Disabled"
        status = True
    else:
        status = False
        acsctl_status = "Enabled"
    if not status:
        logger.error(f"Host - {host.oob_address} ACSCtl is Enabled")
    data.append({"PCI Address": pci, "ACSCtl Status": acsctl_status, "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})

    logger.info(f"Host - {host.oob_address} ASPM Result:\n{tabulate(data, headers='keys', tablefmt='orgtbl')}")

    return acsctl_status, status


