#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

"""
Boot/BIOS Check part of Performance configuration readiness
1. Check BOOT GRUB CMDLine and BIOS Settings such as CPU Power Managemnt.
2. Return detailed performance status about the setup BIOS Settings before running tests.
"""

import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")


try:
    import logging
    from tabulate import tabulate
    from tests.performance_configuration_readiness.wrappers import host_pci_check as pci_tune
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


# Global variables
logger = logging.getLogger("nvnetperf")

def check_bios_config_test(hosts_list):
    """
    Perform Boot/BIOS check on both hosts and log the results.

    Args:
        hosts_list (list): List of host data for running the checks.
    
        Returns:
        tuple: A tuple of (list, bool), where the list contains the test results and
        the bool indicates if all checks passed.
    """
    results = []

    logger.info("Beginning of Boot/BIOS Check of Performance configuration readiness!\n\
                    It tests the following:\n\
                    BIOS Boot Grunb settings\n\
                    BIOS Settings, such as CPU Power Management, Alg. Performance Boost Disable (ApbDis)...etc.")
    for host in hosts_list:
        status = check_needed_pkg_installed(host)
        results.append(get_boot_grub_settings(host))
        for pci in host.pcie_address:
            if status:
                results.append(get_cpu_power_mng(pci, host))
                results.append(check_perf_boost_status(pci, host))
            else:
                logger.error(f"Error: Host-{host.oob_address} Missing needed 'linux-tools-common linux-tools-$(uname -r) 'packages. Please install or run Bootstrap!")
            results.append(check_numa_nodes_per_socket(pci, host))
            results.append(check_pcie_acs(pci, host))
            results.append(check_x2apic_mode(pci, host))
            results.append(check_preferred_io(pci, host))

    if results:
        logger.info(f"\n Boot/BIOS  Check Data:\n{tabulate(results, headers='keys', tablefmt='orgtbl')}\n")
        if all(r['Status'] == constants.TestStatus.PASSED.value for r in results):
            return results, True
        else:
            logger.error(f"Error: Boot/BIOS Check Data are not good from performance prespective.")
            return results, False
    else:
        logger.error(f"Error: Failed to get Boot/BIOS Check Data")
        return results, False

def check_needed_pkg_installed(host):
    status = True
    cmd = "apt list --installed | grep -i linux-tools-$(uname -r)"
    output = host.sudo(cmd)
    if "linux-tools" not in output:
        status = False
    return status

def get_boot_grub_settings(host):
    """
    Check boot GRUB settings for performance optimizations.

    Args:
        host (object): Host object to perform the check on.

    Returns:
        dict: A dictionary with the check results.
    
    Example Output:
        GRUB_CMDLINE_LINUX_DEFAULT="pci=realloc=off iommu=pt numa_balancing=disable processor.max_cstate=0"
        GRUB_CMDLINE_LINUX=""
    """

    TEST_INFO = "BOOT GRUB Settings: Allows fine-tuning of system behavior at a low level, influencing how the CPU, memory, and other hardware components operate. "
    boot_settings = ["iommu=pt", "numa_balancing=disable", "processor.max_cstate=0", "intel_idle.max", "cstate=0",
                    "intel_pstate=disable"]
    cores_isolated_params = "isolcpus"
    missing_params = []
    status = True
    system_boot_grub_cmd = host.sudo("cat /proc/cmdline")

    for param in boot_settings:
        if param not in system_boot_grub_cmd:
            missing_params.append(param)
            logger.error(f"Error: Boot Grub CMD missing this setting {param} \n Solve it by: `vim /etc/default/grub and appending the missing params to GRUB_CMDLINE_LINUX param`")
    if cores_isolated_params not in system_boot_grub_cmd:
        TEST_INFO = f"{TEST_INFO}\n Do the following to isolate the CPU cores:\n\
                    grubby --update-kernel=ALL --args='isolcpus= x-y nohz_full= x-y rcu_nocbs= x-y'\n\
                    Or vim /etc/default/grub and appending to GRUB_CMDLINE_LINUX param."
        logger.warn(f"CPU Cores are not Isolated, please update the GRUB file with ranges for cores need to be Isolated!")

    if missing_params:
        TEST_INFO = f"{TEST_INFO}\nMissing Params: {','.join(missing_params)}"
        status = False
    
    return {"Host": host.oob_address,
            "PCIe Address": "Any",
            "BIOS Settings": "BOOT GRUB",
            "Output": system_boot_grub_cmd,
            "Expected": f"GRUB CMD contains these params{','.join(boot_settings)},{cores_isolated_params}",
            "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value,
            "Test Notes": TEST_INFO}

def get_cpu_power_mng(pci, host):
    """
    Check CPU Power Management settings.

    Args:
        pci (str): PCIe address of the device to check.
        host (object): Host object representing the system on which the check is performed.

    Returns:
        dict: A dictionary with the check results.
    
    Example Output:
        hardware limits: 800 MHz - 3.40 GHz
        available cpufreq governors: performance powersave
        current policy: frequency should be within 800 MHz and 3.40 GHz.
                        The governor "performance" may decide which speed to use
                        within this range.
        current CPU frequency: Unable to call hardware
        current CPU frequency: 2.36 GHz (asserted by call to kernel)
    """
    TEST_INFO = "CPU Power Management controls how the CPU handles power consumption, frequency scaling, and performance states (P-states)"
    policy = None
    cpu_freq = None
    hardware_limit = None
    status = True
    p_stat = "P0"
    power_management_output = host.sudo(f"cpupower frequency-info").split("\n")

    for line in power_management_output:
        if "available cpufreq governors" in line:
            policy = line.split(":")[1]

        if "hardware limits" in line:
            hardware_limit = line.split(":")[1].split("-")[1].strip()
            TEST_INFO = f"{TEST_INFO}\n{line.strip()}"
            if "G" in hardware_limit:
                hardware_limit = float(hardware_limit.replace("GHz", "")[0].strip())
            elif "M" in hardware_limit:
                hardware_limit = float(hardware_limit.replace("MHz", "")[0].strip()) / 1000                

        if "current CPU frequency" in line and "Hz" in line:
            cpu_freq = line.split(":")[1]
            if "G" in cpu_freq:
                cpu_freq = float(cpu_freq.split("GHz")[0].strip())
            elif "M" in cpu_freq:
                cpu_freq = float(cpu_freq.split("MHz")[0].strip()) / 1000

    if policy:
        if "performance" not in policy:
            logger.error(f"Error: CPU Power Management is not showing 'Performance' in the policy!")
            TEST_INFO = f"{TEST_INFO}\nOutput is not showing 'Performance' in the policy"
            status = False
        if "powersave" in policy:
            logger.info(f"Setup will work with the minimum CPU Frequency as Power-Saving is set part of CPU Power management policy!\n")
            TEST_INFO = f"{TEST_INFO}\nOutput is showing 'powersave' in the policy"
            status = False
    if cpu_freq and hardware_limit:
        if cpu_freq < hardware_limit * 0.50:
            status = False
            p_stat = "P2"
        elif cpu_freq < hardware_limit * 0.85:
            status = False
            p_stat = "P1"

    else:
        p_stat = None
        status = False
        TEST_INFO = f"{TEST_INFO}\nCPU Power is Unable to call to kernel"

    return {"Host": host.oob_address,
            "PCIe Address": pci,
            "BIOS Settings": "CPU Power Management, CPU Frequency, P-Stat", 
            "Output": f"Current CPU Policy{policy}\nCPU Freq: {cpu_freq}\nP-State: {p_stat}",
            "Expected": f"CPU Policy contains `Performance`\nCPU Frequency > 85% of Max limit {hardware_limit}\nP-State: P0",
            "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value,
            "Test Notes": TEST_INFO}

def check_perf_boost_status(pci, host):
    """
    Check if performance boost is supported and active.

    Args:
        pci (str): PCIe address of the device to check.
        host (object): Host object representing the system on which the check is performed.

    Returns:
        dict: A dictionary with the check results.
    
    Example Output:
        boost state support:
            Supported: yes
            Active: yes
    """
    TEST_INFO = "Application Power Management Boost Disable: Controls the boosting capabilities of modern CPUs"
    status = False
    cpu_boost_status = host.sudo(f"cpupower frequency-info")
    if cpu_boost_status and "boost state support:" in cpu_boost_status:
        status = True
        cpu_boost_status = cpu_boost_status.split("boost state support:")[1].strip().split("\n")
        cpu_boost_status = [bstatus.strip() for bstatus in cpu_boost_status]

        for line in cpu_boost_status:
            if "supported" in line.lower() and "no" in line.lower():
                logger.error(f"Error: Performance Boost Service is not supported!")
                TEST_INFO = f"{TEST_INFO}\n Performance Boost is not support!"
                status = False
            elif "active" in line.lower() and "no" in line.lower():
                logger.error(f"Error: Performance Boost serivce is disabled!")
                status = False

    return {"Host": host.oob_address, 
            "PCIe Address": pci,
            "BIOS Settings": "Alg. Performance Boost Disable (ApbDis)", 
            "Output": ",".join(cpu_boost_status) if status else "",
            "Expected": "Supported and Active",
            "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value,
            "Test Notes": TEST_INFO}

def check_numa_nodes_per_socket(pci, host):
    """
    Check the number of NUMA nodes per socket.

    Args:
        pci (str): PCIe address of the device to check.
        host (object): Host object representing the system on which the check is performed.

    Returns:
        dict: A dictionary with the check results.
    """
    TEST_INFO = "NUMA nodes per socket: affects how memory is accessed by CPU cores"
    numa_nodes_number = int(host.sudo(f"lscpu | grep -i 'node(s)'").split(":")[1].strip())
    pci_type = pci_tune.get_pci_type(host, pci)
    status = constants.TestStatus.PASSED.value if numa_nodes_number == 2 else constants.TestStatus.FAILED.value
    numa_exp = "2"
    if "Gen 5" in pci_type:
        status = constants.TestStatus.PASSED.value if numa_nodes_number <= 2 else constants.TestStatus.FAILED.value
        numa_exp = "<=2"
    return {"Host": host.oob_address, 
            "PCIe Address": pci,
            "BIOS Settings": "NUMA Nodes Per Socket", 
            "Output": numa_nodes_number,
            "Expected": numa_exp,
            "Status": status,
            "Test Notes": TEST_INFO}

def check_x2apic_mode(pci, host):
    """
    Check if x2APIC Mode is enabled.

    Args:
        pci (str): PCIe address of the device to check.
        host (object): Host object representing the system on which the check is performed.

    Returns:
        dict: A dictionary with the check results.
    """
    TEST_INFO = " (Extended Advanced Programmable Interrupt Controller) Provides: Scalability for Large Systems, Reduced Interrupt Latency, and Improved Power Efficiency."
    mode = "Enabled" if host.sudo("dmesg | grep -i 'x2apic enabled'") else "Disabled"
    
    return {"Host": host.oob_address, 
            "PCIe Address": pci,
            "BIOS Settings": "x2APIC Mode", 
            "Output": mode,
            "Expected": "Enabled",
            "Status": constants.TestStatus.PASSED.value if mode == "Enabled" else constants.TestStatus.FAILED.value,
            "Test Notes": TEST_INFO}

def check_pcie_acs(pci, host):
    """
    Check PCIe ACS (Access Control Services).

    Args:
        pci (str): PCIe address of the device to check.
        host (object): Host object representing the system on which the check is performed.

    Returns:
        dict: A dictionary with the check results.

    Example Output:
        ACSCap:	SrcValid- TransBlk- ReqRedir- CmpltRedir- UpstreamFwd- EgressCtrl- DirectTrans-
    """
    TEST_INFO = "(Access Control Services) when its disabled, it Improves I/O Performance in Non-Virtualized Environments"
    mode = "Disabled"
    status = constants.TestStatus.PASSED.value
    value = host.sudo(f"lspci -vvv -s {pci} | grep -i 'ACSCap'")
    if value:
        value = value.split(":")[1].strip()
        if "SrcValid+" in value:
            mode = "Enabled"
            status = constants.TestStatus.FAILED.value
        else:
            mode = "Disabled"
            status = constants.TestStatus.PASSED.value
    else:
        mode = "None"
        status = constants.TestStatus.FAILED.value

    return {"Host": host.oob_address,
            "PCIe Address": pci,
            "BIOS Settings": "ACS PCIe", 
            "Output": mode,
            "Expected": "Disabled",
            "Status": status,
            "Test Notes": TEST_INFO}

def check_preferred_io(pci, host):
    """
    Check the status of Preferred I/O for the given PCIe device on the host.

    Args:
        pci (str): PCIe address of the device to check.
        host (object): Host object representing the system on which the check is performed.

    Returns:
        dict: A dictionary with the check results.
    
    Example Output:
        Control: I/O- Mem+ BusMaster+ SpecCycle- MemWINV- VGASnoop- ParErr+ Stepping- SERR+ FastB2B- DisINTx+
    """
    TEST_INFO = "Preferred I/O: refers to the system's handling of Input/Output (I/O) operations between\nthe CPU and PCIe devices. Disabling it can optimize I/O performance in some environments"
    mode = "Enabled"
    io_status = host.sudo(f"lspci -vvv -s {pci} | grep -i 'control:'").split(":")[1]
    if "I/O-" in io_status:
        mode = "Disabled"

    return {"Host": host.oob_address,
            "PCIe Address": pci,
            "BIOS Settings": "Preferred I/O", 
            "Output": mode,
            "Expected": "Disabled",
            "Status": constants.TestStatus.PASSED.value if mode == "Disabled" else constants.TestStatus.FAILED.value,
            "Test Notes": TEST_INFO}

