#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

"""
System HW Inventory Check pat of Performance configuration readiness
1. Check PHY config and cable capabilities performance configurations checks .
2. Return status about Cable & PHY.
"""

import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    import logging
    from tabulate import tabulate
    from common.utils import node
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


# Global variables
logger = logging.getLogger("nvnetperf")
ansi_escape = re.compile(r'\x1B[@-_][0-?]*[ -/]*[@-~]')

IB_TO_ETH_IN_G = {
    "IB-SDR": 2.5,
    "IB-DDR": 5,
    "IB-QDR": 10,
    "IB-FDR10": 10,
    "IB-FDR": 14,
    "IB-EDR": 25,
    "IB-HDR": 50,
    "IB-NDR": 100,
    "IB-XDR": 200,
}

LINK_WIDTHS = {
    0: "1x",
    1: "2x",
    2: "4x",
    3: "8x",
    4: "12x"
}
        
def check_cable_phy_matching(hosts_list):
    """
    Checks the matching between cable capabilities and PHY speed on each host's PCIe devices.
    
    Returns:
        tuple: A tuple containing a list of results and a boolean indicating if all tests passed.
    """
    TEST_INFO = "System HW Inventory: ensure that the hardware is capable of operating at its maximum potential speed and efficiency."
    extra_note = ""
    results = []

    logger.info("Beginning of System HW Inventory check for Performance configuration readiness!")

    for host in hosts_list:
        for pci in host.pcie_address:
            # Get PHY speed and cable capabilities
            phy_speed, cable_speed, link_speed = get_cable_phy_details(host, pci)

            # Determine the status based on the matching of PHY and cable speed
            if phy_speed == 0 and cable_speed == 0 and link_speed ==0:
                logger.info(f"Error Cable/PHY Capabilities Check: Failed to get link layer of {pci} on {host.oob_address}")
                extra_note = "Failed to get PCI Link Layer, Check `mlxconfig -d <PCI> q | grep -i 'link_type''"
            status = constants.TestStatus.PASSED.value if phy_speed == cable_speed else constants.TestStatus.FAILED.value
            results.append({
                "Host": host.oob_address, 
                "PCI Address": pci, 
                "PHY Speed": phy_speed,
                "Cable Max Speed": cable_speed,
                "Enabled Link Speed":link_speed,
                "Expected": cable_speed,
                "Status": status,
                "Test Notes": f"{TEST_INFO}\n{extra_note}"
            })

    if results:
        logger.info(f"Cable/PHY Capabilities Check Data:\n{tabulate(results, headers='keys', tablefmt='orgtbl')}")
        if all(r['Status'] == constants.TestStatus.PASSED.value for r in results):
            return results, True
        else:
            logger.error(f"Error: Cable and PHY Does not match and so not good from performance prespective.")
            return results, False
    else:
        logger.error(f"Error: Failed to get Cable/PHY Info")
        return results, False

def get_cable_phy_details(host, pci):
    """
    Retrieves the cable capabilities for the given PCI device.

    Args:
        host (Host): The host object containing the PCI device.
        pci (str): The PCI address of the device.

    Returns:
        float: The cable speed limit in Gbps.
    """
    data = []
    link_layer = node.HostNode.get_link_layer(host, pci)

    link_speed = ansi_escape.sub('', host.sudo(f"mlxlink -d {pci} | grep -i 'link speed'")).strip().split(" ")[-1].strip()
    cable_max_speed = link_speed.split(",")[0].strip().split("(")[1]
    status = False
    if constants.LinkLayerType.ETHERNET.value in link_layer:
        phy_speed = host.sudo(f"mlxlink -d {pci} | grep -i 'speed'").split("\n")[0].strip().split(":")[1]
        if phy_speed and "N/A" not in phy_speed:
            phy_speed = float(ansi_escape.sub('', phy_speed.split("G")[0].strip()))
        else:
            logger.error(f"Failed to get PHY Speed for {pci} on {host.oob_address}")
            phy_speed = 0
        cable_max_speed = float(cable_max_speed.split("G")[0].strip())
        status = constants.TestStatus.PASSED.value if phy_speed == cable_max_speed else constants.TestStatus.FAILED.value
    elif constants.LinkLayerType.INFINIBAND.value in link_layer:
        current_speed, current_width = get_current_phy_values(pci, host)
        enabled_link_speeds, link_max_capability_width = get_link_capabilities(pci, host)

        # Calculate in Gbps
        current_speed_gbps = IB_TO_ETH_IN_G.get(ansi_escape.sub('', current_speed), 0)
        max_enabled_speed_gbps = max(IB_TO_ETH_IN_G.get(speed, 0) for speed in enabled_link_speeds)
        logger.info(f"On Host-{host.oob_address} | PCI-{pci} :- PHY Configs are: Speed: {current_speed}({current_speed_gbps}), Width: {current_width}")

        phy_speed = current_speed_gbps * current_width
        cable_max_speed = max_enabled_speed_gbps * link_max_capability_width
        link_speed = enabled_link_speeds
        status = constants.TestStatus.PASSED.value if phy_speed == cable_max_speed else constants.TestStatus.FAILED.value
    else:
        return 0, 0, 0

    data.append({"PCI Address": pci, "PHY Speed": phy_speed, "Cable Max Speed": cable_max_speed, "Enabled Link Speed": link_speed, "Status": status})

    logger.info(f"Host - {host.oob_address} Cable Speed:\n{tabulate(data, headers='keys', tablefmt='orgtbl')}")
    return phy_speed, cable_max_speed, link_speed

def get_current_phy_values(pci, host):
    logs = host.sudo(f"mlxlink -d {pci}")
    current_speed_match = re.search(r"Speed\s+:\s+(\S+)", logs)
    width_match = re.search(r"Width\s*:\s*(\S+)x", logs)

    current_speed = current_speed_match.group(1) if current_speed_match else None
    width = int(ansi_escape.sub('', width_match.group(1))) if width_match else 1
    logger.info(f"On Host-{host.oob_address} | PCI: {pci} :- PHY Speed is: {current_speed}, PHY Width is: {width}")
    return current_speed, width

def get_link_capabilities(pci, host):
    logs = host.sudo(f"mlxlink -d {pci}")
    enabled_speeds_match = re.search(r"Enabled Link Speed\s+:.*\((.*?)\)", logs)
    enabled_speeds = [f"IB-{speed.strip()}" for speed in enabled_speeds_match.group(1).split(",")] if enabled_speeds_match else []
    width = get_link_max_width_capability(pci, host)
    logger.info(f"On Host-{host.oob_address} | PCI: {pci} :- Enabled Link Speed is/are: {enabled_speeds}, Link Max capability Width: {width}")
    return enabled_speeds, width

def get_link_max_width_capability(pci, host):
    logs = host.sudo(f"sudo mlxreg -d {pci} --reg_name PTYS --get --indexes 'local_port=1,proto_mask=1,lp_msb=0,plane_ind=0,port_type=1'  | grep ib_link_width_capability")
    capability_width = re.search(r"ib_link_width_capability\s*\|\s*(0x[0-9a-fA-F]+)", logs)

    if capability_width:
        hex_value = ansi_escape.sub('', capability_width.group(1))  # Extract the hex value
        binary_value = bin(int(hex_value, 16))[2:].zfill(32)  # Convert to binary, pad to 32 bits
    else:
        logger.error(f"On Host-{host.oob_address} | PCI-{pci} :- No hex value found.")
    
    width = max(int(LINK_WIDTHS[i].split('x')[0]) for i in range(len(binary_value)) if binary_value[-(i+1)] == '1')
    return width

