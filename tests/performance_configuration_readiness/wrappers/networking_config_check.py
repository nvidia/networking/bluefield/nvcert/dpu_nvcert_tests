#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

"""
Networking Configurations Check part of Performance configuration readiness
1. Cehck Memory size and compare it to the cache size as part of performance configurations checks.
2. Return detailed performance status about the setup Cache/Memory before running tests.
"""

import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import logging
    from tabulate import tabulate
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


# Global variables
NUM_OF_QPs = 4
logger = logging.getLogger("nvnetperf")


logger.info("Beginning Networking Configurations Check of Performance configuration readiness!")

def cache_memory_check_test(hosts_list):
    """
    Perform Networking Configurations check on both hosts and log the results.

    Returns:
        bool: True if all checks pass, False otherwise.
    """
    TEST_INFO = "Networking Configurations check: To ensure enough cache for running full line rate"
    results = []

    logger.info("Beginning Networking Configurations Check of Performance configuration readiness!")
    for host in hosts_list:
        cache_size = get_l3_cache_size(host)
        direction_num = 2 # bidirectional
        buffer_data = get_buffer_size(host)
        for data in buffer_data:
            status = cache_size * 1000 > (2 * data["buffer_size"] * NUM_OF_QPs * direction_num)
            results.append({
                "Host": host.oob_address,
                "ETH Device": data["device"],
                "Cache Size": cache_size,
                "Buffer Size": data["buffer_size"],
                "Number of QP": NUM_OF_QPs,
                "Number of Directions": direction_num,
                "Expected": "cache_size * 1000 > (2 * buffer_size * NUM_OF_QPs * direction_num)",
                "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value,
                "Test Notes": TEST_INFO if status else f"{TEST_INFO}\n LLC size is lower than(2 * Buffer Size * Number Of QPs * Number of Directions)"
            })

    if results:
        logger.info(f"\n Networking Configurations Check Data:\n{tabulate(results, headers='keys', tablefmt='orgtbl')}\n")
        if all(r['Status'] == constants.TestStatus.PASSED.value for r in results):
            return results, True
        else:
            logger.error(f"Error: Networking Configurations Data are not good from performance prespective.")
            return results, False
    else:
        logger.error(f"Error: Failed to get Networking Configurations Data")
        return results, False

def get_l3_cache_size(host):
    """
    Get the L3 cache size of the CPU on the host.

    This function retrieves the CPU LLC cache sizes (L3) using the `lscpu` command

    Args:
        host: The host object on which the command is run.

    Returns:
        float: The CPU L3 cache size in MiB.
    
    Example:
        # Expected output format from lscpu:
        # L3 cache:                             12M
    """
    # Retrieve the cache sizes using lscpu command
    cache_output = host.sudo(f"lscpu | grep -i 'l3 cache' | awk '{{print $3 $4}}'").strip()
    cache_size = 0

    if 'K' in cache_output:
        cache_size = float(cache_output.split("KiB")[0].strip()) / 1024  # Convert from KiB to MiB
    elif 'M' in cache_output:
        cache_size = float(cache_output.split("MiB")[0].strip())         # Cache is already in MiB
    elif 'G' in cache_output:
        cache_size = float(cache_output.split("GiB")[0].strip()) * 1024  # Convert from GiB to MiB

    return cache_size

def get_buffer_size(host):
    HEADER = 40
    data = []
    for pci in host.pcie_address:
        net_device = get_net_device(pci, host)
        mtu_value = float(host.sudo(f"ifconfig {net_device} | grep -i 'mtu'").split(" ")[-1])
        buffer_size = mtu_value - HEADER
        data.append({"device": net_device, "buffer_size": buffer_size})
    return data

def get_net_device(pci, host):
    device = host.sudo(f"mst status -v | grep -i {pci} | awk '{{print $5}}'").strip().split("-")[1]
    return device


