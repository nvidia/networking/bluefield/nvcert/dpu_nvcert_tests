#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

"""
System Health Check Wrapper part of Performance configuration readiness
1. Check if the CPU is idle or in use, and Return CPU status based on predefined idle percentage limits
2. Check IRQ Balance Service Enabled/Disabled.
"""
import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import logging
    from tabulate import tabulate
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


# Global variables
logger = logging.getLogger("nvnetperf")
CPU_IDLE_LIMIT = 99 # Minimum percentage for CPU to be considered idle


def check_system_health_status(hosts_list):
    """
    Perform System Health checks on a list of hosts.

    Args:
        hosts_list (list): List of host objects.

    Returns:
        tuple: A list of results and a boolean indicating if all hosts passed the test.
    """

    results = []

    logger.info("Beginning of System Health check for Performance configuration readiness!")
    for host in hosts_list:
        cpu_status = get_cpu_idle_status(host)
        irq_balance_status = get_irq_balance_status(host)
        status = cpu_status[1] and irq_balance_status[1]
        results.append({
            "Host": host.oob_address, 
            "CPU Idle %": f"{cpu_status[0]} %",
            "irqbalance Status": irq_balance_status[0],
            "Status": constants.TestStatus.PASSED.value if status else constants.TestStatus.FAILED.value,
            "Test Expectation": f"CPU Idle > {CPU_IDLE_LIMIT}% and IRQBalance service is Disabled"
        })

    if results:
        logger.info(f"System Health Check Data:\n{tabulate(results, headers='keys', tablefmt='orgtbl')}")
        if all(r['Status'] == constants.TestStatus.PASSED.value for r in results):
            return results, True
        else:
            logger.error(f"Error: CPU is in use , System might not have enough resources.")
            return results, False
    else:
        logger.error(f"Error: Failed to get CPU Idleation info")
        return results, False

def get_cpu_idle_status(host):
    """
    Fetches the CPU idle percentage for a given host.

    Args:
        host (object): Host object containing host information.

    Returns:
        tuple: CPU idle percentage, bool (True/False)
    """
    TEST_INFO = "CPU Idle: indicates that the system has available resources to handle additional workloads\n\
                        and respond quickly to new tasks"
    data = []
    # Fetch CPU idle percentage using the `sar` command
    try:
        cpu_idle_percent = float(host.sudo("sar 1 10 | grep Average | awk '{print $NF}'").strip())
        data.append({"Host ip": host.oob_address, "CPU Idle %": cpu_idle_percent, "Status": constants.TestStatus.PASSED.value if cpu_idle_percent >= CPU_IDLE_LIMIT else constants.TestStatus.FAILED.value, "Notes": TEST_INFO})
        logger.info(f"\nHost - {host.oob_address} CPU Idle Data:\n{tabulate(data, headers='keys', tablefmt='orgtbl')}\n")
    except ValueError:
        logger.error(f"Failed to parse CPU usage percent for host: {host.oob_address}")
        cpu_idle_percent = 100.0 - CPU_IDLE_LIMIT  # Fallback if parsing fails
    return cpu_idle_percent, True if cpu_idle_percent >= CPU_IDLE_LIMIT else False

def get_irq_balance_status(host):
    """
    Get the irq_balance service status for a given host.

    Args:
        host (object): Host object containing host information.

    Returns:
        tuple: irq_balance status Enabled/Disabled, test status.
    """
    TEST_INFO = "IRQ Balance service need to be Disabled"
    irq_status = "Disabled"
    data = []

    irq_res = host.sudo("systemctl status irqbalance.service | grep -i 'Active:'").strip()
    if "inactive" not in irq_res:
        logger.warning(f"Warning: irq_balance service is Enabled on Host-{host.oob_address}")
        irq_status = "Enabled"
    data.append({"Host ip": host.oob_address, "IRQ Balance status": irq_status, "Status": constants.TestStatus.PASSED.value, "Notes": TEST_INFO})
    logger.info(f"\nHost - {host.oob_address} IRQ Balance Service Data:\n{tabulate(data, headers='keys', tablefmt='orgtbl')}\n")
    
    return irq_status, True


