#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


"""
GPU HW Inventory Check pat of Performance configuration readiness
1. Check GPU Frequency set to the max clock config.
2. Return status about GPU Frequency compared to the MAX Clock.
"""

import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    import logging
    from tabulate import tabulate
    from common.utils import node
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


# Global variables
logger = logging.getLogger("nvnetperf")


def check_gpu_freq_clocks(hosts_list):
    """
    Check GPU HW Inventory for all hosts provided in the `hosts_list`.

    Args:
    - hosts_list (list): A list of host objects that contain relevant system information.

    Returns:
    - results (list): A list of results for each host.
    - success (bool): Whether all tests have passed or not.
    """
    
    TEST_INFO = "GPU Frequency Clocks: ensure that the hardware is capable of operating at its maximum potential speed and efficiency."
    CONFIG_CLOCKS = "Use the following to configure it: 1.nvidia-smi -lgc 2100 - lock dGPU to max clock (actually it will lock to 1905). 2.nvidia-smi -lmc 2000 - lock dGPU memory max clock."
    results = []

    logger.info("Beginning of GPU HW Inventory check for Performance configuration readiness!")
    for host in hosts_list:
        if node.HostNode.check_gpus_available(host):
            freq, max_clocks = get_freq_clocks(host)

            results.append({
                "Host": host.oob_address, 
                "GPU Frequency": f"{freq} MHz",
                "MAX Clocks": f"{max_clocks} MHz",
                "Expected": f"{max_clocks} MHz",
                "Status": constants.TestStatus.PASSED.value if freq == max_clocks else constants.TestStatus.FAILED.value,
                "Test Notes": TEST_INFO if freq == max_clocks else f"{TEST_INFO}\n{CONFIG_CLOCKS}"
            })
        else:
            logger.error(f"Error: No available GPU on this setup, GPU HW Inventory test can't run!")
            return [], True


    if results:
        logger.info(f"GPU HW Inventory Check Data:\n{tabulate(results, headers='keys', tablefmt='orgtbl')}")
        if all(r['Status'] == constants.TestStatus.PASSED.value for r in results):
            return results, True
        else:
            logger.error(f"Error: GPU Frequency does not match the MAX Clocks, and so not good from performance prespective.")
            return results, False
    else:
        logger.error(f"Error: Failed to get GPU HW Inventory Info")
        return results, False

def get_freq_clocks(host):
        """
        Gets the current GPU frequency and maximum clock values from the system.

        Args:
        - host (object): Host object to run commands on.

        Returns:
        - (float, float): Tuple containing current GPU frequency and maximum clock values.
        """
        smi_logs = host.sudo(f"nvidia-smi -q -i 0")

        frequency = re.search(r"Clocks\s+Graphics\s+:\s+\d+\s?(?:[GMK]?Hz)", smi_logs, re.MULTILINE)
        if frequency:
            frequency = frequency.group().strip().split(":")[1]
            if "M" in frequency:
                frequency = float(frequency.replace("MHz", "").strip())
            elif "G" in frequency:
                frequency = float(frequency.replace("GHz", "").strip()) * 1000
            
        max_clocks = re.search(r"Max Clocks\s+Graphics\s+:\s+\d+\s?(?:[GMK]?Hz)", smi_logs, re.MULTILINE)
        if max_clocks:
            max_clocks = max_clocks.group().strip().split(":")[1]
            if "M" in max_clocks:
                max_clocks = float(max_clocks.replace("MHz", "").strip())
            elif "G" in max_clocks:
                max_clocks = float(max_clocks.replace("GHz", "").strip()) * 1000
        return frequency, max_clocks