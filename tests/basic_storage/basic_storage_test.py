#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    from common.basic_classes import nvnetperf_base_class as nbc
    from tabulate import tabulate
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class BasicStorageTest(nbc.NVNetPerfBaseClass):
    """
    Basic Storage Test Class
    """

    def __init__(self):
        super().__init__()
        self.test_name = "basic_storage_benchmark"
        self.benchmark_basic_storage_target_init = "tests/basic_storage/scripts/basic_storage_test/nvmeof_target_init.sh"
        self.benchmark_basic_storage_target_clear = "tests/basic_storage/scripts/basic_storage_test/nvmeof_target_clear.py"
        self.benchmark_basic_storage_initiator_init = "tests/basic_storage/scripts/basic_storage_test/nvmeof_initiator_init.sh"
        self.benchmark_basic_storage_initiator_run = "tests/basic_storage/scripts/basic_storage_test/nvmeof_initiator_run.sh"


    def args_test(self):
        super().args_test()
        self.parser.add_argument("--disable_ports_config", action='store_true',
                        help="Disable the ports configuration step")
        self.parser.add_argument("--disable_ports_deconfig", action='store_true',
                        help="Disable the ports deconfiguration step")

    def args_parsed(self):
        super().args_parsed()
        self.disable_ports_config = self.args.disable_ports_config
        self.disable_ports_deconfig = (self.args.disable_ports_deconfig or self.args.disable_ports_config)


    def test_setup(self):
        self.logger.info("Basic Storage benchmark - Start Test Preparation")
        config_ports = False if self.disable_ports_config else True
        super().test_setup(config_ports=config_ports)
        self.compute_pass_rates()


    def test_logic(self):
        self.logger.info("Basic Storage Benchmark Test - Start Execution")
        self.run_basic_storage_benchmark()
        return self.test_status


    def post_test_logic(self):
        super().post_test_logic()
        self.server_handler.clear_test_setup_resources(self.node_group_dict, self.disable_ports_deconfig)


    def compute_pass_rates(self):
        """
        set pass rates for the various benchmark tests
        """
        self.basic_storage_pass_rate = {}
        for node_group_k in self.node_group_dict:
            node_group = self.node_group_dict[node_group_k]
            speeds = [netdev.speed for netdev in node_group.hosts['host_1'].netdevs] + [netdev.speed for netdev in node_group.hosts['host_2'].netdevs]
            # Interface speed is in Mbps; convert to Gbps to match with throughput
            min_host_rate = min(speeds)/1000
            self.basic_storage_pass_rate[node_group_k] = (constants.TestThroughputPassRatio.FIO_PASS_THROUGHPUT.value * min_host_rate)/100


    def run_basic_storage_benchmark(self):
        """
        Run Basic Storage test concurrently on all servers
        """
        results = {}
        super().run_test_on_all_servers(servers=self.node_group_dict.keys(), timeout=1500, results=results)
        self.print_results(results)


    def run_test_on_server(self, idx, **kwargs):
        """
        Run basic storage benchmark (FIO) on the given node group
        """
        self.logger.info(f"Run Basic Storage benchmark on {idx}")
        res_dict = kwargs['results']
        node_group = self.node_group_dict[idx]
        host_1 = node_group.hosts['host_1']
        host_2 = node_group.hosts['host_2']

        # Check if OS is installed on NVMe Device
        os_check_cmd = "df -h | grep '/boot/efi'"
        os_device1 = host_1.run(os_check_cmd, warn=True, hide="stderr")
        os_device2 = host_2.run(os_check_cmd, warn=True, hide="stderr")
        test_status = constants.ExitStatus.NVNETPERF_FAIL.value
        if "nvme" in os_device1:
            self.logger.info(f"Basic storage test is not supported because OS is installed on NVMe device on server {host_1.oob_address}")
            return test_status
        elif "nvme" in os_device2:
            self.logger.info(f"Basic storage test is not supported because OS is installed on NVMe device on server {host_2.oob_address}")
            return test_status

        # Copy the scripts to remote servers
        host_1.put(self.benchmark_basic_storage_target_init, constants.TestsPaths.FILES_DIR.value, None)
        host_1.put(self.benchmark_basic_storage_target_clear, constants.TestsPaths.FILES_DIR.value, None)
        host_2.put(self.benchmark_basic_storage_initiator_init, constants.TestsPaths.FILES_DIR.value, None)
        host_2.put(self.benchmark_basic_storage_initiator_run, constants.TestsPaths.FILES_DIR.value, None)

        if not host_1.uplink_networks:
            raise Exception(f"The uplink_network list is missing for {host_1.oob_address} in the inventory file. Please add it.")
        ip = host_1.uplink_networks[0].split("/")[0]
        output = host_1.run(f"{constants.TestsPaths.FILES_DIR.value}/nvmeof_target_init.sh {ip}")
        if "NVMe Target init finished successfully" not in output:
            self.logger.error(f"Failed to initiate the NVMe target on {host_1.oob_address}")

        output = host_2.run(f"{constants.TestsPaths.FILES_DIR.value}/nvmeof_initiator_init.sh {ip}")
        if "NVMe Initiator init finished successfully" not in output:
            self.logger.error(f"Failed to initiate the NVMe initiator on {host_2.oob_address}")

        output = host_2.run(f"{constants.TestsPaths.FILES_DIR.value}/nvmeof_initiator_run.sh")

        pattern = r"\((\d+(?:\.\d+)?)[GM]B/s\)"
        bw = re.search(pattern, output)

        host_2.sudo("nvme disconnect -n nvnetperf")
        host_1.sudo(f"{constants.TestsPaths.FILES_DIR.value}/nvmeof_target_clear.py")

        if bw:
            if "GB" in bw.group():
                result = 8*float(bw.group(1).split("GB/s")[0])
                res_dict.setdefault(idx, {})["bw"] = f"{result} GB/s"
                if result > self.basic_storage_pass_rate[idx]:
                    res_dict.setdefault(idx, {})["status"] = constants.TestStatus.PASSED
                    test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
                else:
                    res_dict.setdefault(idx, {})["status"] = constants.TestStatus.FAILED
                    test_status = constants.ExitStatus.NVNETPERF_PERF_ERROR.value
            else:
                result = 8*float(bw.group(1).split("MB/s")[0])
                res_dict.setdefault(idx, {})["bw"] = f"{result} MB/s"
                res_dict.setdefault(idx, {})["status"] = constants.TestStatus.FAILED
                test_status = constants.ExitStatus.NVNETPERF_PERF_ERROR.value
        else:
            res_dict.setdefault(idx, {})["bw"] = "N/A"
            res_dict.setdefault(idx, {})["status"] = constants.TestStatus.FAILED
            test_status = constants.ExitStatus.NVNETPERF_FAIL.value
        return test_status


    def print_results(self, res_dict):
        """
        Print the test's results for all node groups
        """
        table = []

        headers = ['Servers', f'BW [Gb/s]', 'Test Status']
        table.insert(0, headers)

        for node_group_key in self.node_group_dict.keys():
            row = []
            server1 = self.node_group_dict[node_group_key].hosts['host_1'].oob_address
            server2 = self.node_group_dict[node_group_key].hosts['host_2'].oob_address
            row.append(f"{server1} <-> {server2}")
            row.append(res_dict[node_group_key]['bw'])
            row.append(res_dict[node_group_key]["status"].value)
            table.append(row)

        column_alignments = ('center', 'center', 'center')
        self.logger.info(f"\n\nTest Summary:\n\n{tabulate(table, headers='firstrow', tablefmt='orgtbl', colalign=column_alignments)}\n")



if __name__ == "__main__":
    test = BasicStorageTest()
    test.args_execute()
    exit(test.execute())