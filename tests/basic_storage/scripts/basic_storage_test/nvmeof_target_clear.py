#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

# pylint: disable=too-many-instance-attributes

import sys
if sys.version_info[0] >= 3:
     unicode = str
import os
try:
    import commands
except ImportError:
    import subprocess as commands


NVMET_CONFIGFS = "/sys/kernel/config/nvmet/"
NVMET_PORTS_CONFIGFS = os.path.join(NVMET_CONFIGFS, "ports")
NVMET_SUBSYSTEMS_CONFIGFS = os.path.join(NVMET_CONFIGFS, "subsystems")

def unload(module):
    cmd = "modprobe -r " + module
    rc, out = commands.getstatusoutput(cmd)
    if rc:
        print(out)
    return rc


def unload_modules(modules):
    rc = 0
    for module in modules:
        rc = unload(module) or rc
    return rc


def remove_port(port):
    rc = 0
    subsystems_path = os.path.join(port, "subsystems")
    subsystems = os.listdir(subsystems_path)
    for subsystem in subsystems:
        subsystem_path = os.path.join(subsystems_path, subsystem)
        print(f"[NvCert] removing: {subsystem_path}")
        rc = os.remove(subsystem_path) or rc
    print(f"[NvCert] removing: {port}")
    rc = os.rmdir(port) or rc
    return rc

def remove_subsystem(subsystem):
    rc = 0
    namespaces_path = os.path.join(subsystem, "namespaces")
    namespaces = os.listdir(namespaces_path)
    for namespace in namespaces:
        namespace_path = os.path.join(namespaces_path, namespace)
        print(f"[NvCert] removing: {namespace_path}")
        rc = os.rmdir(namespace_path) or rc
    print(f"[NvCert] removing: {subsystem}")
    rc = os.rmdir(subsystem) or rc
    return rc

def remove_all_helper(full_dir_path, remove):
    rc = 0
    for item in os.listdir(full_dir_path):
        full_item_path = os.path.join(full_dir_path, item)
        if os.path.isdir(full_item_path):
            print(f"[NvCert] removing: {full_item_path}")
            rc = remove(full_item_path) or rc
    return rc


def remove_all_ports():
    return remove_all_helper(NVMET_PORTS_CONFIGFS, remove_port)


def remove_all_subsystems():
    return remove_all_helper(NVMET_SUBSYSTEMS_CONFIGFS, remove_subsystem)


def main():
    print("Clearing...")
    rc = remove_all_ports()
    rc = remove_all_subsystems() or rc
    rc = unload_modules(["nvmet_rdma", "nvme_loop", "nvmet", "nvme", "null_blk"]) or rc
    sys.exit(rc)


if __name__ == "__main__":
    main()
