#!/bin/bash

set -e

echo "Running FIO, it may take up to 3 minutes"

# Step 1 - set up parameters
dev_name=`sudo nvme list | awk '{print $1}' | head -3 | tail -1`
numa=$(sudo mst status -v | grep BlueField | awk '{print $6}' | head -1)
cpus=`sudo lscpu | grep "NUMA node${numa}" | awk '{print $4}'`

# Step 2 - run FIO
sudo fio --filename=${dev_name} --bs=4k --numjobs=32 --iodepth=128 --loops=1 --ioengine=libaio --direct=1 --norandommap=1 --time_based=1 --runtime=60 --name=read-phase --rw=randread --group_reporting=1 --cpus_allowed=${cpus} --cpus_allowed_policy=split --ramp_time=30