#!/bin/bash

set -e

echo "Initiating NVMe Initiator"

IP="${1}"
PORT="${2:-4420}"
SUBSYSTEM="${3:-nvnetperf}"

# Step 1 - load modules
sudo modprobe nvme
sudo modprobe nvme-rdma

# Step 2 - set affinity
numa=$(sudo mst status -v | grep BlueField | awk '{print $6}' | head -1)
mlx_dev=$(sudo mst status -v | grep BlueField3 | awk '{print $4}' | paste -sd, -)
if (($numa >= 0)); then
    sudo set_irq_affinity_bynode.sh "${numa}" "${mlx_dev}"
else
    sudo set_irq_affinity.sh "${mlx_dev}"
fi

#Step 3 - detect Subsytyem
sudo nvme discover -t rdma -a ${IP} -s ${PORT}


if [ $? -ne 0 ]; then
    # Command failed
    echo "Command failed"
    exit -1
fi

# Step 4 - create new NVMe device in the OS
sudo nvme connect -t rdma -n ${SUBSYSTEM} -a ${IP} -s ${PORT}

echo "NVMe Initiator init finished successfully"