#!/bin/bash

#IP and PORT are related to the address of where nvme target is exposed.
# IP should be actually configured on any of the physical interfaces
#nvme target has a default 4420 port
#remaining parameters contain internal nvme stuff, but are still configurable with default values

set -e

IP="${1}"
PORT="${2:-4420}"
SUBSYSTEM="${3:-nvnetperf}"
NAMESPACE="${4:-10}"
NVMEPORT="${5:-1}"

echo "Initiating NVMe Target"

# Step 1 - load all modules
sudo modprobe mlx5_ib
sudo modprobe rdma_cm
sudo modprobe ib_ipoib
sudo modprobe null_blk nr_devices=1 use_per_node_hctx=Y irqmode=2 completion_nsec=1000 hw_queue_depth=128
sudo modprobe nvmet
sudo modprobe nvmet_rdma
sudo service irqbalance stop

# Step 2 - set affinity
interface=$(ip a | grep -w "inet ${IP}" |  awk '{print $NF}')
if [[ -z "$interface" ]]; then
    echo "No interface found with IP address ${IP}"
fi

numa=$(cat /sys/class/net/"${interface}"/device/numa_node)
mlx_dev=$(sudo mst status -v | grep BlueField | awk '{print $4}' | paste -sd, -)
if (($numa >= 0 )); then
    sudo set_irq_affinity_bynode.sh "${numa}" "${mlx_dev}"
else
    sudo set_irq_affinity.sh "${mlx_dev}"
fi

# Step 3 - configure subsystem
sudo mkdir /sys/kernel/config/nvmet/subsystems/${SUBSYSTEM}
pushd /sys/kernel/config/nvmet/subsystems/${SUBSYSTEM}
echo 1 | sudo tee attr_allow_any_host

# Step 4 - configure namespace
sudo mkdir namespaces/${NAMESPACE}
cd namespaces/${NAMESPACE}

# Step 5 - configure blk device
echo -n /dev/nullb0 | sudo tee device_path
echo 1 | sudo tee enable

# Step 3 - config ports
sudo mkdir /sys/kernel/config/nvmet/ports/${NVMEPORT}
cd /sys/kernel/config/nvmet/ports/${NVMEPORT}
echo ${IP} | sudo tee addr_traddr
echo rdma | sudo tee addr_trtype
echo ${PORT} | sudo tee addr_trsvcid
echo ipv4 | sudo tee addr_adrfam
echo 4096 | sudo tee param_inline_data_size
echo 1 | sudo tee param_offload_queues

sudo ln -s /sys/kernel/config/nvmet/subsystems/${SUBSYSTEM} /sys/kernel/config/nvmet/ports/${NVMEPORT}/subsystems/${SUBSYSTEM}

popd

echo "NVMe Target init finished successfully"