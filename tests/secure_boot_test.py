#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    from tabulate import tabulate
    from fabric2 import Connection
    from common.basic_classes import nvnetperf_base_class as nbc
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class SecureBootTest(nbc.NVNetPerfBaseClass):
    """
    Secure Boot Test
    """

    def __init__(self):
        super().__init__()
        self.test_name = "secure_boot"

    def test_setup(self):
        self.logger.info("Secure Boot - Start Test Preparation")
        super().test_setup(loopback_supported=True, ib_supported=True)


    def test_logic(self):
        """
        Run secure boot benchmark
        """
        self.logger.info("Secure Boot - Start Test Execution")
        dpus = []
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                for dpu in host.dpus.values():
                    dpus.append(dpu)
        # Run the command concurrently on all servers
        results = {}
        super().run_test_on_all_servers(servers=dpus, timeout=20, results=results)
        self.print_results(results)
        return self.test_status


    def print_results(self, results, **kwargs):
        """
        Print test's results
        """
        table = [['DPU', 'Test Status']]
        for key in results.keys():
            row = [key, results[key]]
            table.append(row)
        self.logger.info(f"\n\nTest Summary:\n\n{tabulate(table, headers='firstrow', tablefmt='orgtbl')}\n")


    def run_test_on_server(self, dpu, **kwargs):
        results = kwargs['results']
        command = "mokutil --sb-state"
        result = dpu.run(command)
        output_pat = re.compile(r"SecureBoot (?P<result>\S+)")
        obj = output_pat.search(result.stdout)
        if obj:
            if "enabled" in obj.group("result"):
                results[dpu.oob_address] = 'Passed'
                test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
            else:
                results[dpu.oob_address] = 'Failed'
                test_status = constants.ExitStatus.NVNETPERF_PERF_ERROR.value
        else:
            results[dpu.oob_address] = 'Failed to run'
            test_status = constants.ExitStatus.NVNETPERF_FAIL.value
        return test_status



if __name__ == "__main__":
    test = SecureBootTest()
    exit(test.execute())