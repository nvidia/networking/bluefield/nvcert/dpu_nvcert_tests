#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    from common.basic_classes import nvnetperf_base_class as nbc
    from datetime import datetime
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc

class SystemInfoTest(nbc.NVNetPerfBaseClass):
    """
    System Info Test
    """

    def __init__(self):
        super().__init__()
        self.test_name = "system_info"


    def pre_test_logic(self):
        super().pre_test_logic()
        return constants.ExitStatus.NVNETPERF_SUCCESS.value


    def test_setup(self):
        self.logger.info("System Info - Start Test Preparation")
        super().test_setup(loopback_supported=True, ib_supported=True)


    def test_logic(self):
        """
        Run system info
        """
        self.logger.info("System Info - Start Test Execution")
        servers = []
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                servers.append(host)
                if not host.nic_mode:
                    for dpu in host.dpus.values():
                        servers.append(dpu)
        results = {}
        super().run_test_on_all_servers(servers=servers, timeout=1000, results=results)
        return self.test_status


    def print_results(self, results, **kwargs):
        """
        Print results
        """
        servers = kwargs["servers"]
        time_stamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        self.test_server_node.create_dir(f"{constants.TestsPaths.FILES_DIR.value}/sysinfo_{time_stamp}")
        for server in servers:
            server.get(results[server.oob_address], f"{constants.TestsPaths.FILES_DIR.value}/sysinfo_{time_stamp}/")
        self.logger.info(f"All sysinfo logs can be found under {constants.TestsPaths.FILES_DIR.value}/sysinfo_{time_stamp}/")


    def run_test_on_server(self, server, **kwargs):
        """
        Run sys-info utility on the given server
        """
        test_status = constants.ExitStatus.NVNETPERF_FAIL.value
        results = kwargs['results']
        logs = server.sudo("sysinfo-snapshot.py")
        pattern = re.compile(r'/tmp/sysinfo-snapshot-.*\.tgz')
        match = pattern.search(logs)
        if match:
            out_file = match.group()
            results[server.oob_address] = out_file
            test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value

        return test_status


    def post_test_logic(self):
        self.logger.info("System log error inspector is not running here")
        self.post_status = constants.ExitStatus.NVNETPERF_SUCCESS.value



if __name__ == "__main__":
    test = SystemInfoTest()
    exit(test.execute())