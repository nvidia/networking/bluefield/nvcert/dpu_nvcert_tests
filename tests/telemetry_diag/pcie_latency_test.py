#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import csv
    from tabulate import tabulate
    from common.basic_classes import telemetry_diag_base_class as tdbc

except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class PcieLatencyTest(tdbc.TelemetryDiagBaseClass):
    """
    Pcie Latency Test Class
    """

    def __init__(self):
        super().__init__()
        self.test_name = "pcie_latency_benchmark"
        # in nano-seconds
        self.average_pcie_read_latency = 2000


    def args_test(self):
            super().args_test()
            self.parser.add_argument("--steady", action='store_true',
                            help="Run PCIe latency test on steady state on the system")
            self.parser.add_argument("--stress", action='store_true',
                            help="Run PCIe latency test under stress on the system")
            self.parser.add_argument("--bw_message_size_list",
                            help="RDMA tests - provide a comma separated message size list to run bw tests (default: 65536)")


    def args_parsed(self):
        super().args_parsed()
        self.steady_state = self.args.steady
        self.under_stress = self.args.stress
        self.bw_message_size_list = self.args.bw_message_size_list
        # If no specific mode is selected, run both steady state and under stress tests
        if not self.steady_state and not self.under_stress:
            self.steady_state = True
            self.under_stress = True


    def test_setup(self):
        self.logger.info("Pcie Latency Benchmark - Start Test Preparation")
        if self.under_stress:
            super().test_setup(loopback_supported=True, ib_supported=True)

            # Check if ping works
            if not self.run_ping_with_backoff():
                self.test_status =  constants.ExitStatus.NVNETPERF_COMM_ERROR.value
                self.logger.error(f"Network connection is down between the hosts")
                raise Exception("Network connection error")
            self.logger.info("Network connection is up between all the hosts")

            # Set MTU on hosts and DPUs
            for node_group in self.node_group_dict.values():
                for host in node_group.hosts.values():
                    host.set_mtu()
                    if host.dpus:
                        [dpu.set_mtu_on_uplinks() for dpu in host.dpus.values()]
        else:
            super().test_setup(config_ports=False, loopback_supported=True, ib_supported=True)


    def test_logic(self):
        self.logger.info("Pcie Latency Benchmark Test - Start Execution")
        self.run_pcie_latency_benchmark()
        return self.test_status


    def run_pcie_latency_benchmark(self):
        """
        Run the pcie latency tests concurrently on all servers
        """
        results = {}
        super().run_test_on_all_servers(servers=self.node_group_dict.keys(), timeout=800, results=results)
        self.print_results(results)


    def calculate_pcie_latency(self, file_path, pcie):
        """Calculate the minimum, maximum and the average PCIe latency in nanoseconds.

        Args:
            file_path (str): The path of the output file of doca-telemetry-diag in CSV format.
            pcie (str): The PCIe address for which the latency is being calculated.

        Returns:
            tuple: A tuple containing the minimum, maximum, and average PCIe read latency.
        """

        # Doca-telemetry-diag file columns
        TOTAL_READ_NS_COL = 3
        TOTAL_READ_PACKETS_COL = 4
        MAX_READ_NS_COL = 5
        MIN_READ_NS_COL = 6

        with open(file_path, 'r') as file:
            csv_reader = csv.reader(file)
            next(csv_reader)  # Skip the header row
            data = list(csv_reader)

        self.logger.debug("Calculate the minimum latency for a single PCIe read from the device per PCIe link, ignoring 0 values.")
        min_value = min(int(row[MIN_READ_NS_COL]) for row in data if int(row[MIN_READ_NS_COL]) > 0)
        self.logger.debug(f"Minimum PCIe {pcie} latency: {min_value} ns")

        self.logger.debug("Calculate the maximum latency for a single PCIe read from the device per PCIe link.")
        max_value = max(int(row[MAX_READ_NS_COL]) for row in data)
        self.logger.debug(f"Maximum PCIe {pcie} latency: {max_value} ns")

        self.logger.debug("Calculate the average latency in nanoseconds.")
        total_packets = sum(int(row[TOTAL_READ_PACKETS_COL]) for row in data)
        total_ns = sum(int(row[TOTAL_READ_NS_COL]) for row in data)
        average = int(total_ns / total_packets)
        self.logger.debug(f"Average PCIe {pcie} latency: {average} ns")
        return  min_value, max_value , average


    def check_pcie_latency_result(self, idx, res_dict, state: constants.TestState, exit_status: int):
        """
        Collected data of minimum, maximum, and average PCIe read latency for the node group with the given idx.
        Fails when the average latency is too high or when the CSV file of one of the PCIe devices does not exist.

        Args:
            state (constants.TestState): Indicates the test condition (steady or stress) to determine latency thresholds and file naming.
            exit_status (int): The value of constants.TestStatus indicating the current test status.
        """
        test_failed = False
        failed_pcie_list = []

        for host_idx, host in enumerate(self.node_group_dict[idx].hosts.values(), start=1):
            filtered_pcie_list = self.filtered_pcie_dict[idx]['host_1'] if host_idx == 1 else self.filtered_pcie_dict[idx]['host_2']
            for pcie in filtered_pcie_list:
                    file = f"{self.base_dir}/{idx}_host{host_idx}_{pcie}_{state.value}.csv"

                    if not self.test_server_node.check_file_exists(file):
                        self.logger.error(f"{idx}- file- {file} is not exist")
                        min_val, max_val, avg_val, test_status = "N/A", "N/A", "N/A", constants.TestStatus.FAILED.value
                        exit_status = constants.ExitStatus.NVNETPERF_FAIL.value

                    else:
                        min_val, max_val, avg_val = self.calculate_pcie_latency(file, pcie)
                        if avg_val < self.average_pcie_read_latency:
                            test_status = constants.TestStatus.PASSED.value
                        else:
                            test_status = constants.TestStatus.FAILED.value
                            test_failed = True
                            failed_pcie_list.append((str(host.oob_address), pcie))

                    res_dict.setdefault(idx, {}).setdefault(state.value, []).append([str(host.oob_address), state.value, pcie, min_val, max_val, avg_val, test_status])

        if test_failed:
            failed_pcie_str = ", ".join([f"Host {host}, PCIe {pcie}" for host, pcie in failed_pcie_list])
            error = f"Alert: The average PCIe read latency time has exceeded {self.average_pcie_read_latency} nanoseconds for the following: {failed_pcie_str}."
            res_dict.setdefault(idx, {}).setdefault("errors", []).append(error)
            exit_status = constants.ExitStatus.NVNETPERF_PERF_ERROR.value

        return exit_status


    def print_results(self, res_dict):
        """
        Print summary tables of the pcie latency test results, up to two summery table per node group (steady state and under stress).
        """
        headers = ["Server", "State", "PCIe", "Min Latency [ns]", "Max Latency [ns]", "Average Latency [ns]", "Test Status"]
        for node_group_key in res_dict.keys():
            self.logger.info(f"PCIe read latency results for Node {node_group_key}:")
            # Iterate over the states dynamically (STEADY and STRESS)
            states = [
                (self.steady_state, constants.TestState.STEADY.value, "PCIe read latency on steady state"),
                (self.under_stress, constants.TestState.STRESS.value, "PCIe read latency under stress")
            ]

            for state_enabled, state_key, state_title in states:
                if state_enabled:
                    table = res_dict[node_group_key].get(state_key)
                    if table:
                        formatted_table = tabulate(table, headers, tablefmt="pretty")
                        self.logger.info(f"{state_title}:\n{formatted_table}")
                    else:
                        raise Exception(f"{state_title} data is missing for {node_group_key}")

            errors = res_dict[node_group_key].get("errors", [])
            for error in errors:
                self.logger.error(error)


    def run_test_on_server(self, idx, **kwargs):
        """
        Run the PCIe latency benchmark based on user preference: steady state, under stress, or both.
        """
        self.logger.info(f"Running PCIe latency benchmark on {idx}")
        results = kwargs['results']
        node_group= self.node_group_dict[idx]
        # Ensure all DPUs are in Trusted Mode before proceeding with the test.
        for host in node_group.hosts.values():
            if not host.all_dpus_trusted_mode():
                self.logger.error(f"{idx}- A card in Zero-Trust Mode was found. The DOCA telemetry diag test only supports DPUs in Trusted Mode")
                self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
                return

        self.filtered_pcie_dict.setdefault(idx, {}).setdefault('host_1', self.filter_pcie_addresses(node_group.hosts['host_1'].pcie_address))
        if 'host_2' in node_group.hosts:
            self.filtered_pcie_dict.setdefault(idx, {}).setdefault('host_2', self.filter_pcie_addresses(node_group.hosts['host_2'].pcie_address))

        self.compile_telemetry_diag_sample(idx)
        exit_status= constants.ExitStatus.NVNETPERF_SUCCESS.value
        if self.under_stress:
            self.run_telemetry_diag_stress(idx)
            exit_status= self.check_pcie_latency_result(idx, results, constants.TestState.STRESS, exit_status)
        if self.steady_state:
            self.run_telemetry_diag_sample(idx, constants.TestState.STEADY)
            exit_status= self.check_pcie_latency_result(idx, results, constants.TestState.STEADY, exit_status)
        self.cleanup_telemetry_diag(idx)
        return exit_status


if __name__ == "__main__":
    test = PcieLatencyTest()
    test.args_execute()
    exit(test.execute())