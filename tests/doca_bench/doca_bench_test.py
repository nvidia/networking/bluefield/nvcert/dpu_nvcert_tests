#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

"""
DOCA Bench Test -
1. Build DOCA Bench Command depends on the parsed parameters
2. Run DOCA Bench command over the defined direction
3. Return throughput results and the expected ones for comparison
"""

import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    from tabulate import tabulate
    from enum import Enum
    from common.basic_classes import nvnetperf_base_class as nbc
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc

doca_bench_bf3_throughput_expectations = {
    "doca_dma": {
        "Host-Host": 320,
        "DPU-Host" : 370,
        "Host-DPU" : 380
    },
    "doca_ec_create": {
        "Host-Host": 150,
        "DPU-Host" : 200,
        "Host-DPU" : 150
    },
    "doca_decompress_lz4": {
        "Host-Host": 310,
        "DPU-Host" : 370,
        "Host-DPU" : 370
    },
    "doca_decompress_deflate": {
        "Host-Host": 210,
        "DPU-Host" : 210,
        "Host-DPU" : 210
    },
    "doca_aes_gcm_encrypt": {
        "Host-Host": 130,
        "DPU-Host" : 130,
        "Host-DPU" : 140
    },
    "doca_aes_gcm_decrypt": {
        "Host-Host": 130,
        "DPU-Host" : 130,
        "Host-DPU" : 140
    },
}

class CompressionMode(str, Enum):
    LZ4 = "lz4",
    DEFLATE  = "deflate"

class RunOn(str, Enum):
    HOST = "Host",
    DPU  = "DPU"

class DocaBenchTest(nbc.NVNetPerfBaseClass):
    """
    DOCA Bench (DMA, Decompress, AES_GCM, EC) with/without remote buffers.
    From Host-Host, Host-DPU, or DPU-Host running on the Host.
    """
    _LZ4_COMPRESSED_FILE = "compressed_files_16k.txt"
    _DEFLATE_COMPRESSED_FILE = "deflate_compressed_files_16k.txt"
    _ENCRYPTED_FILE = "encrypted_16384.txt"
    _DPU_PCIE_ADDRESS = "03:00.0"

    def __init__(self):
        super().__init__()
        self.doca_bench_files_path = f"{constants.NVNETPERF_ROOT}/tests/doca_bench/files/doca_bench/"
        self.test_run_name = "doca_bench"


    def args_test(self):
        super().args_test()
        self.parser.add_argument("--bench_name", required=True, choices=["doca_dma", "doca_decompress_lz4", "doca_decompress_deflate", "doca_ec_create", "doca_aes_gcm_encrypt", "doca_aes_gcm_decrypt"],
                        help="DOCA Bench test - Specify the test-name")
        self.parser.add_argument("--direction", default="Host-Host", choices=["Host-Host", "DPU-Host", "Host-DPU"],
                        help="DOCA Bench test - Specify the test-direction.")


    def args_parsed(self):
        super().args_parsed()
        self.bench_name = self.args.bench_name
        self.direction = self.args.direction
        self.test_name = f"doca_bench_{self.bench_name.split('doca_')[1]}_{self.direction.replace('-', '_')}"


    def test_setup(self):
        self.logger.info(f"Beginning preparation of DOCA Bench {self.bench_name} from {self.direction} Tests")
        super().test_setup()
        if not self.check_doca_path_exists():
            self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
            raise Exception("DOCA Bench path does not exist! Please check DOCA installation.")


    def test_logic(self):
        initiator, target = self.direction.split("-")
        if initiator != target:
            self.server_handler.setup_dpu_ssh_keys(self.node_group_dict)
        self.run_doca_bench()
        return self.test_status


    def run_doca_bench(self):
        """
        Run Doca Bench on all servers
        """
        results = {}
        super().run_test_on_all_servers(servers=self.node_group_dict.keys(), timeout=800, results=results)
        self.print_results(results)


    def print_results(self, res_dict):
        """
        Print test's results
        """
        for node_group_key in self.node_group_dict.keys():
            table = tabulate(res_dict[node_group_key], headers="keys", tablefmt="orgtbl")
            self.logger.info(f"\nDOCA Bench Test Summary for {node_group_key}:\n{table}\n")


    def check_doca_path_exists(self):
        """
        Check DOCA is installed successfully in the global path
        Returns:
        - True/False (bool): Represents doca path exists.
        """
        for node_group in self.node_group_dict.values():
            host = node_group.hosts['host_1']
            rc = host.sudo(f"ls {constants.TestsPaths.DOCA_DIR.value}/tools/doca_bench && echo 0 || echo -1").split("\n")[-1].strip()
            if float(rc):
                self.logger.error(f"Test Failed: DOCA Bench path does not exist! Please check DOCA installation on {host.oob_address}")
                return False
            else:
                return True


    def run_test_on_server(self, idx, **kwargs):
        """
        Run DOCA Bench on node group (DMA, Decompress, EC, AES-GCM) Host-Host or Host-DPU or DPU-Host and measure network throughput.

        Returns:
        - throughput (list): List of throughput values.
        - expected (float): Expected throughput value.
        """
        res_dict = kwargs['results']
        exit_status = constants.ExitStatus.NVNETPERF_FAIL.value
        node_group = self.node_group_dict[idx]
        host = node_group.hosts['host_1']
        bf_pci = self._DPU_PCIE_ADDRESS
        r_obj = []
        job_size = 16384
        duration = 5
        initiator, target = self.direction.split("-")
        expected = float(doca_bench_bf3_throughput_expectations[self.bench_name].get(self.direction, 0))

        cmd = f" {constants.TestsPaths.DOCA_DIR.value}/tools/doca_bench --run-limit-seconds {duration} "

        # Build DOCA Bench execution command based on the given bench_name
        if "doca_dma" in self.bench_name:
            cmd += f" --pipeline-steps {self.bench_name} --job-output-buffer-size {job_size} --uniform-job-size {job_size}  --data-provider random-data "
        elif "doca_ec" in self.bench_name:
            ec_mode = self.bench_name.split("_")[-1]
            cmd += f" --pipeline-steps doca_ec::{ec_mode} --attribute doca_ec.matrix_type=cauchy --attribute doca_ec.data_block_count=16 --attribute doca_ec.redundancy_block_count=4 --job-output-buffer-size {job_size*2} --uniform-job-size {job_size}  --data-provider random-data "
        elif "doca_aes_gcm" in self.bench_name:
            host.put(f"{self.doca_bench_files_path}/aes_gcm/{self._ENCRYPTED_FILE}", constants.TestsPaths.FILES_DIR.value, None)
            host.put(f"{self.doca_bench_files_path}/aes_gcm/aes128.key", constants.TestsPaths.FILES_DIR.value, None)
            aes_gcm_mode = self.bench_name.split("_")[-1]
            aes_gcm_input_file = f" --data-provider random-data --uniform-job-size {job_size} "
            if aes_gcm_mode == "decrypt":
                aes_gcm_input_file = f" --data-provider file-set --data-provider-input-file {self._ENCRYPTED_FILE} "
            cmd += f" --pipeline-steps doca_aes_gcm::{aes_gcm_mode}  --input-cwd {constants.TestsPaths.FILES_DIR.value} --job-output-buffer-size {2*job_size}  {aes_gcm_input_file} --attribute doca_aes_gcm.key-file='aes128.key' "
        elif "doca_decompress" in self.bench_name:
            compress_mode = self.bench_name.split("_")[-1]
            compress_algo = " doca_compress.algorithm=deflate "
            compressed_file = self._DEFLATE_COMPRESSED_FILE
            if compress_mode == CompressionMode.LZ4:
                compress_algo = " doca_compress.algorithm=lz4_stream --attribute doca_compress.algorithm.has_block_checksum=false --attribute doca_compress.algorithm.are_blocks_independent=false"
                compressed_file = self._LZ4_COMPRESSED_FILE
            host.put(f"{self.doca_bench_files_path}/compress/{self.compressed_file}", constants.TestsPaths.FILES_DIR.value, None)
            cmd += f" --pipeline-steps doca_compress::decompress --attribute {compress_algo} --data-provider file-set --input-cwd {constants.TestsPaths.FILES_DIR.value} --data-provider-input-file {compressed_file}  --job-output-buffer-size {job_size*2}"

        pci_list = []
        if initiator != target:
            remote_buffers = "--use-remote-input-buffers" if initiator == RunOn.DPU else "--use-remote-output-buffers"
            for dpu in host.dpus.values():
                cmd_param = ""
                host_pcie = self.get_card_pci(host, dpu)
                pci_list.append(host_pcie)
                core = self.get_test_core(host, host_pcie)
                cmd_param = (f" --device {host_pcie} --core-list {core} {remote_buffers} "
                        f"--companion-connection-string proto=tcp,mode=dpu,dev={bf_pci},user={dpu.uname},addr={dpu.oob_address},port=12345 "
                        f"--attribute dopt.companion_app.path={constants.TestsPaths.DOCA_DIR.value}/tools/doca_bench_companion")
                full_cmd = cmd + cmd_param
                self.logger.info(f"Running: {full_cmd}")
                r_obj.append(host.sudo(full_cmd))
        else:
            host = node_group.hosts['host_1']
            core = self.get_test_core(host, host.pcie_address[0])
            pci_list.append(host.pcie_address[0])
            cmd += f" --device {host.pcie_address[0]} --core-list {core}"
            self.logger.info(f"Running: {cmd}")
            r_obj.append(host.sudo(cmd))
        # Get throughput from r_obj
        throughput_pat = r"Egress rate: \d*\.\d+|\d+\.?\d* Gib/s"
        throughput = []
        test_results = []
        for obj in r_obj:
            match = re.search(throughput_pat, obj)
            if match:
                rate = match.group().split("Gib/s")[0]
                throughput.append(round(float(rate), 2))
            else:
                throughput.append("N/A")
        for i,bw in enumerate(throughput):
            if bw >= ((constants.TestThroughputPassRatio.DOCA_BENCH_THROUGHPUT.value * expected) / 100):
                test_status = constants.TestStatus.PASSED.value
                exit_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
            else:
                test_status = constants.TestStatus.FAILED.value
                if bw == "N/A":
                    exit_status = constants.ExitStatus.NVNETPERF_PARAM_ERROR.value
                else:
                    exit_status = constants.ExitStatus.NVNETPERF_PERF_ERROR.value
            data = {
                "Bench Name" : self.bench_name,
                "Core"       : core,
                "Duration"   : duration,
                "Job Size"   : job_size,
                "Initiator"  : initiator,
                "Target"     : target,
                "Device"     : f"{pci_list[i]}",
                "BW"         : bw,
                "Expected BW": expected,
                "Test Status": test_status
            }
            test_results.append(data)
            self.set_test_json_data(data, bw, expected)
        res_dict[idx] = test_results
        return exit_status


    def get_test_core(self, host, host_pcie):
        """
        Return core of the NUMA connected to the Host PCIE. Neglect core 0.
        """
        numa_number = str(host.sudo(f"mst status -v | grep {host_pcie} | awk '{{print $6}}'")).strip()
        cmd = f"lscpu | grep -i 'NUMA node0 CPU(s)'" if '-1' in numa_number else f"lscpu | grep -i 'NUMA node{numa_number} CPU(s)'"
        r_obj = host.sudo(cmd)
        cores = r_obj.strip().split(":")[1].split(",")
        cores_list = self.get_numa_cores_list(cores[0]) if '-' in cores[0] else cores
        running_core = cores_list[0] if cores_list[0] != "0" else cores_list[1]
        return running_core


    def get_numa_cores_list(self, cores_range):
        """
        Return List of cores from cores range.
        Example: cores_range = 1-5
        return value: [1, 2, 3, 4, 5]
        """
        start_val, end_val = cores_range.split('-')
        cores_list = [str(i) for i in range(int(start_val), int(end_val) + 1)]
        return cores_list


    def get_card_pci(self, host, dpu):
        """
        Return Host PCIE address connected to the used BF card.
        Steps:
            - Get card PSID from the BF.
            - Get MST Device for that card PSID on the Host.
            - Return PCIE Address connected of MST device.
        """
        cmd = f"mlxfwmanager | grep PSID"
        obj = dpu.sudo(cmd)
        psid = str(re.search(r"MT_\d+", obj).group())
        obj = host.sudo("mlxfwmanager").split("\n")
        pci_dev = ""
        for idx, line in enumerate(obj):
            if psid in line:
                res = re.search(r"mt.*", obj[idx + 1])
                pci_dev = str(res.group()).strip()
                break
        card_pcie = host.sudo(f"mst status -v | grep {pci_dev} | awk '{{print $3}}' | grep -i '00.0'")
        return str(card_pcie).strip()


    def set_test_json_data(self, results, bw, expected):
        info = ""
        status = True
        exp_bw = (constants.TestThroughputPassRatio.DOCA_BENCH_THROUGHPUT.value * expected) / 100
        if bw < exp_bw:
            info = f"Throughput {bw} Gb/sec is lower than minimal required throughput {exp_bw} Gb/sec"
            status = False
        results.pop("Expected BW")
        data = results
        threshold = {
            "min_throughput": expected,
            "info": info,
        }
        self.subtests_list = self.json_helper.create_json_subtest(self.bench_name, data, threshold, status)



if __name__ == "__main__":
    test = DocaBenchTest()
    test.args_execute()
    exit(test.execute())