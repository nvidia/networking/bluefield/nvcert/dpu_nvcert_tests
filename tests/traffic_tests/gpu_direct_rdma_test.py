#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2023-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    from tests.traffic_tests import rdma_test as rt
    from common.utils import node
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class GpuDirectRdmaTest(rt.RdmaTest):
    """
    GPU Direct RDMA Test
    """

    test_suite = {
        "mode": "ovs",                         
        "skip_speed_check": False,             
        "disable_ports_config": False,        
        "disable_ports_deconfig": False,       
        "allow_gpu_node_relation": True       # Allow 'NODE' relation between GPU and NIC (Required for GB200)
    }

    def __init__(self):
        super().__init__()
        self.test_name = "gpu_direct_rdma_benchmark"
        self.test_run_name = "gpu_direct_rdma_bw_lat_test"

    def args_test(self):
        super().args_test()
        self.parser.add_argument("--allow_gpu_node_relation",
                         action="store_true",
                         help="Allow 'NODE' relation between GPU and NIC. Required for GB200.")

    def args_parsed(self):
        super().args_parsed()
        self.allow_gpu_node_relation = self.args.allow_gpu_node_relation

    def _validate_perftest_cuda_support(self, host):
        """
        Validates that Perftest on the specified host is built with CUDA support
        """
        output = host.sudo("ib_write_bw -h | grep use_cuda")
        if output.return_code:
            raise Exception(f"[{host.oob_address}] Perftest must be built with CUDA support to enable GPU-Direct functionality. Please refer to the README for detailed build instructions.")

    def test_setup(self):
        self.logger.info("GDR Tests - Start Test Preparation")
        super().test_setup()
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                if not host.check_gpus_available():
                    self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
                    raise Exception(f"No Available GPUs on {host.oob_address}")
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                host.load_nvidia_peermem()
                self._validate_perftest_cuda_support(host)
            if not self.check_gpu_dpu_conn(node_group):
                raise Exception(f"Cannot run GDR, GPU and NIC (DPU/CX) must be connected on the same NUMA")

    def test_logic(self):
        self.logger.info("GDR Benchmark Test - Start Execution")
        is_GDR = True
        single_port = False
        self.run_rdma_benchmark(single_port, is_GDR, self.allow_gpu_node_relation)
        return self.test_status

    def check_gpu_dpu_conn(self, node_group):
        table_file = f"{constants.TestsPaths.FILES_DIR.value}/table.txt"
        for host in node_group.hosts.values():
            output = host.run(f'nvidia-smi topo -mp | grep -m 1 -B 1000 "Legend:" | head -n -2 > {table_file}', hide=False)
            output = host.run(f"sed -i 's/\x1B\[[0-9;]*[a-zA-Z]//g' {table_file}", hide=False)
            host.get(table_file, table_file)
            nics, gpus = [], []
            for mlx_dev in host.mlxdevs:
                command = f"nvidia-smi topo -mp | grep {mlx_dev} | awk '{{print $1}}' | grep -o 'NIC[0-9]\+'"
                output = host.run(command, hide=False).stdout.rstrip("\n")
                nics.append(output)
            command = "nvidia-smi topo -mp | grep -o 'GPU[0-9]\+' | sort | uniq"
            output = host.run(command, hide=False).stdout.rstrip("\n")
            gpus = output.split("\n")
            with open(f"{constants.TestsPaths.FILES_DIR.value}/table.txt", 'r') as f:
                lines = f.readlines()
            header = lines[0].split()
            allowed_relations = ['PIX', 'PXB']
            if self.allow_gpu_node_relation:
                allowed_relations.append('NODE')
            for nic in nics:
                row = [line.split() for line in lines[1:] if line.startswith(nic)][0]
                related_gpus_num = 0
                for gpu in gpus:
                    conn_value = row[header.index(gpu)+1]
                    if conn_value in allowed_relations:
                        related_gpus_num += 1
                if related_gpus_num == 0:
                    self.logger.critical(f"Cannot run GDR, GPU and {nic} (DPU/CX) must be connected on the same NUMA on server {host.oob_address}")
                    self.test_status = constants.ExitStatus.NVNETPERF_COMM_ERROR.value
                    return False
        return True

    def post_test_logic(self):
        super().post_test_logic()
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                host.restore_nvidia_peermem()
    
    def set_variables_from_file(self, inventory, params):
        super().set_variables_from_file(inventory, params)
        self.allow_gpu_node_relation = params["tests"][self.test_run_name]["allow_gpu_node_relation"]


if __name__ == "__main__":
    test = GpuDirectRdmaTest()
    test.args_execute()
    exit(test.execute())