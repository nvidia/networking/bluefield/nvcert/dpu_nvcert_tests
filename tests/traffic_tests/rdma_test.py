#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    from tabulate import tabulate
    from common.basic_classes import traffic_base_class as tbc
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class RdmaTest(tbc.TrafficBaseClass):
    """
    RDMA Benchmark Test
    """

    test_suite =  {
        "mode": "ovs",                          # Specify the test-traffic mode ["ovs", "hbn", "ipsec"]                 
        "skip_speed_check": False,              # Skip pre-test ports and cables speed check          
        "disable_ports_config": False,          # Disable the ports configuration step          
        "disable_ports_deconfig": False,        # Disable the ports deconfiguration step     
        "duration": 30,                         # Specify the test-traffic duration in seconds (default: 120 sec)      
                
        "qp": 4,                                # Specify QPs number (default: 4 QPs per device, max: 64)                             
        "bw_message_size_list": 65536,          # Provide a list of message sizes for BW tests (default: 65536)
        "lat_message_size_list": 2,             # Provide a list of message sizes for latency tests (default: 2)     
        "extra": "",                            # Add extra parameters          
        "tests": [
            "ib_write_bw", "ib_write_lat", "ib_send_bw", "ib_send_lat", "ib_read_bw", "ib_read_lat"
        ]  # List of RDMA tests to run
    }

    def __init__(self):
        super().__init__()
        self.test_name = "rdma_benchmark"
        self.test_run_name = self.test_name


    def args_test(self):
        super().args_test()
        self.parser.add_argument("--qp", type=int,
                        help="RDMA tests - sepecify QPs number (default: 4 QPs per device, max: 64)")
        self.parser.add_argument("--bw_message_size_list",
                            help="RDMA tests - provide a comma separated message size list to run bw tests (default: 65536)")
        self.parser.add_argument("--lat_message_size_list",
                            help="RDMA tests - provide a comma separated message size list to run lat tests (default: 2)")
        self.parser.add_argument("--extra", "-e", type=str,
                            help="RDMA tests: add extra parameters")
        self.parser.add_argument("--tests", "-t", type=str,
                            help="RDMA tests: list of RDMA tests to run, (e.g: ib_write_bw, ib_write_lat, ib_send_bw, ib_send_lat, ib_read_bw, ib_read_lat), ex: --tests 'ib_write_bw,ib_read_lat'")
        self.parser.add_argument("--duration", "-d",
                        help="Specify the tests duration in seconds (default: 30 sec per test)")

    def args_parsed(self):
        super().args_parsed()
        self.qp = self.args.qp
        self.bw_message_size_list = self.args.bw_message_size_list
        self.lat_message_size_list = self.args.lat_message_size_list
        self.extra = self.args.extra
        self.tests = self.args.tests
        self.duration = self.args.duration


    def test_setup(self):
        self.logger.info("RDMA benchmark - Start Test Preparation")
        if self.mode:
            if self.mode == constants.TrafficMode.OVS.value:
                self.test_run_name = "rdma_bw_lat_test"
                super().test_setup(loopback_supported=True, ib_supported=True)
            elif self.mode == constants.TrafficMode.HBN.value:
                super().test_setup()
                # Setup an EVPN VxLAN bridge
                if not self.create_evpn_vxlan_bridge():
                    raise Exception("Failed to setup EVPN VxLAN Bridge")
            elif self.mode == constants.TrafficMode.IPSEC.value:
                self.test_run_name = "rdma_ipsec_test"
                super().test_setup()
                if self.enable_passwordless_access:
                    # IPsec tests require passwordless access to the DPUs, setup the ssh keys
                    self.server_handler.setup_dpu_ssh_keys(self.node_group_dict)
            # Check if ping works
            if self.mode != constants.TrafficMode.HBN.value:
                if not self.run_ping_with_backoff():
                    self.test_status =  constants.ExitStatus.NVNETPERF_COMM_ERROR.value
                    raise Exception("Network connection error")
                self.logger.info("Network connection is up between all the hosts")
        # Set MTU on hosts and DPUs
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                host.set_mtu()
                if host.dpus:
                    [dpu.set_mtu_on_uplinks() for dpu in host.dpus.values()]


    def test_logic(self):
        if self.mode == constants.TrafficMode.OVS.value:
            self.logger.info("RDMA OVS Benchmark Test - Start Execution")
            is_GDR = False
            single_port = False
            self.run_rdma_benchmark(single_port, is_GDR)
        elif self.mode == constants.TrafficMode.HBN.value:
            self.logger.info("RDMA HBN  Test - Start Execution")
            single_port = True
            is_GDR = False
            self.test_status = self.run_test_on_server('node_group_1', single_port=True, is_GDR=False)
        elif self.mode == constants.TrafficMode.IPSEC.value:
            self.logger.info("RDMA IPSEC Benchmark Test - Start Execution")
            self.run_rdma_benchmark()
        return self.test_status


    def run_rdma_benchmark(self, single_port = False, is_GDR = False, allow_gpu_node_relation = False):
        """
        Run the RDMA tests concurrently on all servers
        """
        tests_list = ["ib_write_bw", "ib_read_bw", "ib_send_bw", "ib_read_lat", "ib_send_lat"] if is_GDR else ["ib_write_bw", "ib_read_bw", "ib_send_bw", "ib_write_lat", "ib_read_lat", "ib_send_lat"]
        tests = self.tests.split(",") if self.tests else tests_list
        results = {}
        for test in tests:
            self.tests = test
            super().run_test_on_all_servers(servers=self.node_group_dict.keys(), timeout=800, results=results, single_port=single_port, is_GDR=is_GDR, allow_gpu_node_relation=allow_gpu_node_relation)
        self.print_results(results, tests)


    def run_test_on_server(self, idx, **kwargs):
        """
        Run rdma benchmark over whatever is configured on the DPU
        """
        self.logger.info(f"Run RDMA benchmark on {idx}")
        results = kwargs['results']
        single_port = kwargs['single_port']
        is_GDR = kwargs['is_GDR']
        allow_gpu_node_relation = kwargs['allow_gpu_node_relation']
        cmd = self.generate_base_rdma_cmd(idx, single_port)
        cmd += f" --qp={self.qp}" if self.qp else ""
        cmd += f" --tests={self.tests}" if self.tests else ""
        cmd += f" --bw_message_size_list={self.bw_message_size_list}" if self.bw_message_size_list else ""
        cmd += f" --lat_message_size_list={self.lat_message_size_list}" if self.lat_message_size_list else ""
        cmd += f" --duration={self.duration}" if self.duration else ""
        cmd += f" {self.extra}" if self.extra else ""
        if is_GDR:
            cmd += " --use_cuda "
            if allow_gpu_node_relation:
                cmd += " --allow_gpu_node_relation "
            if self.tests == "ib_send_lat" and not self.lat_message_size_list:
                self.lat_message_size_list = 64
        if self.mode == constants.TrafficMode.IPSEC.value:
            node_group = self.node_group_dict[idx]
            if node_group.topology == constants.HostNodeTopology.B2B.value:
                host_1 = node_group.hosts['host_1']
                host_2 = node_group.hosts['host_2']
                cmd += " --ipsec "
                for host in [host_1, host_2]:
                    dpu_ip = ""
                    ports = ""
                    for dpu in host.dpus.values():
                        dpu_ip += f"{dpu.uname}@{dpu.oob_address},"
                        port_num_cmd = "mst status -v | grep BlueField3 | wc -l"
                        output = dpu.sudo(port_num_cmd)
                        if "2" in output:
                            dpu_ip += f"{dpu.uname}@{dpu.oob_address},"
                            ports += "p0,p1,"
                        elif "1" in output:
                            ports += "p0,"
                    cmd += f" {dpu_ip[:-1]} {ports[:-1]} "
            else:
                raise Exception("RDMA IPSec isn't supported for loopback")
        self.logger.info(f"Running: {cmd}")
        output = self.test_server_node.run(cmd, warn = True, hide = "stderr", pty = True)
        return self.check_res_for_node_group(output, idx, results)


    def check_res_for_node_group(self, output, idx, res_dict):
        """
        Check RDMA test results for the node group with the given idx and the given test (self.tests), based on the log from NGC and update the results dictionary
        """
        node_group = self.node_group_dict[idx]
        host_1 = node_group.hosts['host_1']
        server1 = f"{host_1.oob_address}"

        perf_error_count = 0
        test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
        lat_linerate = {"ib_send_lat": self.send_lat_pass_rate, "ib_write_lat": self.write_lat_pass_rate, "ib_read_lat": self.read_lat_pass_rate}
        self.ovs_pass_rate = None
        special_devs_dict = self.create_special_devices_dict(node_group.hosts.values())

        # Define mlx_dev_src <-> mlx_dev_dst for B2B and LB connections to parse the log
        if node_group.topology == constants.HostNodeTopology.Loopback.value:
            mlx_devs_src = host_1.mlxdevs[0::2]
            mlx_devs_dst = host_1.mlxdevs[1::2]
            server2 = server1
        else:
            host_2 = node_group.hosts['host_2']
            mlx_devs_src = host_1.mlxdevs
            mlx_devs_dst = host_2.mlxdevs
            server2 = f"{host_2.oob_address}"
        num_devices = len(mlx_devs_src)

        threshold_info = ""
        case_status = False
        test_result = ""
        exp_res = "N/A"
        mode = constants.TrafficTestType.BW.value
        for [mlx_dev1,mlx_dev2] in zip(mlx_devs_src, mlx_devs_dst):
            devices = f"{mlx_dev1} <-> {mlx_dev2}"
            res_lines = self.find_lines_before_string(output, f"{self.tests} - ", 3*num_devices)
            bw_pat = re.compile(fr'Device {mlx_dev1} reached (\d+) Gb/s \(max possible: (\d+) Gb/s\)')
            bw_match = bw_pat.search(res_lines)
            lat_pat = re.compile(fr'{mlx_dev1} avg. latency: (\d+(\.\d+)?) μs.')
            lat_match = lat_pat.search(res_lines)

            case_status = False
            threshold_info = ""
            test_result = ""

            if bw_match:
                self.ovs_pass_rate = constants.TestThroughputPassRatio.OVS_PASS_THROUGHPUT.value/100 * int(bw_match.group(2))
                exp_res = self.ovs_pass_rate
                bw = float(bw_match.group(1)) + self.find_b3240_bw_rdma(special_devs_dict.get("B3240", {}), mlx_dev1, mlx_dev2, res_lines, server1, server2)
                test_result = bw
                if bw >= self.ovs_pass_rate:
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["bw_bool"] = "Passed"
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["bw"] = f"{float(bw_match.group(1))}  (Expected > {self.ovs_pass_rate})"
                    case_status = True
                else:
                    perf_error_count += 1
                    threshold_info = f"Throughput BW {float(bw_match.group(1))} Gb/s is lower than minimal required throughput {self.ovs_pass_rate} Gb/s"
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["bw_bool"] = "Failed"
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["bw"] = f"{float(bw_match.group(1))}  (Expected > {self.ovs_pass_rate})"

            elif lat_match:
                mode = constants.TrafficTestType.LAT.value
                lat = float(lat_match.group(1))
                test_result = lat
                exp_res = lat_linerate[self.tests.split('-')[0].strip()]
                if lat <= exp_res:
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["lat_bool"] = "Passed"
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["lat"] = f"{lat}  (Expected < {exp_res})"
                    case_status = True
                else:
                    perf_error_count += 1
                    threshold_info = f"Latency {float(lat_match.group(1))} us is more than maximal required latency {exp_res} us"
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["lat_bool"] = "Failed"
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["lat"] = f"{lat}  (Expected < {exp_res})"
            else:
                if "bw" in self.tests:
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["bw_bool"] = "Failed"
                    res_dict[idx][devices][self.tests]["bw"] = "N/A"
                elif "lat" in self.tests:
                    res_dict.setdefault(idx, {}).setdefault(devices, {}).setdefault(self.tests, {})["lat_bool"] = "Failed"
                    res_dict[idx][devices][self.tests]["lat"] = "N/A"
                test_status = constants.ExitStatus.NVNETPERF_FAIL.value

            self.set_subtest_data(self.tests.split('-')[0],
                                    test_result,
                                    {"server1":server1, "dev1":mlx_dev1, "server2":server2, "dev2":mlx_dev2},
                                    mode,
                                    {"expected": exp_res, "info":threshold_info},
                                    case_status,
                                    idx)

            if perf_error_count > 0:
                test_status = constants.ExitStatus.NVNETPERF_PERF_ERROR.value
        return test_status


    def print_results(self, res_dict, tests):
        """
        Print the dictionary of the results as tables (table per node group)
        Args: tests - the tests' list to include in the table
        """
        headers1 = ['Servers', 'Devices', 'PING']
        headers2 = ['Servers', 'Devices']
        tests_headers = [" ".join(test.split("_")[1:]).title() for test in tests]
        [headers1.append(test_name) for test_name in tests_headers]
        tests_headers = [f"{item} [{'Gb/s' if 'Bw' in item else 'μs'}]" for item in tests_headers]
        [headers2.append(test_name) for test_name in tests_headers]

        for node_group_key in res_dict.keys():
            pass_fail_table = []
            results_table = []
            pass_fail_row = []
            results_row = []
            pass_fail_table.append(headers1)
            results_table.append(headers2)
            server1 = self.node_group_dict[node_group_key].hosts['host_1'].oob_address
            if self.node_group_dict[node_group_key].topology == constants.HostNodeTopology.B2B.value:
                server2 = self.node_group_dict[node_group_key].hosts['host_2'].oob_address
            else:
                server2 = server1
            for devs in res_dict[node_group_key].keys():
                pass_fail_row.append(f"{server1} <-> {server2}")
                results_row.append(f"{server1} <-> {server2}")
                pass_fail_row.append(f"{devs}")
                results_row.append(f"{devs}")
                pass_fail_row.append(f"Passed") # ping test result
                for test in tests:
                    if "bw" in test:
                        pass_fail_row.append(res_dict[node_group_key][devs][test]["bw_bool"])
                        results_row.append(res_dict[node_group_key][devs][test]["bw"])
                    elif "lat" in test:
                        pass_fail_row.append(res_dict[node_group_key][devs][test]["lat_bool"])
                        results_row.append(res_dict[node_group_key][devs][test]["lat"])
                pass_fail_table.append(pass_fail_row)
                results_table.append(results_row)
                pass_fail_row = []
                results_row = []

            column_alignments = ["center"] * len(headers1)
            self.logger.info(f"\n\nTest Summary:\n\n{tabulate(pass_fail_table, headers='firstrow', tablefmt='pretty', colalign=column_alignments)}\n")
            column_alignments = ["center"] * len(headers2)
            self.logger.info(f"\n\nTest Detailed Results:\n\n{tabulate(results_table, headers='firstrow', tablefmt='pretty', colalign=column_alignments)}\n\n")


    def find_lines_before_string(self, content, search_str, num_lines):
        """
        Find #(=num_lines) of lines that appear before search_str in content
        """
        result_lines = []
        lines = content.splitlines()
        for i, line in enumerate(lines):
            if search_str in line:
                start = max(0, i - num_lines)
                result_lines.append(lines[start:i])

        joined_sublists = ['\n'.join(sublist) for sublist in result_lines]
        return '\n\n'.join(joined_sublists)


    def find_b3240_bw_rdma(self, special_devs_dict, mlx_dev1, mlx_dev2, res, server1, server2):
        """
        Find the BW of the second port of dual port B3240 in RDMA test
        """
        # If mlx_dev1 is connected to a B3240 DPU, update bw to reflect that
        if (server1 in special_devs_dict.keys()) and any(mlx_dev1 in sublist for server1 in special_devs_dict.values() for sublist in server1):
            mlx_dev = mlx_dev1
            self.logger.info(f"{mlx_dev1} is connected to B3240 DPU, calculating BW")
        # If mlx_dev2 is connected to a B3240 DPU, update bw to reflect that
        elif (server2 in special_devs_dict.keys()) and any(mlx_dev2 in sublist for server2 in special_devs_dict.values() for sublist in server2):
            mlx_dev = mlx_dev2
            self.logger.info(f"{mlx_dev2} is connected to B3240 DPU, calculating BW")
        else:
            return 0
        # Find the other port of this DPU and sum up BW
        second_dev = next((sublist[1] for nested_list in special_devs_dict.values() for sublist in nested_list if sublist[0] == mlx_dev), None)
        if not second_dev:
            second_dev = next((sublist[0] for nested_list in special_devs_dict.values() for sublist in nested_list if sublist[1] == mlx_dev), None)
        second_dev_bw_pat = re.compile(fr'Device {second_dev} reached (\d+) Gb/s \(max possible: (\d+) Gb/s\)')
        second_match = second_dev_bw_pat.search(res)
        if second_match:
            return float(second_match.group(1))
        return 0




    def set_variables_from_file(self, inventory, params):
        super().set_variables_from_file(inventory, params)
        self.qp = params["tests"][self.test_run_name]["qp"]
        self.bw_message_size_list = params["tests"][self.test_run_name]["bw_message_size_list"]
        self.lat_message_size_list = params["tests"][self.test_run_name]["lat_message_size_list"]
        self.extra = params["tests"][self.test_run_name]["extra"]
        self.tests = params["tests"][self.test_run_name]["tests"]
        self.duration = params["tests"][self.test_run_name]["duration"]


if __name__ == "__main__":
    test = RdmaTest()
    test.args_execute()
    exit(test.execute())