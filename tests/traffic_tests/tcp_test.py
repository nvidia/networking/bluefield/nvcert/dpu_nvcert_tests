#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import re
    from tabulate import tabulate
    from common.basic_classes import traffic_base_class as tbc
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class TcpTest(tbc.TrafficBaseClass):
    """
    TCP Benchmark Test
    """

    test_suite = {
        "mode": "ovs",                          
        "skip_speed_check": False,             
        "disable_ports_config": False,        
        "disable_ports_deconfig": False,      
        "duration": 120                         
    }

    def __init__(self):
        super().__init__()
        self.test_name = "tcp_benchmark"
        self.test_run_name = self.test_name


    def args_test(self):
        super().args_test()
        self.parser.add_argument("--duration", "-d",
                        help="Specify the test-traffic duration in seconds (default: 120 sec)")


    def args_parsed(self):
        super().args_parsed()
        self.duration = self.args.duration


    def test_setup(self):
        self.logger.info("TCP benchmark - Start Test Preparation")
        self.benchmark_tcp_test = f"{self.root_dir}/ngc_multinode_perf/ngc_tcp_test.sh"
        if self.mode == constants.TrafficMode.OVS.value:
            self.test_run_name = "tcp_bw_test"
            super().test_setup(loopback_supported=True)
        else:
            if self.mode == constants.TrafficMode.IPSEC.value:
                self.test_run_name = "tcp_ipsec_test"
            super().test_setup()
        # Set MTU on hosts and DPUs
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                host.set_mtu()
                if host.dpus:
                    [dpu.set_mtu_on_uplinks() for dpu in host.dpus.values()]
        if self.enable_passwordless_access and self.mode == constants.TrafficMode.IPSEC.value:
            self.server_handler.setup_dpu_ssh_keys(self.node_group_dict)
        # Check ping
        if self.mode != constants.TrafficMode.HBN.value:
            if not self.run_ping_with_backoff():
                self.test_status = constants.ExitStatus.NVNETPERF_COMM_ERROR.value
                raise Exception("Network connection error")
            self.logger.info("Network connection is up between the hosts")


    def test_logic(self):
        if self.mode == constants.TrafficMode.OVS.value:
            self.logger.info("TCP OVS Benchmark Test - Start Execution")
            self.run_tcp_benchmark()
        elif self.mode == constants.TrafficMode.HBN.value:
            self.logger.info("TCP HBN  Test - Start Execution")
            self.run_evpn_tcp_benchmark('node_group_1')
        elif self.mode == constants.TrafficMode.IPSEC.value:
            self.logger.info("TCP IPSEC Benchmark Test - Start Execution")
            self.run_tcp_benchmark()
        return self.test_status


    def run_tcp_benchmark(self, single_dpu = False):
        """
        Run TCP concurrently on all servers
        """
        results = {}
        super().run_test_on_all_servers(servers=self.node_group_dict.keys(), timeout=1500, results=results, single_dpu=single_dpu)
        self.print_results(results)


    def run_test_on_server(self, idx, **kwargs):
        """
        Run TCP on the given node and measure network throughput
        """
        self.logger.info(f"Running TCP benchmark on {idx}")
        results = kwargs['results']
        single_dpu = kwargs["single_dpu"] if kwargs["single_dpu"] else False
        node_group = self.node_group_dict[idx]
        host_1 = node_group.hosts['host_1']
        cmd = ""
        if node_group.topology == constants.HostNodeTopology.Loopback.value:
            cmd = f"{self.benchmark_tcp_test} {host_1.uname}@{host_1.oob_address} {','.join(host_1.mlxdevs[0::2])} {host_1.uname}@{host_1.oob_address} {','.join(host_1.mlxdevs[1::2])} HALF CHANGE"
        if node_group.topology == constants.HostNodeTopology.B2B.value:
            host_2 = node_group.hosts['host_2']
            if (len(host_1.mlxdevs) != len(host_2.mlxdevs)):
                self.test_status = constants.ExitStatus.NVNETPERF_PARAM_ERROR.value
                raise Exception("Number of mlx devices for the two hosts doesn't match, please review your inventory file")
            #HBN runs on single DPU and single port
            if single_dpu:
                cmd += f"{self.benchmark_tcp_test} {host_1.uname}@{host_1.oob_address} {host_1.mlxdevs[0]} {host_2.uname}@{host_2.oob_address} {host_2.mlxdevs[0]} HALF CHANGE"
            else:
                cmd += f"{self.benchmark_tcp_test} {host_1.uname}@{host_1.oob_address} {','.join(host_1.mlxdevs)} {host_2.uname}@{host_2.oob_address} {','.join(host_2.mlxdevs)} HALF CHANGE"
        cmd += f" --duration={self.duration}" if self.duration else ""
        if self.mode == constants.TrafficMode.IPSEC.value:
            if node_group.topology == constants.HostNodeTopology.B2B.value:
                cmd += " --ipsec "
                for host in [host_1, host_2]:
                    dpu_ip = ""
                    ports = ""
                    for dpu in host.dpus.values():
                        dpu_ip += f"{dpu.uname}@{dpu.oob_address},"
                        port_num_cmd = "mst status -v | grep BlueField3 | wc -l"
                        output = dpu.sudo(port_num_cmd)
                        if "2" in output:
                            dpu_ip += f"{dpu.uname}@{dpu.oob_address},"
                            ports += "p0,p1,"
                        elif "1" in output:
                            ports += "p0,"
                    cmd += f" {dpu_ip[:-1]} {ports[:-1]} "
            else:
                raise Exception("TCP IPSec isn't supported for loopback")
        self.logger.info(f"Running: {cmd} ")
        output = self.test_server_node.run(cmd, warn = True, hide = "stderr")
        return self.check_tcp_results_for_node_group(output, idx, results)


    def check_tcp_results_for_node_group(self, output, idx, results):
        """
        Check TCP test results for the given node group, based on the log from NGC and update results dictionary
        """
        perf_error_count = 0
        self.ovs_pass_rate = None
        test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
        # Remove color related characters from output
        ansi_escape_pattern = re.compile(r"\x1b\[[0-?]*[ -/]*[@-~]")
        clean_output = ansi_escape_pattern.sub("", output)

        node_group = self.node_group_dict[idx]
        host_1 = node_group.hosts['host_1']
        server1 = f"{host_1.oob_address}"
        # Define mlx_dev_src <-> mlx_dev_dst for B2B and LB connections to parse the log
        if node_group.topology == constants.HostNodeTopology.Loopback.value:
            mlx_devs_src = host_1.mlxdevs[0::2]
            mlx_devs_dst = host_1.mlxdevs[1::2]
            server2 = server1
        else:
            host_2 = node_group.hosts['host_2']
            mlx_devs_src = host_1.mlxdevs
            mlx_devs_dst = host_2.mlxdevs
            server2 = f"{host_2.oob_address}"

        special_devs_dict = self.create_special_devices_dict(node_group.hosts.values())

        for [mlx_dev1,mlx_dev2] in zip(mlx_devs_src, mlx_devs_dst):
            devices = f"{mlx_dev1} <-> {mlx_dev2}"
            bw_pat = re.compile(fr'Throughput {server1}:{mlx_dev1} ->  {server2}:{mlx_dev2} :  (\d+(\.\d+)?).*?linerate (\d+(\.\d+)?)')
            match = bw_pat.search(clean_output)

            threshold_info = ""
            case_status = False
            if match:
                self.ovs_pass_rate = constants.TestThroughputPassRatio.OVS_PASS_THROUGHPUT.value/100 * int(match.group(3))
                bw = float(match.group(1)) + self.find_b3240_bw_tcp(special_devs_dict.get("B3240", {}), mlx_dev1, mlx_dev2, clean_output, server1, server2)

                if bw > self.ovs_pass_rate:
                    results.setdefault(idx, {}).setdefault(devices, {})["bw_bool"] = "Passed"
                    results.setdefault(idx, {}).setdefault(devices, {})["bw"] = f"{float(match.group(1))}  (Expected > {self.ovs_pass_rate})"
                    case_status = True
                else:
                    perf_error_count += 1
                    threshold_info = f"Throughput BW {float(match.group(1))} Gb/s is lower than minimal required throughput {self.ovs_pass_rate} Gb/s"
                    results.setdefault(idx, {}).setdefault(devices, {})["bw_bool"] = "Failed"
                    results.setdefault(idx, {}).setdefault(devices, {})["bw"] = f"{float(match.group(1))}  (Expected > {self.ovs_pass_rate})"
            else:
                results.setdefault(idx, {}).setdefault(devices, {})["bw_bool"] = "Failed"
                results.setdefault(idx, {}).setdefault(devices, {})["bw"] = f"N/A"
                test_status = constants.ExitStatus.NVNETPERF_FAIL.value

            self.set_subtest_data(self.test_name,
                                float(match.group(1)) if match else "N/A",
                                {"server1":server1, "dev1":mlx_dev1, "server2":server2, "dev2":mlx_dev2},
                                constants.TrafficTestType.BW.value,
                                {"expected": self.ovs_pass_rate,"info":threshold_info},
                                case_status)
        if perf_error_count > 0:
            test_status = constants.ExitStatus.NVNETPERF_PERF_ERROR.value
        return test_status


    def print_results(self, results):
        """
        Print a summary tables of the TCP test results, one table per node group
        """
        for node_group_key in results.keys():
            table = []
            row = []
            headers = ['Servers', 'Devices', 'PING', f'Throughput [Gb/s]', 'Test Status']
            table.insert(0, headers)
            server1 = self.node_group_dict[node_group_key].hosts['host_1'].oob_address
            if self.node_group_dict[node_group_key].topology == constants.HostNodeTopology.B2B.value:
                server2 = self.node_group_dict[node_group_key].hosts['host_2'].oob_address
            else:
                server2 = server1
            for devs in results[node_group_key].keys():
                row.append(f"{server1} <-> {server2}")
                row.append(f"{devs}")
                row.append(f"Passed")  # ping test
                row.append(results[node_group_key][devs]["bw"])
                row.append(results[node_group_key][devs]["bw_bool"])
                table.append(row)
                row = []
            column_alignments = ('center', 'center', 'center', 'center', 'center')
            self.logger.info(f"\n\nTest Summary:\n\n{tabulate(table, headers='firstrow', tablefmt='orgtbl', colalign=column_alignments)}\n")


    def run_evpn_tcp_benchmark(self, idx):
        """
        Setup EVPN VxLAN bridge and run tcp performance benchmark over it
        """
        if not self.create_evpn_vxlan_bridge():
            raise Exception("Failed to setup EVPN VxLAN Bridge")
        self.logger.info("Run TCP benchmark on the EVPN VxLAN Bridge")
        single_dpu = True
        results = {}
        self.test_status = self.run_test_on_server(idx, results, single_dpu=single_dpu)
        self.print_results(results)


    def find_b3240_bw_tcp(self, special_devs_dict, mlx_dev1, mlx_dev2, res, server1, server2):
        """
        Find the BW of the second port of dual port B3240 for TCP test
        """
        # If one of the mlx_dev is connected to a B3240 DPU, update bw to reflect that
        if (server1 in special_devs_dict.keys()) and any(mlx_dev1 in sublist for server1 in special_devs_dict.values() for sublist in server1):
            first = True
            mlx_dev = mlx_dev1
            self.logger.info(f"{mlx_dev1} is connected to B3240 DPU, calculating total BW")
        # If only mlx_dev2 is connected to a B3240 DPU
        elif (server2 in special_devs_dict.keys()) and any(mlx_dev2 in sublist for server2 in special_devs_dict.values() for sublist in server2):
            mlx_dev = mlx_dev2
            first = False
            self.logger.info(f"{mlx_dev2} is connected to B3240 DPU, calculating total BW")
        else:
            return 0

        # Find the other port of this DPU and return BW
        second_dev = next((sublist[1] for nested_list in special_devs_dict.values() for sublist in nested_list if sublist[0] == mlx_dev), None)
        if not second_dev:
            second_dev = next((sublist[0] for nested_list in special_devs_dict.values() for sublist in nested_list if sublist[1] == mlx_dev), None)
        if first:
            second_dev_bw_pat = re.compile(fr'Throughput {server1}:{second_dev} ->  {server2}:mlx5_\d :  (\d+(\.\d+)?).*?linerate (\d+(\.\d+)?)')
        else:
            second_dev_bw_pat = re.compile(fr'Throughput {server1}:mlx5_\d ->  {server2}:{second_dev} :  (\d+(\.\d+)?).*?linerate (\d+(\.\d+)?)')
        second_match = second_dev_bw_pat.search(res)
        if second_match:
            return float(second_match.group(1))
        return 0

    def set_variables_from_file(self, inventory, params):
        super().set_variables_from_file(inventory, params)
        self.duration = params["tests"][self.test_run_name]["duration"]




if __name__ == "__main__":
    test = TcpTest()
    test.args_execute()
    exit(test.execute())