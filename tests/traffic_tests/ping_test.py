#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent.parent.absolute()) + "/")

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    from common.basic_classes import traffic_base_class as tbc
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class PingTest(tbc.TrafficBaseClass):
    """
    Ping Test
    """

    def __init__(self):
        super().__init__()
        self.test_name = "ping_test"


    def test_setup(self):
        super().test_setup()
        self.logger.info("Ping Test - Start Test Preparation")

        # check if the hosts can talk to each other over the OVS bridge
        if self.mode == constants.TrafficMode.HBN.value:
            self.create_evpn_vxlan_bridge()


    def test_logic(self):
        if self.run_ping_with_backoff():
            self.test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
            self.logger.info("Test Setup is healthy")
        else:
            self.logger.error("Network connection is down between the hosts")
            self.logger.critical("Test Setup is rotten")
            self.test_status = constants.ExitStatus.NVNETPERF_COMM_ERROR.value
        return self.test_status

if __name__ == "__main__":
    test = PingTest()
    exit(test.execute())