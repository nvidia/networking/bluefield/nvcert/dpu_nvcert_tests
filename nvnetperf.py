#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#



import sys
import yaml
import shutil
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    import os
    import time
    import argparse
    import logging
    from logging.handlers import RotatingFileHandler
    from common.wrappers import nvnetperf_tests_wrapper as ntw
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc

logger = logging.getLogger("nvnetperf")

# Default file path for inventory file
inventory_file_path = "hosts.yaml"
test_suite_file_path = "test_suites/test_suite.yaml"

class DocaPerfTest():

    def __init__(self):
        self.params = {}
        self.exit_status = {}

    def parse_arguments(self):
        parser = argparse.ArgumentParser(description="Test execution script")
        parser.add_argument("-i", "--inventory", default=inventory_file_path,
                            help="Path to the inventory file")
        parser.add_argument("-f", "--test_suite_file", default=test_suite_file_path,
                            help="Path to the params config file")

        return parser.parse_args()



    def read_test_suite_file(self, test_suite_file):
        """
        Parse the params file
        """
        # Load the YAML file
        with open(test_suite_file, 'r') as yaml_file:
            try:
                data = yaml.safe_load(yaml_file)
            except yaml.YAMLError as e:
                logger.error(f"Error parsing test suite file: {e}")
                sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)
        self.params =  ntw.merge_test_suites_config_with_defaults(data)

    def run_tests(self, params):
        selected_tests = params["tests"].keys()
        all_tests = ntw.get_nvnetperf_tests(self.inventory, params)
        
        status = 0
        rc = 0
        exit_statuses = {}

        # Perform the selected tests and gather results
        for test_name in selected_tests:
            if test_name in all_tests:
                script_start_time = time.time()
                logger.info("******************************************")
                logger.info(f"Starting test {test_name}.")
                logger.info(f"Executing {test_name}.")
                log_file = f"{params['general']['log_dir']}/{test_name}.log"
                params["general"]["log_file"] = log_file
                status = all_tests[test_name]()
                exit_statuses[test_name] = status
                elapsed_time = self.calculate_elapsed_time(script_start_time, time.time())
                if status == 0:
                    logger.info(f"Test \"{test_name}\" passed.")
                else:
                    logger.error(f"Test \"{test_name}\" failed with status {status}.")
                    rc = 1

                logger.info(f"Test time: {elapsed_time}")
                logger.info(f"******************************************\n")
        return rc, exit_statuses

    
    def setup_logger(self, log_dir):
        """Configure the logger to write to a file in the specified directory."""
        log_path = os.path.join(log_dir, f"summary.log")
        handler = RotatingFileHandler(log_path, maxBytes=10**6, backupCount=5)
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(logging.INFO)
        return log_path
    
    def create_log_directory(self, base_dir):
        """Create a log folder named 'nvnetperf_logs_<timestamp>' inside the specified base directory."""
        os.makedirs(base_dir, exist_ok=True)
        timestamp = time.strftime("%Y-%m-%d_%H-%M-%S")
        log_dir = os.path.join(base_dir, f"nvnetperf_logs_{timestamp}")
        os.makedirs(log_dir, exist_ok=True)
        return log_dir

    # Calculate elapsed time
    def calculate_elapsed_time(self, start_time, end_time):
        elapsed_time = end_time - start_time
        hours, rem = divmod(elapsed_time, 3600)
        minutes, seconds = divmod(rem, 60)
        return f"{int(hours)}:{int(minutes):02}:{int(seconds):02}"


    def execute_test(self):
        start_time = time.time()

        root_dir = constants.DEFAULT_ROOT_DIR
        # If running as a PyInstaller executable
        if getattr(sys, 'frozen', False):
            root_dir = sys._MEIPASS

        self.args = self.parse_arguments()
        self.inventory = self.args.inventory
        self.test_suite_file = self.args.test_suite_file

        self.read_test_suite_file(self.test_suite_file)

        # Setup log directory
        log_dir = self.create_log_directory(self.params["general"]["log_dir"])
        self.params["general"]["log_dir"] = log_dir
        self.params["general"]["root_dir"] = root_dir
        self.setup_logger(log_dir)

        shutil.copy(self.inventory, log_dir)

        rc, exit_statuses = self.run_tests(self.params)

        # Final summary
        elapsed_time = self.calculate_elapsed_time(start_time, time.time())
        logger.info("============ Execution summary ============")
        for test_name, status in exit_statuses.items():
            if status == 0:
                logger.info(f"Test \"{test_name}\" passed.")
            else:
                logger.error(f"Test \"{test_name}\" failed with status {status}.")
        logger.info(f"Elapsed time: {elapsed_time}")
        logger.info(f"Logs saved in: {log_dir}")
        logger.info(f"Test JSON Output Files Path: {log_dir}/{constants.TestsPaths.JSON_FOLDER.value}")
        logger.info("===========================================\n")
        return rc


if __name__ == "__main__":
    test = DocaPerfTest()
    sys.exit(test.execute_test())