# NVIDIA Testing for Bluefield DPUs and ConnectX

NVNetPerf provides software validation packages for NVIDIA partners, ensuring optimal functionality and performance in the integration of NVIDIA products with partner systems.

## Supported Platforms

- NVIDIA Bluefield3 (BF3)
- NVIDIA ConnectX7 (CX7)

## Test Setup Requirements

### Supported Topologies

The following topologies are supported for testing:

![Back to Back Topology with Test Server](./docs/B2B-Topology.jpg)
![Loopback Topology with Test Server](./docs/Loopback-Topology.jpg)

Note: There is also support for the Loopback topology, where a card is connected to itself. 

### Physical Connectivity

Choose one of the following connectivity options:

1. **Back-to-Back Topology**: 
  - Connect the DPU/CX uplinks (p0 and p1) directly between two separate hosts, as shown in Figure 1 above.
2. **Loopback Topology**:
  - Connect the DPU/CX uplinks (p0 and p1) between two cards on the same host, as shown in Figure 2 above.
  - Connect a single card to itself, where port p0 is connected to port p1.

**Using a Switch**: The DPU uplinks (p0 and p1) can also be connected via a top-of-rack (ToR) switch. Ensure that:  
    - The ToR switch ports have matching speeds.  
    - The same VLAN is provisioned for traffic bridging between the DPUs.<br>
High-throughput test traffic with varying patterns is sent over this network, so it must be isolated from the management network. Using a separate untagged/PVID VLAN on the ToR for the DPU uplinks is sufficient.<br>
**Note:** While a switch connection between the NICs is possible, it is not recommended for this test due to added complexity and increased latency.

**Note:** Connecting the AUX power cable to Bluefield-3 cards (which come with an ATX connector) is mandatory; otherwise, the ARM will fail to boot.

### Out-Of-Band (OOB) Connectivity - DPU Only

For running DPUs, out-of-band SSH access from the test server to the DPUs is required. Connect the DPU's OOB 1G port (oob_net0) to a management network with a DHCP server. The DPU (DHCP assigned) OOB IPv4 address is used for software installation and DPU configuration. Internet access is required in the OOB network for downloading packages.

### DPU BMC Connectivity - DPU Only

The DPU requires a BMC for management. The DPU OOB and DPU BMC share the same external 1Gbps port. Separate MAC addresses are allocated to the DPU OOB and BMC ports. The DPU (DHCP assigned) BMC IPv4 address is used for installing the DPU OS.

### Host Requirements

#### Out-Of-Band (OOB) Connectivity
Out-of-band SSH access from the test server to all participating hosts is required for installing, configuring, and running the tests.

### Tuning instructions for servers for best performance

| Item                                    | Description                    |
|-----------------------------------------|--------------------------------|
| Eth Switch ports                        | Set MTU to 9216<br>Enable PFC and ECN using the single "Do ROCE" command |
| IB Switch OpenSM                        | Change IPoIB MTU to 4K:<br>[standalone: master] → en<br>[standalone: master] # conf t<br>[standalone: master] (config) # ib partition Default mtu 4K force |
| **AMD CPUs: EPYC 7002 and 7003 series** |                                |
| BIOS Settings                           | CPU Power Management → Maximum Performance<br>Memory Frequency → Maximum Performance<br>Alg. Performance Boost Disable (ApbDis) → Enabled<br>ApbDis Fixed Socket P-State → P0<br>NUMA Nodes Per Socket → 2<br>L3 cache as NUMA Domain → Enabled<br>x2APIC Mode → Enabled<br>PCIe ACS → Disabled<br>Preferred IO → Disabled<br>Enhanced Preferred IO → Enabled |
| Boot grub settings                      | Add this `iommu=pt numa_balancing=disable processor.max_cstate=0` to `GRUB_CMDLINE_LINUX_DEFAULTS` in `/etc/default/grub`, run `sudo update-grub` and reboot the server|
| **Intel CPUs: Xeon Gold and Platinum**  |                                |
| BIOS Settings                           | Out of the box                 |
| Boot grub settings                      | `intel_idle.max_cstate=0 processor.max_cstate=0 intel_pstate=disable` |
| NIC PCIe settings                       | For each NIC PCIe function:<br>Change PCI MaxReadReq to 4096B<br>Run `setpci -s $PCI_FUNCTION 68.w`, it will return 4 digits ABCD<br>→ Run `setpci -s $PCI_FUNCTION 68.w=5BCD` |
| **Intel CPUs: Sappire Rapid**           |                                |
| BIOS Settings*                          | Socket Configuration > IIO Configuration > Socket# Configuration > PE# Restore RO Write Perf > Enabled |
| Boot grub settings                      | `intel_idle.max_cstate=0 processor.max_cstate=0 intel_pstate=disable` |
| NIC PCIe settings                       | For each NIC PCIe function:<br>Change PCI MaxReadReq to 4096B<br>Run `setpci -s $PCI_FUNCTION 68.w`, it will return 4 digits ABCD<br>→ Run `setpci -s $PCI_FUNCTION 68.w=5BCD`<br>In case BIOS settings* is messing then need to disable PCIe relaxed ordering in PCI:<br>Run `setpci -s $PCI_FUNCTION 68.w`, it will return 4 digits ABCD<br>if C=5 then change it to 4<br>if C=3 then change it to 2<br>For example if it is AB5D<br>→ Run `setpci -s $PCI_FUNCTION 68.w=AB4D`<br>Restart driver: `/etc/init.d/openibd restart` |
| For bset TCP                            | When running X instances of TCP<br>Run `systemctl stop irqbalance.service`<br>Receive interrupts on cores that used to run TCP app, where Y to (Y+X) is the list of cores on same NUMA as NIC:<br>`set_irq_affinity_cpulist.sh Y-(Y+X) $iface`<br>`ethtool -L $iface combined X`<br>`ethtool -C $iface adaptive-rx off`<br>`ethtool -C $iface rx-usecs 256`<br>`ethtool -C $iface rx-frames 1024`<br>`ethtool -G $iface rx 2048 tx 2048`<br>Set flow steering:<br>`for i in $(seq 0 X-1)`<br>`do`<br>`echo “Attempting to delete any existing rules...”`<br>`ethtool -U $iface delete $i`<br>`echo “DONE : Attempting to delete any existing rules...”`<br>`ethtool -U $iface flow-type tcp4 dst-port $((10000 + $i)) loc $i queue $i`<br>`done` |

### Test Server Requirements
The test server is used to run all installations, configurations, and tests on all units.

### Connectivity
The test server should have access to all devices (DPUs, CXs, hosts) and internet access.

### Account
The tests must be run using an account with root privileges, allowing actions to be performed without requiring a password.
This can be achieved by either using the root user or granting the test server user and the hosts users such privileges.
If you want to use the credentials nvidia/nvidia, follow these instructions to create the user:
```
sudo useradd -m -d /nvidia -s /bin/bash -u 990 nvidia
echo nvidia:nvidia | sudo chpasswd
sudo usermod -aG sudo nvidia
sudo reboot now
```

If you chose different credentials, make sure to perform the password-less access setup using the credentials you provided.

### Password-less Access to the Hosts for NVIDIA User
After creating the accounts on all hosts and the test server, ensure that the test server has full password-less SSH access to all hosts and DPUs in the testing environment.

The DPUs will automatically have an "ubuntu" account created through the bootstrap Ansible playbooks.

Follow these steps to set up password-less SSH access from the test server to the participating hosts and DPUs:
- Generate SSH key on the test server
    `username@test-server:~# ssh-keygen -f ~/.ssh/id_rsa -N ""`

- Copy the key to each the hosts under test and the DPUs:

    `username@test-server:~# ssh-copy-id -i ~/.ssh/id_rsa.pub nvidia@host-oob-address`

    `username@test-server:~# ssh-copy-id -i ~/.ssh/id_rsa.pub ubuntu@dpu-oob-address`

- Verify password-less SSH into the hosts under test and DPUs

    `username@test-server:~# ssh nvidia@host-oob-address`

    #exit from the ssh session 

    `username@test-server:~# ssh ubuntu@dpu-oob-address`

    #exit from the ssh session

Once done, to allow the user (e.g., the example user "nvidia") to run the required binaries without entering a password, follow these steps on all hosts:
  - SSH into the host
  - Use root permissions to edit the sudoers file by running: `sudo EDITOR=vim visudo`
  - Add the following line at the end of the file: `nvidia ALL=(ALL) NOPASSWD: /usr/bin/bash,/usr/sbin/ip,/opt/mellanox/iproute2/sbin/ip,/usr/bin/mlxprivhost,/usr/bin/mst,/usr/bin/systemctl,/usr/sbin/ethtool,/usr/sbin/set_irq_affinity_cpulist.sh,/usr/bin/tee,/usr/bin/numactl,/usr/bin/awk,/usr/bin/taskset,/usr/bin/setpci,/usr/bin/rm -f /tmp/*,/usr/bin/mlxfwmanager,/usr/sbin/modprobe,/usr/sbin/set_irq_affinity_bynode.sh,/usr/sbin/set_irq_affinity.sh,/usr/sbin/nvme,/usr/bin/lscpu,/usr/bin/fio,/usr/sbin/service irqbalance *,/usr/bin/mkdir,/usr/bin/ln`
  - Save the file and exit.

Note that this line gives permission to the "nvidia" user to run the listed commands using sudo without prompting for a password.

### Host and DPUs software requirements

In order for the tests to work properly, the hosts and the DPUs software must be up to date.
Also there are several configurations required on them, as described below.

The user can choose to manually configure all the SW and settings, or to use the ansible playbooks designed for it, as described below.

#### Ansible Automation

The DPU and host setup can be done automatically by using the [NVNetPerf Bootstrap](https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_bootstrap) git. This repository contains the Ansible playbook necessary for setting up the test environment- `nvnetperf-install-doca-zt.yml`.
The Ansible playbook is responsible for managing all required installations and configurations for the tests. 

**note** it is unnecessary to rerun the Ansible playbook before each individual test. Once the playbook, such as `nvnetperf-install-doca-zt.yml` for instance, has been executed, all subsequent tests that require its configurations as prerequisites can be run seamlessly.

#### Manual preparations

To manually prepare the system for running tests, follow these steps:

##### Host Operating System

The host must run Ubuntu 22.04 (Jammy Jellyfish). The installation of the host OS is outside the scope of this guide.

##### 1. Update SW to the Latest Versions

Ensure the following components are updated to the latest GA versions:

- BMC
- DOCA for the host
- BFB and FW

##### 2. Install Required Packages

Run the following command to install the necessary packages:

```sh
sudo apt update && sudo apt install sshpass minicom lldpd python3-pip numactl bc iperf3 net-tools ipmitool sysstat jq fio nvme-cli curl && pip install fabric2 mako requests jsonschema
```

##### 3. Additional Setup for GPU Tests

If you are running GPU tests, perform the following additional steps:

- Install the `cuda-keyring_1.1-1_all.deb` package on the host.
- Install the following packages:

  ```sh
  sudo apt install build-essential git libibverbs-dev librdmacm-dev libibumad-dev autoconf libtool libpci-dev cuda nvidia-cuda-toolkit
  ```
- Run the Perftest installation script provided in the bootstrap: `/roles/host/install-gdr-utils/files/install_perftest.sh`.

Alternatively, execute the `nvnetperf-install-doca-zt.yml` playbook with the `--tags install_gdr_utils` option.

##### 4. Configure Operation Mode

Set the system to the desired operation mode (NIC/DPU mode).


## Testing preparations

### Testing repository

To run NVNetPerf, you will need the following repository:

- [NVNetPerf Tests](https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_tests): Contains the test scripts
  `git clone --recurse-submodules https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_tests.git`

### Requirements.txt file

To enable the test server to successfully execute the tests, it is essential to install the required Python libraries specified in the requirements.txt file.

- Run the following command from the test server: `pip install -r requirements.txt`

### Inventory File

**Before preforming any test**, ensure the inventory file, `hosts.yaml`, is properly configured.
This file outlines the configurations necessary for executing the tests. 
The "hosts.yaml" from [NVNetPerf Tests](https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_tests) is the default inventory file for the test. It should be edited to match the current testing setup.

If you prefer not to edit the original `hosts.yaml` file or want to keep multiple copies for different setups, you can create and maintain separate inventory files.
These inventory files can be specified to the test script via the -i option.
For example, `./tests/traffic_tests/tcp_test.py -i inventory-1.yaml`.

**Note:**  This is a different inventory file than the bootstrap inventory file.

key points:
- Node_group:
  Each node_group defines a connection in the system and must be configured separately. 
  Multiple node_group entries can be included in the same system, even with different topologies. 
  The node_group value specifies the connection and must match the topology.
  Each node_group will execute tests concurrently.
  
- Topologies:
  The topology field defines the connection type for each host. For more details, see the beginning of the README. Supported options:
  - `back-to-back`- Direct connection between two hosts.
  - `loopback`- Connection within a single host.

***Guidelines for Filling the Inventory File:***
- Define Groups:
  - A group represents a set of connected hosts or a loopback configuration.
  - Each group is specified under a unique key (e.g., node_group_1).
  - The `topology` field should indicate either back-to-back or loopback.

- Specify Hosts (Nodes):
  - In back-to-back topology, two hosts are required.
  - In loopback, only one host is defined.
  - For each host:
    - `host_ip`: The host's IP for remote access.
    - `user/password`: Credentials for accessing the host.

- Configure DPUs/CX:
  - Use the `pcie_bdf_address` field to list the PCIe addresses of all DPUs/CXs.
  - Addresses must be encapsulated in quotes (") and separated by commas.
  - Order matters: addresses should align with the physical connections:
    - For loopback topology, ensure that pcie_bdf_address are listed sequentially to reflect the pairing of connections. e.g: `pcie_address: c2:00.0, c2:00.1, 89:00.0, 89:00.1` means that `c2:00.0` is connected to `c2:00.1`, and `89:00.0` is connected to `89:00.1`.
    - For back-to-back, ensure the pcie_bdf_address values for host_1 and host_2 align, reflecting    the physical connections. e.g: `host_1 pcie_address: 9c:00.0, 9c:00.1` and `host_2:  pcie_address: c2:00.0, c2:00.1`, where `9c:00.0` is connected to `c2:00.0` and `9c:00.1` is connected to `c2:00.1`.
  
- Assign IP Addresses:
  - Use the `uplink_network` field to specify the IP addresses for each DPUs/CX port and illustrate their connections.
  - IPs *must* correspond to the `pcie_bdf_address` order.
  - In case of IB server, please remove `uplink_network` entry.

- DPU Details:
  - If DPUs are used, provide:
    - `oob_address`: Out-of-band management IP for the DPU.
    - `user/password`: Access credentials.
  - Multiple DPUs can be defined under each host.

### NIC vs DPU Mode

The tests have several running modes, and testing should be done according to the planned system architecture:

**DPU Mode** - This mode, also known as Embedded CPU Function Ownership (ECPF) mode, is the default mode for BlueField DPU.

**NIC Mode** - In NIC mode, the DPU behaves exactly like an adapter card from the perspective of the external host.

1. **DPU/NIC mode (DPU only)** - The DPU can run in two modes. For further explanation, please follow this [link](https://docs.nvidia.com/doca/sdk/nvidia+bluefield+modes+of+operation/index.html). When running in DPU mode, the testing will be done in Zero Trust mode. Additional information about each of the modes is described below.

2. **Testing Scenario Category**: Defined by the use case being tested. Please follow at least one of the test categories:
   2.1. N-S Testing
   2.2. E-W Testing

To review each category, follow the instructions below. The table below provides a short summary of the tests required for all categories:

![Tests categories](./docs/DOCA_Perftest_tests_categories.png)

For extended reading, please follow NVIDIA documentation - [NVIDIA BlueField DPU Modes of Operation](https://docs.nvidia.com/doca/sdk/nvidia+bluefield+dpu+modes+of+operation/index.html)



#### Zero Trust vs Trusted Mode


##### Host Trusted Mode

By default, the DPU is in DPU mode where the host is trusted. The host can SSH into the DPU using the host-rshim network, change NIC config, and program the DPU's acceleration engines.

To onboard a new DPU in `Host Trusted` mode, connect the 1G OOB port to a management network with a DHCP server and run the Ansible installation playbook described in the next section. The installation playbook uses the host rshim network for DPU OS installation and for querying the (DHCP assigned) DPU OOB IPv4 address.

##### Zero Trust Mode

Sample DPU Board Label:
![DPU Sticker](./docs/board-label.jpg)

To onboard a new DPU in `Zero Trust` mode, you need to:

1. Locate the DPU BMC MAC (`DPU BMC:`) and DPU OOB MAC (`OOB:`) on the DPU `Board Label`.
2. Set up MAC to IPv4 mappings for the DPU BMC MAC and DPU OOB MAC on the DHCP server running in the management network.
3. The DPU BMC and DPU OOB share the same external port. Connect this port to the 1Gbps management network.
4. Run the Ansible zero-trust installation playbook. The playbook installs the DPU OS via the DPU BMC and places the DPU in restricted mode. In this mode, the host cannot SSH into the DPU or program the DPU network accelerators.

To verify the mode, run the following command on the host:
```
nvidia@host-oob-address:~# sudo mlxprivhost -d /dev/mst/mt41692_pciconf0 q
Host configurations
-------------------
level                         : RESTRICTED

Port functions status:
-----------------------
disable_rshim                 : TRUE
disable_tracer                : TRUE
disable_port_owner            : TRUE
disable_counter_rd            : TRUE

nvidia@host-oob-address:~#
```

## Test Execution

Each test has its one execution under  `/tests` directory, logs are created under `/tmp/nvnetperf/`

Additional optional command line parameters that can be used:
- `-i <inventory>` for inventory file, the default is `hosts.yaml`
- `-f <file path>` to specify a custom log file, the default is `/tmp/nvnetperf/<test>_<time>.log`
- `-v <verbosity level>` to determine the verbosity of the log file, the default value is `DEBUG`
- `--skip_speed_check` to skip speed configuration validation at the beginning of the run (it checks that cables and ports speed is configured to maximum) - only available for traffic tests (TCP/RDMA based tests)
- `--skip_system_validation` to skip the automatic scan of syslog and dmesg at the end of the run to check errors

### <u>RDMA and TCP Performance Test Usage</u>

![OVS Topology](./docs/OVS-Topology.jpg)

The main goal of the tests is to check that both RDMA and TCP are passing with full bandwidth on the system. The tests validate several basic functionalities such as PCI, PHY, system memory, etc.

To reach full bandwidth, the tests perform multiple configurations on the system (such as irq, MTU) and run in an optimized manner (for example, on the correct cores and NUMA). Please make sure all the suggested BIOS optimizations mentioned above are configured.


For additional explanation, please follow [RDMA and TCP Test Description](https://github.com/Mellanox/ngc_multinode_perf/blob/main/README.md).

The tests run concurrently on all node groups specified in the inventory file.

#### How to Run:

- Run the TCP performance test command from the root folder of the test repo:
  `./tests/traffic_tests/tcp_test.py --mode ovs`
- Run the RDMA tests command from the root folder of the test repo:
  `./tests/traffic_tests/rdma_test.py --mode ovs`

Notes:
- RDMA-OVS supports two additional cases:
  - Loopback topology
  - Both ETH and IB link types
- TCP-OVS additionally supports:
  - Loopback topology
  - ETH link type
- For TCP test, if your server has Sapphire Rapid CPU, make sure the BIOS has the following fix: "Socket Configuration > IIO Configuration > Socket# Configuration > PE# Restore RO Write Perf > Enabled" to allow optimizations to reach full performance.
- Optional run command parameters:
  - TCP tests:
    - Test's duration: `--duration=<in seconds>`
  - RDMA tests:
    - Packet size: `--bw_message_size_list=<list of message sizes> ` or `--lat_message_size_list=<list of message sizes>`, example: `--bw_message_size_list=512,2048`
    - Number of QPs: `--qp=<num of QPs>`
    - Tests to run: `--tests=ib_write_bw,ib_read_lat`
    - Tests duration: `--duration=<time in seconds>`
    - Extra parameters: `--extra="<parameters>"`

#### Expected Results:

Hitting line rate. Pass rate - 90% of full bandwidth.

### <u>IPSec Performance Test Usage</u>

The main goal of the test is to verify that the server can run IPSEC over TCP and RDMA on the system.


The tests run on all available DPUs, so all DPU's PCIes must be added to the inventory file.

#### How to Run:

Run the TCP performance test from the root folder of the test repo
  `./tests/traffic_tests/tcp_test.py --mode ipsec`
Run the RDMA test from the root folder of the test repo
  `./tests/traffic_tests/rdma_test.py --mode ipsec`

#### Expected Results:

Hitting line rate. Pass rate - 90% of full bandwidth.

### <u>GPU Direct RDMA Test Usage</u>

The main goal of the test is to verify that the server can run RDMA between 2 GPUs on the system.
The test runs concurrently on all node groups in the inventory file.

Notes:
- This test supports two additional cases:
  - Both ETH and IB link types
  - Loopback topology
#### How to Run:

- Run the GPU Direct RDMA performance test from the root folder of the test repo
  `./tests/traffic_tests/gpu_direct_rdma_test.py`
- On GB200 system, running the test with the `--allow_gpu_node_relation` flag is required to enable 'NODE' relation between the GPU and NIC.

#### Expected Results:

Hitting line rate. Pass rate - 90% of full bandwidth.

### <u>Secure Boot Test Usage</u>

The test checks if the server is capable of running secure boot on the system.

UEFI Secure Boot works as a security gate. Code signed with valid keys (whose public key/certificates exist in the DPU) gets through the gate and executes while blocking and rejecting code that has either a bad or no signature.

For additional reading please follow this [link](https://docs.nvidia.com/networking/display/bluefielddpubspv422/uefi+secure+boot).

The test runs on all available DPUs, so all DPU's PCIes must be added to the inventory file.

This test supports two additional cases:
  - Both ETH and IB link types
  - Loopback topology

#### How to Run:

- Run the secure boot test command from the root folder of the test repo
  `./tests/secure_boot.py `

#### Expected Results:

All system is secure boot enabled.

### <u>Basic Storage (FIO) Test Usage</u>

NVM Express (NVMe) is an interface standard for accessing non-volatile storage, commonly SSD disks.
NVMeoF is an architecture to access NVMe storage over different networking fabrics, for example RDMA, TCP or NVMe over Fibre Channel (FC-NVMe). The role of NVMeoF is similar to iSCSI. To increase the fault-tolerance, NVMe-oF has a built-in support for multipathing. The NVMe-oF multipathing is not based on the traditional DM-Multipathing.
The NVMe Initiator is the machine that connects to an NVMe target. The NVMe target is the machine that shares its NVMe block devices.

For additional reference, please follow this [link](https://enterprise-support.nvidia.com/s/article/howto-configure-nvme-over-fabrics).

The tests run on single DPU (per host), it will automatically run on the first DPU (per host, according to PCIe) that is specified in inventory file.

#### How to Run:

- Run this test command from the root folder of the test repo
  `./tests/basic_storage/basic_storage_test.py `

#### Expected Results:

Hitting line rate. Pass rate - 90% of full bandwidth.


### <u>HBN EVPN Performance Test Usage</u>

![HBN Topology](./docs/HBN-Topology.jpg)

[HBN Test description](https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_tests/-/blob/main/hbn/README.md) - This is a performance test on the hosts under test with a) BGP-EVPN offloaded on the DPU ARM, and b) VxLAN packet processing pipeline HW-accelerated on the DPU eSwitch. **HBN is supported on dual-port DPUs only**.

The tests run on single DPU (per host), it will automatically run on the first DPU (per host, according to PCIe) that is specified in inventory file.

#### How to Run:

- Start the HBN container on the DPUs using `ansible-playbook nvnetperf-install-hbn-container.yml` in the NVNetPerf Bootstrap repo.
- Run the TCP performance test command from the root folder of the test repo with EVPN accelerated on the DPU.
  `./tests/traffic_tests/tcp_test.py --mode hbn`
- Run the RDMA performance test command from the root folder of the test repo with EVPN accelerated on the DPU.
  `./tests/traffic_tests/rdma_test.py --mode hbn`

#### Expected Results:

Hitting line rate. Pass rate - 90% of full bandwidth.

### Doca Telemetry Diag Tool

The tool provides programmable access to an on-device mechanism for sampling diagnostic data, such as statistics and counters.
The telemetry diag tool operates on specific PCIe devices and enables diagnostic tests based on the data it collects.

For additional reference, please follow this [link](https://docs.nvidia.com/doca/sdk/doca+telemetry+diagnostics/index.html).

The DOCA Telemetry Diag tool requires the fwctl driver, which facilitates secure access to firmware for device debugging, configuration, and provisioning.
The fwctl driver is automatically installed as part of the DOCA installation in the NVNetPerf Bootstrap.

For additional reference, please follow this [link](https://lore.kernel.org/all/0-v3-960f17f90f17+516-fwctl_jgg@nvidia.com/#t)

**Note:** The doca telemetry diag test is only supported DPUs in Trusted Mode and **does not support Zero-Trust Mode**.

#### Telemetry Diag-Based Tests:

Currently, the available test based on the data from the doca telemetry diag tool is: pcie_latency_test.

### PCIe Latency Test

This test calculates the minimum, maximum, and average PCIe read latency values in nanoseconds for each PCIe device.
The test can be executed in two modes:
- Steady State: The test runs under normal conditions, without any stress applied to the system.
- Under Stress: The test runs while the system is under stress. The stress is created by running ib_write_bw from the RDMA test.

This test supports two additional cases:
  - Both ETH and IB link types
  - Loopback topology
#### How to Run:

- To execute the PCIe Latency Test in steady state, run the following command from the root folder of the test repository:
 `./tests/telemetry_diag/pcie_latency_test.py --steady`

- To execute the PCIe Latency Test under stress, run the following command from the root folder of the test repository:
  `./tests/telemetry_diag/pcie_latency_test.py --stress`

- If no mode is specified, the test will *run both under stress and in steady state* by default.

- Optional run command parameters:
Packet size (for stress mode): `--bw_message_size_list=<list of message sizes> `, example: `--bw_message_size_list=512,2048`

#### Expected Results:

The average PCIe read latency time should be below 2 microseconds for all PCIe devices, both under stress and in steady state.

### DOCA Bench

The primary objective of the DOCA Bench tests is to evaluate the performance of various pipelines and ensure that bandwidth results meet system expectations.

- DOCA Bench Test supports the following pipelines:
    - DOCA DMA
    - DOCA Compress/Decompress: LZ4, deflate
    - DOCA Erasure Coding (EC)
    - DOCA Encryption and Decryption

The test uses DOCA Bench located at /opt/mellanox/doca/. Ensure that DOCA is successfully installed on the Host system before proceeding with the tests.

- Test Environment
  - The tests are executed exclusively on X86 systems, with or without remote buffers.
  - The test utilizes NUMA cores, running on one core (excluding Core 0), with a job size of 16K and a duration of 5 seconds.
  - Bandwidth expectations are pre-configured and constant within the tests.
  - The test supports both ETH and IB link types and supports a loopback topology
  - note: DOCA Bench cannot run on ConnectX (CX) devices; it can only run with BlueField DPUs.
  - note: DOCA Bench with remote input/output buffers (directions dpu-host/host-dpu) not supported on Zero Trust mode and Nic mode.
  - note: DOCA Bench decompress(LZ4/deflate) can't run with remote-input-buffers (direction dpu-host)(DOCA Bench Specification)

When testing with remote buffers, all available DPUs will be utilized, so ensure that all DPU PCIes are included in the inventory file.

For additional explanation for DOCA Bench, please follow [DOCA Bench Sample Invocations](https://docs.nvidia.com/doca/sdk/doca+bench+sample+invocations/index.html).

#### How to Run:

- Run the DOCA Bench test command from the root folder of the test repo
  `./tests/doca_bench/doca_bench_test.py `

- Run command parameters:
  - Pipeline Selection: (Required) Specify the pipeline to test using the `--bench_name=<string>` parameter.
      - Options: doca_dma, doca_decompress_lz4, doca_decompress_deflate, doca_ec_create, doca_aes_gcm_encrypt, doca_aes_gcm_decrypt
      - Example: `--bench_name=doca_dma`
  - Direction: Use the `--direction=<string>` parameter to specify the test direction.
      - Options: Host-Host, DPU-Host, Host-DPU
      - Example: `--direction=Host-Host`

#### Expected Results:

Pass rate - 80% of the expected bandwidth.



## Other Options and Utilities

### Get Setups Info

Gather system information at the end of the test run. Using `linux-sysinfo-snapshot` this tool is part for DOCA on Host and BFB by default. The tool creates a output tgz file under `/tmp/` that contains the collected information about the following: 2 hosts, 2 DPUs, and for each DPU, the ports info.

#### How to Run:

- Run this test command from the root folder of the test repo

  `./tests/system_info_test.py`

### Performance Configuration Readiness

This utility is designed to run static analysis on system configurations before conducting full-scale performance testing. The goal is to ensure that the system (hosts) is optimally configured from a performance perspective. Any misconfiguration can potentially degrade the performance of the tests, and this analysis helps identify and address such issues.

#### How to Run:

- Run the Performance configuration readiness test command from the root folder of the test repo
  `./tests/performance_configuration_readiness/performance_configuration_readiness_test.py`

#### Expected Results:

Setup is optimized if all performance checks passed.

## Additional Documentation

- Use `./tests/<test_executable> help` for tests params menu
- Use `./tests/ping_test.py --mode <dpu-config>` for sanity check of setup connection
- [HBN Test Details](https://gitlab.com/nvidia/networking/bluefield/doca_perftest/doca_perftest_tests/-/blob/main/tests/traffic_tests/hbn/README.md)
- [OVS, IPSec Details](https://github.com/Mellanox/ngc_multinode_perf/blob/main/README.md)

For NVIDIA internal users:
- [TCP Tuning](https://confluence.nvidia.com/pages/viewpage.action?spaceKey=NSWX&title=Tuning+Guide)
- [Intel SPR Tuning for Best TCP Performance](https://confluence.nvidia.com/display/SW/Intel+SPR+Tuning+for+Best+TCP+performance)
- [Intel Sapphire Rapid Can't Handle PCIe Relaxed Ordering](https://confluence.nvidia.com/display/SW/Intel+Sapphire+Rapid+Can%27t+handle+PCIe+Relaxed+Ordering)
- [NVMeoF for NVNetPerf](https://gitlab-master.nvidia.com/nvcert/nvcert-storage/-/tree/main/nvmeof?ref_type=heads)

## Authors
Guy Mittelman <guymi@nvidia.com>

Hala Awisat <hawisat@nvidia.com>

Anuradha Karuppiah <anuradhak@nvidia.com>

Oded Angel <oangel@nvidia.com>

Hadeel Hamdan <hadeelh@nvidia.com>