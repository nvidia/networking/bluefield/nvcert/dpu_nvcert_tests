
#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

import sys
import os
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    from common.basic_classes import traffic_base_class as tbc
    import subprocess
    import time
    import re
    
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class TelemetryDiagBaseClass(tbc.TrafficBaseClass):
    """
    Telemetry Diag Base Class
    """

    def __init__(self):
        super().__init__()
        self.base_dir = constants.TestsPaths.FILES_DIR.value
        self.compile_dir = f"{self.base_dir}/telemetry_diag_compile/"
        self.filtered_pcie_dict = {}


    def compile_telemetry_diag_sample(self, idx):
        """Compile the telemetry diag sample on each host.
        Raises:
            Exception: If the compilation or running the telemetry diag tool fails.
        """

        for host in self.node_group_dict[idx].hosts.values():
            # Transfer the telemetry diag json file from the test server to the host
            host.put(f"{constants.NVNETPERF_ROOT}/tests/telemetry_diag/telemetry_diag_file.json", f"{self.base_dir}/telemetry_diag_file.json", None)
            
            # Create a new directory for each host for the compilation
            host.run(f"mkdir -p {self.compile_dir}")
            
            # Try to compile the telemetry diag sample
            try:
                telemetry_diag_sample_path = f"{constants.TestsPaths.DOCA_DIR.value}/samples/doca_telemetry/telemetry_diag"
                host.run(f"cd {telemetry_diag_sample_path} && meson setup {self.compile_dir} ")
                host.sudo(f"ninja -C {self.compile_dir}")
            except Exception as e:
                host.remove_path(self.compile_dir, recursive=True)
                host.remove_path(f"{self.base_dir}/telemetry_diag_file.json") 
                self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
                self.logger.error(f"{idx}- An error occurred during the compilation of the telemetry diag sample on host {str(host.oob_address)}. Error: {str(e)}")
                raise

            self.logger.info(f"{idx}- Telemetry diag sample compilation completed successfully for host {str(host.oob_address)}")


    def run_telemetry_diag_sample(self, idx , state: constants.TestState):
        """Run the tool on each pcie on each host.
        Save the results in CSV files and transfer them to the test-server for further processing.
        Args:
            state (constants.TestState): Indicates the test condition (steady or stress) to determine file naming.
        Raises:
            Exception: If running the telemetry diag tool fails.
        """ 

        telemetry_diag_runtime = 5
        success_message = "Sample finished successfully"
        
        # Use the filtered PCIe list based on the current host
        for host_idx, host in enumerate(self.node_group_dict[idx].hosts.values(), start=1):
            filtered_pcie_list = self.filtered_pcie_dict[idx]['host_1'] if host_idx == 1 else self.filtered_pcie_dict[idx]['host_2']
            for pcie in filtered_pcie_list:

                    output_file_path = f"{self.base_dir}/{idx}_host{host_idx}_{pcie}_{state.value}.csv"
                    
                    # Deletes the previous output file if it exists
                    host.remove_path(output_file_path)
                    if self.test_server_node.check_file_exists(output_file_path):
                        self.test_server_node.remove_dir(output_file_path)

                    # Runs the doca telemetry diag tool on the PCIe
                    try:
                        self.logger.info(f"{idx}- start the sample for {telemetry_diag_runtime} seconds")
                        cmd = f"{self.compile_dir}/doca_telemetry_diag -p {pcie} -o {output_file_path} -rt {telemetry_diag_runtime} -di {self.base_dir}/telemetry_diag_file.json "
                        command_output = host.sudo(cmd)

                    except Exception as e:
                        host.remove_path(self.compile_dir, recursive=True)
                        host.remove_path(f"{self.base_dir}/telemetry_diag_file.json") 
                        self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
                        self.logger.error(f"{idx}- An error occurred during the running of telemetry diag tool. On host- {str(host.oob_address)}.  Error: {str(e)}")
                        raise

                    if success_message in command_output:
                        # Copy the output file from the remote server to the test server
                        file_dest = f"{self.base_dir}/{idx}_host{host_idx}_{pcie}_{state.value}.csv"
                        host.get(output_file_path, file_dest)
                    else:
                        self.logger.error(f"{idx}- Failed to run the doca telemetry diag tool on {pcie} on host - {str(host.oob_address)}.")
                        self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
    

    def run_telemetry_diag_stress(self, idx):
        """  Runs stress on the system by executing an RDMA stress test between the hosts,
        Detect when the stress begins in the script, and when it occurs, simultaneously run the doca-telemetry-diag-sample.
        Raises:
            Exception: If the network connection is down or if running the RDMA test fails.
        """
        node_group = self.node_group_dict[idx]
        stress_runtime = len(self.filtered_pcie_dict[idx]['host_1']) * 10
        if 'host_2' in node_group.hosts:
            stress_runtime += len(self.filtered_pcie_dict[idx]['host_2']) * 10

        cmd = self.generate_base_rdma_cmd(idx)
        cmd += " --tests=ib_write_bw"
        cmd += f" --duration={stress_runtime}"
        cmd += f" --bw_message_size_list={self.bw_message_size_list}" if self.bw_message_size_list else ""

        self.logger.info(f"{idx}- Running: {cmd}")

        # Execute the command using subprocess
        process = subprocess.Popen(
            cmd, 
            shell=True, 
            stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT,  # Redirect stderr to stdout
            universal_newlines=True
        )
        
        # Monitor the output in real-time and track occurrences of the message
        stress_message = "INFO: run ib_write_bw client on"
        stress_message_count = 0
        host1_mlxdev_count = len(node_group.hosts['host_1'].mlxdevs)
        is_back_to_back = node_group.topology == constants.HostNodeTopology.B2B.value
        is_loopback = node_group.topology == constants.HostNodeTopology.Loopback.value
        output_lines = []

        for line in process.stdout:
            self.logger.info(line.strip())
            output_lines.append(line.strip())
            
            # Check if the stress message appeared in the output
            if stress_message in line:
                stress_message_count += 1
                self.logger.info(f"{idx} Detected 'run ib_write_bw' message, count: {stress_message_count}")

                # Trigger after the number of mlxdev occurrence
                if  ((stress_message_count == host1_mlxdev_count and is_back_to_back) or
                    (stress_message_count == host1_mlxdev_count / 2 and is_loopback)):
                    # Checking if the process is still running
                    if process.poll() is None:
                        self.logger.info(f"{idx}- Starting telemetry diagnostics after detecting the 'run ib_write_bw' message of the requires number of mlxdev.")
                        time.sleep(1)
                        self.run_telemetry_diag_sample(idx, constants.TestState.STRESS)
                    else:
                        self.logger.error(f"{idx}- RDMA test terminated unexpectedly. Skipping telemetry diagnostics.")
                        raise Exception("RDMA test terminated unexpectedly.")
            
        # Wait for the process to finish
        process.wait()

        # Checking whether the NGC RDMA script managed to produce any result, meaning it successfully ran.
        output = "\n".join(output_lines)
        bw_pat = re.compile(r'Device mlx5_\d+ reached (\d+) Gb/s \(max possible: (\d+) Gb/s\)')
        bw_match = bw_pat.search(output)
        if not bw_match:
            error_message = f"{idx}- NGC script did not produce a result. Stress was not performed."
            self.logger.error(error_message)
            raise Exception(error_message)
        
        # Handle errors, if any
        if process.returncode != 0:
            self.logger.error(f"{idx}- ngc-rdma-test failed with error.")
            raise Exception("Failed to run ngc_rdma_test")


    def cleanup_telemetry_diag(self, idx):
        """Deleting the compile directory and the output file from the hosts"""
        for host in self.node_group_dict[idx].hosts.values():
            host.remove_path(f"{self.base_dir}/telemetry_diag_file.json")
            host.remove_path(self.compile_dir, recursive=True) 
        self.logger.info(f"{idx}- Telemetry cleanup completed successfully.")


    def filter_pcie_addresses(self, pcie_list):
        """
        Filters PCIe addresses to keep only the first occurrence for each unique prefix (the part before the last '.').
        Args:
            pcie_list (list): List of PCIe addresses.
        Returns:
            list: Filtered list with unique prefixes, keeping only the first entry for each prefix.
        """
        pcie_list_copy = sorted(pcie_list)
        # Start with the first PCIe address in the sorted list
        filtered_pcie = [pcie_list_copy[0]]
        for i in range(1, len(pcie_list_copy)):
            # Compare current PCIe address to the previous one, based on the part before the last '.'
            current_prefix = pcie_list_copy[i].rsplit(".", 1)[0]
            previous_prefix = pcie_list_copy[i - 1].rsplit(".", 1)[0]
            if current_prefix != previous_prefix:
                filtered_pcie.append(pcie_list_copy[i])
        return filtered_pcie