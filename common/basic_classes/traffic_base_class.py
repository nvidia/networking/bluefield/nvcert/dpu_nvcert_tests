#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    import time
    import re
    from common.basic_classes import nvnetperf_base_class as nbc
    from tests.performance_configuration_readiness.wrappers import host_pci_check as pci_check
    from tests.performance_configuration_readiness.wrappers import system_hw_inventory_check as port_check
    from common.wrappers import pause_frames_wrapper as pause_frames_check
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class TrafficBaseClass(nbc.NVNetPerfBaseClass):
    """
    Traffic Base Class
    """
    def __init__(self):
        super().__init__()
        self.vteps = []
        self.read_lat_pass_rate = 0
        self.write_lat_pass_rate = 0
        self.send_lat_pass_rate = 0
        self.pause_frames_results = []

    def args_test(self):
        super().args_test()
        self.parser.add_argument("--mode", "-m", default="ovs", choices=["ovs", "hbn", "ipsec"],
                        help="Specify the test-traffic mode")
        self.parser.add_argument("--skip_speed_check", action='store_true',
                        help="Skip pre test ports and cables speed check ")
        self.parser.add_argument("--disable_ports_config", action='store_true',
                        help="Disable the ports configuration step")
        self.parser.add_argument("--disable_ports_deconfig", action='store_true',
                        help="Disable the ports deconfiguration step")


    def set_variables_from_file(self, inventory, params):
        super().set_variables_from_file(inventory, params)
        self.mode = params["tests"][self.test_run_name]["mode"]
        self.skip_speed_check = params["tests"][self.test_run_name]["skip_speed_check"]
        self.disable_ports_config = params["tests"][self.test_run_name]["disable_ports_config"]
        self.disable_ports_deconfig = (params["tests"][self.test_run_name]["disable_ports_deconfig"] or params["tests"][self.test_run_name]["disable_ports_config"])

    def args_parsed(self):
        super().args_parsed()
        self.mode = self.args.mode
        self.test_name = f"{self.test_name}_{self.mode}" if(self.mode and self.test_name != "gdr_test") else self.test_name
        self.skip_speed_check = self.args.skip_speed_check
        self.disable_ports_config = self.args.disable_ports_config
        self.disable_ports_deconfig = (self.args.disable_ports_deconfig or self.args.disable_ports_config)


    def test_setup(self, config_ports=True, loopback_supported=False, ib_supported=False):
        self.benchmark_rdma_test = f"{self.root_dir}/ngc_multinode_perf/ngc_rdma_test.sh"
        if not config_ports:
            self.disable_ports_deconfig = True
        if self.disable_ports_config:
            config_ports = False
        super().test_setup(config_ports, loopback_supported, ib_supported)
        if self.enable_passwordless_access and self.mode != constants.TrafficMode.IPSEC.value:
            self.server_handler.setup_host_ssh_keys(self.node_group_dict)
        self.compute_pass_rates()


    def remove_old_ngc_result_files(self):
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                host.remove_path("/tmp/perftest_ib_*")

    def check_pause_frames(self, pre_data=[]):
        """
        Checks the pause frames status for network devices on all hosts in the node groups.

        This method iterates through all hosts in the node groups, retrieves the network device for each PCIe address,
        and checks if pause frames are enabled. It then collects the pause frame statistics and compares them with
        previously recorded data if provided.

        Args:
            pre_data (list, optional): A list of dictionaries containing previous pause frames data to compare against.

        Returns:
            Pause Frames status SUCCESS/FAIL from constants.ExitStatus.

        """
        results = []
        for node_group in self.node_group_dict.values():
            for host in node_group.hosts.values():
                for pci in host.pcie_address:
                    net_dev = self.node.HostNode.get_net_device(host, pci)
                    if pause_frames_check.is_pause_frames_enabled(net_dev, host):
                        results.append(pause_frames_check.check_pause_frames_status(net_dev, host))
        if pre_data:
            return pause_frames_check.get_pause_frames_status(results, pre_data)
        else:
            self.pause_frames_results = results
        return constants.ExitStatus.NVNETPERF_SUCCESS.value

    def pre_test_logic(self):
        if not self.skip_system_validation:
            self.check_pause_frames()
        if not self.skip_speed_check:
            pci_status_count = 0
            hosts_list = []
            for node_group in self.node_group_dict.values():
                for host in node_group.hosts.values():
                    hosts_list.append(host)
                    for pci in host.pcie_address:
                        pci_result = pci_check.get_pci_type(host, pci)
                        if not pci_result[1]:
                            pci_status_count += 1
            port_result = port_check.check_cable_phy_matching(hosts_list)

            if not port_result[1] or pci_status_count > 1:
                self.pre_test_logic = constants.ExitStatus.NVNETPERF_PERF_ERROR.value
                raise Exception("Performance Error: Pre Test Logic failed due to non-optimal configurations!")

        # Temporary workaround for the NGC bug:
        # RDMA files do not have timestamps, which may result in inaccurate outcomes if an NGC failure occurs.
        self.remove_old_ngc_result_files()
        return self.pre_test_status


    def post_test_logic(self):
        super().post_test_logic()
        if not self.skip_system_validation:
            pause_frames_status = self.check_pause_frames(self.pause_frames_results)
            if pause_frames_status == constants.ExitStatus.NVNETPERF_FAIL.value:
                self.post_status = constants.ExitStatus.NVNETPERF_FAIL.value
        self.server_handler.clear_test_setup_resources(self.node_group_dict, self.disable_ports_deconfig)


    def compute_pass_rates(self):
        """
        set pass rates for the various benchmark tests
        """
        # in micro-seconds
        self.read_lat_pass_rate = 4.5
        self.write_lat_pass_rate = 2.5
        self.send_lat_pass_rate = 2.5


    def generate_base_rdma_cmd(self, idx, single_port = False):
        """"
        Prepares and formats the command to run the RDMA test, including necessary pre-test setup.
        """
        node_group = self.node_group_dict[idx]
        self.cleanup_before_rdma(node_group)
        host_1 = node_group.hosts['host_1']
        cmd  = ""
        if node_group.topology == constants.HostNodeTopology.Loopback.value:
            cmd = f"{self.benchmark_rdma_test} {host_1.uname}@{host_1.oob_address} {','.join(host_1.mlxdevs[0::2])} {host_1.uname}@{host_1.oob_address} {','.join(host_1.mlxdevs[1::2])}"
        if node_group.topology == constants.HostNodeTopology.B2B.value:
            host_2 = node_group.hosts['host_2']
            if (len(host_1.mlxdevs) != len(host_2.mlxdevs)):
                self.test_status = constants.ExitStatus.NVNETPERF_PARAM_ERROR.value
                raise Exception(f"Number of mlx devices for the two hosts in {idx} doesn't match, please review your inventory file")
            # HBN currently runs for single DPU
            if single_port:
                cmd = f"{self.benchmark_rdma_test} {host_1.uname}@{host_1.oob_address} {host_1.mlxdevs[0]} {host_2.uname}@{host_2.oob_address} {host_2.mlxdevs[0]}"
            else:
                cmd = f"{self.benchmark_rdma_test} {host_1.uname}@{host_1.oob_address} {','.join(host_1.mlxdevs)} {host_2.uname}@{host_2.oob_address} {','.join(host_2.mlxdevs)}"
        return cmd


    def cleanup_before_rdma(self, node_group):
        """
        Cleanup leftovers of previous RDMA processes, if they exist
        """
        for host in node_group.hosts.values():
            for test in ['ib_write_bw', 'ib_read_bw', 'ib_send_bw', 'ib_write_lat', 'ib_read_lat', 'ib_send_lat']:
                cmd = f"ps -aux | grep '{test} -d' | grep -v 'grep' | awk '{{print $2}}'"
                output = host.run(cmd, warn=True, hide="stderr")
                pids = " ".join([pid.strip() for pid in output.split('\n')])
                if pids:
                    kill_cmd = f"kill -9 {pids}"
                    self.logger.info(f"Running {kill_cmd}")
                    host.sudo(kill_cmd)


    def run_ping_with_backoff(self, backoff = 3, attempts = 3):
        """
        run a ping between the hosts. if it doesn't succeed, back off for 1 second
        and retry
        """
        self.logger.info("Running PING test")
        all_senders = 0
        passed_senders = 0
        for node_group in self.node_group_dict.values():
            # Run ping only in case of back-to-back
            if node_group.topology == constants.HostNodeTopology.B2B.value:
                r_val = False
                host_1 = node_group.hosts['host_1']
                host_2 = node_group.hosts['host_2']
                # skip in case one of the hosts is IB (no IPs)
                if host_1.link_type != constants.LinkLayerType.INFINIBAND.value and host_2.link_type != constants.LinkLayerType.INFINIBAND.value:
                    self.logger.info(f"Running ping test: {host_1.oob_address} <-> {host_2.oob_address}")
                    senders = [netdev.ipv4_addrs[0].ip for netdev in host_1.netdevs]
                    all_senders += len(senders)
                    receivers = [netdev.ipv4_addrs[0].ip for netdev in host_2.netdevs]
                    r_vals = []
                    for sender, receiver in zip(senders, receivers):
                        while attempts:
                            r_val = self.run_ping(sender, receiver, host_1)
                            if r_val:
                                r_vals.append(r_val)
                                passed_senders += 1
                                break
                            attempts = attempts - 1
                            if attempts and backoff:
                                time.sleep(backoff)
                else:
                    self.logger.info(f"Skipping ping test for {host_1.oob_address} and {host_2.oob_address}")
            else:
                continue
        return (all_senders == passed_senders)


    def run_ping(self, sender, receiver, host_1):
        """
        ping host-2 from host-1
        """
        count = 3
        cmd = f"ping -c {count} -I {sender} {receiver}"
        output = host_1.sudo(cmd)
        ping_pat = re.compile(r"(?P<sent>\d+) packets transmitted, (?P<received>\d+) received")
        obj = ping_pat.search(output)
        if obj:
            sent = int(obj.group("sent"))
            received = int(obj.group("received"))
        else:
            sent = 0
            received = 0
        return bool(sent == count and received == count)


    def create_evpn_vxlan_bridge(self):
        """
        Setup EVPN VxLAN bridge on the DPUs, ping from one host to
        another and check if the MACs are advertised via EVPN
        """
        node_group = self.node_group_dict['node_group_1']
        self.vteps = self.server_handler.create_vteps(node_group)
        # Check ping over the EVPN-VxLAN bridge
        if not self.run_ping_with_backoff():
            self.test_status = constants.ExitStatus.NVNETPERF_COMM_ERROR.value
            self.logger.error("Network connection is down between the hosts")
            return False
        self.logger.info("Network connectivity is up between the hosts")

        self.logger.info("Sleeping for 10 seconds to accommodate inter-component delays")
        time.sleep(10)

        # check if the host macs (local and remote) are present in zebra
        macs = self.server_handler.get_host_macs(node_group.hosts['host_1'], node_group.hosts['host_2'])
        for vtep in self.vteps:
            if not vtep.check_macs(macs):
                self.logger.error(f"EVPN mac table is missing the host macs {macs}")
                self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
                return False

        self.logger.info("VTEP EVPN mac table has been populated")
        return True


    def create_special_devices_dict(self, hosts):
        """
        Create a dict of dicts to represent special cases devices, each sub-dict contains lists that has the mlx devices corresponding to these special cases
        The output format is: {"B3240": {"host_1": [['mlx5_0', 'mlx5_1'], ['mlx5_6', 'mlx5_7']], "host_2": [['mlx5_4', 'mlx5_5'], ['mlx5_2', 'mlx5_3']]}}
        where: for host_1, mlx5_0 and mlx5_1 are connected on the same pcie mst device (dev0 and dev0.1)
        """
        out_dict = {}
        special_cases = ["B3240"]
        for case in special_cases:
            for host in hosts:
                if case == "B3240":
                    # Case: B3240 with 400 Gb/s total limitation
                    cmd = "mst status -v | grep BlueField3 | awk '{print $2 \" \" $4}' | sed 's/\.1//' | awk '{print $1 \" \" $2}' | sort -k1,1 | awk '{devices[$1] = devices[$1] ? devices[$1] \" \" $2 : $0} END {for (device in devices) print devices[device]}'"
                    output = host.sudo(cmd)
                    # The format of the output:
                    # /dev/mst/mt692_pciconf0 mlx5_2 mlx5_3
                    # /dev/mst/mt692_pciconf1 mlx5_0 mlx5_1
                    lines = output.split("\n")
                    for line in lines:
                        mst_dev = line.split(" ")[0]
                        cmd = f"mlxconfig -d {mst_dev} q | grep Description | grep -oE 'B32[0-9]{{2}}'"
                        dpu_description = host.sudo(cmd)
                        if case in dpu_description:
                            out_dict.setdefault(f"{case}", {})
                            out_dict[f"{case}"].setdefault(f"{host.oob_address}", [])
                            mlx_devs = line.split(" ")[1:3]
                            out_dict[f"{case}"][f"{host.oob_address}"].append(mlx_devs)
        return out_dict


    def set_subtest_data(self, test_name, result, hosts={"server1":"", "dev1":"", "server2":"", "dev2":""}, mode=constants.TrafficTestType.BW.value, threshold={"expected": "N/A", "info": ""}, case_status=True, idx=1):
        self.traffic_data = {
            "host_1": hosts["server1"],
            "host_2": hosts["server2"],
        }
        if mode == constants.TrafficTestType.BW.value:
            self.traffic_data["bw_avg"] = {
                "server_device": hosts["dev1"],
                "client_device": hosts["dev2"],
                "bw_average[Gb/sec]": result if result else "N/A"
            }
            self.traffic_thresholed = {
                "min_throughput": f"{threshold['expected']} Gb/sec",
                "info": threshold["info"],
            }
        else:
            self.traffic_data["lat_avg"] = {
                "server_device": hosts["dev1"],
                "client_device": hosts["dev2"],
                "lat_average[us]": result if result else "N/A"
            }
            self.traffic_thresholed = {
                "max_latency": f"{threshold['expected']} us",
                "info": threshold["info"],
            }
        self.subtests_list = self.json_helper.create_json_subtest(test_name, self.traffic_data, self.traffic_thresholed, case_status, idx)