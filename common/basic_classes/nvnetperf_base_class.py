#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#



import sys
from common import constants

sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    import argparse
    from datetime import datetime
    import time
    from tabulate import tabulate
    import logging
    import traceback
    from concurrent.futures import ThreadPoolExecutor
    from common.utils import log_formatter
    from common.utils import server_connection_handler as server_handler
    from common.wrappers import system_log_error_inspector_wrapper as sys_log_handler
    from common.utils import node
    from common.utils import json_output_utils
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


class NVNetPerfBaseClass():
    """
    Perf Test Base class (Base Framework) contains common params for any test
    """

    general_test_suite = {
        "verbosity": "DEBUG",                      # Logging level: DEBUG, INFO, WARN, ERROR
        "log_dir": "/tmp/doca_perftest_logs/",     # Directory for logs
        "enable_passwordless_access": False,       # Enable SSH key generation for remote access
        "skip_system_validation": False,           # Skip system validation after test
        "root_dir": ".",                           # Root directory for the test runner
    }

    def __init__(self):

        self.test_name = ""
        self.test_run_name = self.test_name
        self.logger = logging.getLogger(__file__)
        self.server_handler = server_handler
        self.node_group_dict = None
        self.inventory = None
        self.node = node
        self.root_dir = constants.DEFAULT_ROOT_DIR
        self.test_start_time = datetime.now()
        self.captured_syslogs_time = self.test_start_time.strftime("%Y-%m-%d %H:%M:%S")
        self.pre_test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
        self.test_server_node = self.node.LocalNode()
        self.json_helper = json_output_utils
        self.subtests_list = {} # dict of subtests
        self.all_servers_result = []


    def _test_description(self):
        if not self.__doc__:
            raise NotImplementedError("Test must implement class level description")
        return self.__doc__


    def _args_common(self):
        """
        Framework arguments.
        Here comes arguments that are for the framework (all tests)
        """
        self.parser.add_argument("--verbosity", "-v", dest="verbosity", default=constants.VERBOSE_LEVEL,
                            help="The logging level for output")
        self.parser.add_argument("--inventory", "-i", dest="inventory", default=f"{constants.NVNETPERF_ROOT}/hosts.yaml",
                            help="Specify the inventory yaml file, default is hosts.yaml")
        self.parser.add_argument("--logfile", "-f", dest="logfile", default="",
                            help="Specify the full path to log file")
        self.parser.add_argument("--skip_system_validation", action='store_true',
                            help="Skip post test system validation")
        self.parser.add_argument("--enable_passwordless_access", action='store_true',
                            help="Enable Passwordless Access (Generate SSH Keys) for the remote hosts. Default is disabled, the user need to create it.")
        # Secret flag to update the root directory
        # It will have a different value when running from a PyInstaller executable
        self.parser.add_argument("--root_dir", "-r", dest="root_dir", default=f"{constants.DEFAULT_ROOT_DIR}",
                            help=argparse.SUPPRESS)

    def set_variables_from_file(self, inventory, params):
        self.logfile = params["general"]["log_file"]
        self.root_dir = params["general"]["root_dir"]
        self.inventory = inventory
        self.verbosity = params["general"]["verbosity"]
        self.skip_system_validation = params["general"]["skip_system_validation"]
        self.enable_passwordless_access = params["general"]["enable_passwordless_access"]

    def args_test(self):
        """
        Test specific arguments.
        Here comes arguments that are meant for the test case itself
        """
        pass


    def args_execute(self):
        """
        called from the execute method, all the process of parsing the arguments is managed from this method.
        parsing the args also happens here (self.args = self.parser.parse_args())
        """
        self.parser = argparse.ArgumentParser(description="NVNetPerf tests CLI",
            formatter_class=argparse.RawDescriptionHelpFormatter)
        self._args_common()
        self.args_test()

        self.args = self.parser.parse_args()
        self.args_parsed()


    def args_parsed(self):
        """
        after all the arguments have been parsed this method is called to give the class variables the values.
        the values are either from the arguments or from enum files
        """
        # Args has been parsed (can collect and act based on these)
        self.verbosity = self.args.verbosity
        self.inventory = self.args.inventory
        self.root_dir = self.args.root_dir
        self.logfile = self.args.logfile
        self.skip_system_validation = self.args.skip_system_validation
        self.enable_passwordless_access = self.args.enable_passwordless_access


    def test_setup(self, config_ports=False, loopback_supported=False, ib_supported=False):
        # Setup test pre conditions
        self.logger.info("Setup test pre conditions")
        self.json_helper.create_json_out_dir(self.logfile, self.test_server_node)

        node_group_dict = self.server_handler.test_setup_resources(self.inventory,config_ports=config_ports,
                                                                    loopback_supported=loopback_supported,
                                                                    ib_supported=ib_supported, root_dir=self.root_dir)
        self.node_group_dict = node_group_dict


    def get_log_file(self):
        timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        log_file = self.logfile
        if not self.test_server_node.check_file_exists(constants.TestsPaths.FILES_DIR.value):
            self.test_server_node.create_dir(constants.TestsPaths.FILES_DIR.value)
        if not self.logfile:
            log_file = f"{constants.TestsPaths.FILES_DIR.value}/{self.test_name}_{timestamp}.log"
        return log_file


    def pre_test_logic(self):
        """
        logic for Pre test
        Validate if the setup ready to run test_logic()
        Return pre_test_status
        """
        self.logger.info("Pre Test Logic")
        return self.pre_test_status


    def test_logic(self):
        """
        Return test_status
        """
        raise NotImplementedError("test_logic must be implemented")


    def post_test_logic(self):
        """
        logic for post test.
        Returning the setup to initial state, the connections will be closed after sanity tests
        """
        self.post_status = constants.ExitStatus.NVNETPERF_SUCCESS.value
        if not self.skip_system_validation:
            hosts_list = []
            for node_group in self.node_group_dict.values():
                hosts_list += node_group.hosts.values()
            # Cleanup older files
            [self.test_server_node.sudo(f"rm -f /tmp/dmesg_logs_{host.oob_address}.txt") for host in hosts_list]
            [self.test_server_node.sudo(f"rm -f /tmp/syslog_logs_{host.oob_address}.txt") for host in hosts_list]
            with ThreadPoolExecutor() as executor:
                tasks = [executor.submit(sys_log_handler.test_system_log_error_inspector, self.captured_syslogs_time, host) for host in hosts_list]
                for task in tasks:
                    try:
                        val = task.result(timeout=30)
                        if val != constants.ExitStatus.NVNETPERF_SUCCESS.value:
                            self.post_status = constants.ExitStatus.NVNETPERF_FAIL.value
                    except TimeoutError:
                        self.logger.error(f"Post Logic reached timeout")
                        self.post_status = constants.ExitStatus.NVNETPERF_TIMEOUT.value
        else:
            self.post_status = constants.ExitStatus.NVNETPERF_DISABLED.value
        self.json_helper.generate_test_json_file(self.test_start_time, self.test_run_name, self.subtests_list, self.test_status, self.logfile)


    def print_version(self):
        self.logger.info(f"NVNetPerf Version: {constants.VERSION}")
    
    def execute_test(self, test_name, inventory, params):
        self.test_run_name = test_name
        self.set_variables_from_file(inventory, params)
        
        return(self.execute())


    def run_test_on_server(self, server, **kwargs):
        """
        Run test on one server, this must be overridden by subclass
        """
        raise NotImplementedError("run_test_on_server must be implemented")


    def print_results(self):
        """
        Print test's results, this must be overridden by subclass
        """
        raise NotImplementedError("print_results must be implemented")


    def run_test_on_all_servers(self, **kwargs):
        """
        Run test on all servers in the given list
        """
        servers = kwargs["servers"]
        success_counter = 0
        start_time = time.time()
        exit_status = constants.ExitStatus.NVNETPERF_FAIL.value
        with ThreadPoolExecutor() as executor:
            task_to_server = {}
            task_start_time = {}
            tasks = []
            for server in servers:
                task = executor.submit(self.run_test_on_server, server, **kwargs)
                task_to_server[task] = server
                task_start_time[task] = time.time()
                tasks.append(task)
            for task in tasks:
                server = task_to_server[task]
                start_time = task_start_time[task]
                try:
                    exit_status = task.result(timeout=kwargs["timeout"])
                    hosts_info = f"Host-1:{self.node_group_dict[server].hosts['host_1'].oob_address}"
                    if self.node_group_dict[server].topology == constants.HostNodeTopology.B2B.value:
                        hosts_info = f"{hosts_info}, Host-2:{self.node_group_dict[server].hosts['host_2'].oob_address}"
                    self.all_servers_result.append({"Server node": server, 
                                                    "Hosts":  hosts_info,
                                                    "Task Name": self.test_run_name,
                                                    "Task Duration": self.calculate_elapsed_time(start_time, time.time()),
                                                    "Task Status": self.get_status_name_by_value(exit_status), 
                                                    "Task RC": exit_status})

                    if exit_status == constants.ExitStatus.NVNETPERF_SUCCESS.value:
                        success_counter += 1
                except TimeoutError:
                    self.logger.error(f"Test reached timeout")
                    self.test_status = constants.ExitStatus.NVNETPERF_TIMEOUT.value
        if len(self.node_group_dict.keys()) == 1:
            self.test_status = exit_status
        else:
            if success_counter == len(self.node_group_dict.keys()):
                self.test_status = constants.ExitStatus.NVNETPERF_SUCCESS.value 
            else:
                self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value 

    def calculate_elapsed_time(self, start_time, end_time):
        elapsed_time = end_time - start_time
        hours, rem = divmod(elapsed_time, 3600)
        minutes, seconds = divmod(rem, 60)
        return f"{int(hours)}:{int(minutes):02}:{int(seconds):02}"

    def get_status_name_by_value(self, value):
        for member in constants.ExitStatus:
            if member.value == value:
                return member.name
        return None

    def execute(self):
        """
        Common tests steps:
            Args parsing
            Test Preparation
            Test Execution
            Post Test configurations
            Return test status
        """
        self.test_status = constants.ExitStatus.NVNETPERF_FAIL.value
        self.post_status = constants.ExitStatus.NVNETPERF_FAIL.value

        self.logfile = self.get_log_file()

        log_formatter.init_logger("nvnetperf", self.verbosity, self.logfile)
        self.logger = logging.getLogger("nvnetperf")

        self.print_version()
        self.logger.info(self._test_description())
        try:
            self.test_setup() # preparation should be set here!
            self.pre_test_logic = self.pre_test_logic()
            self.test_status = self.test_logic() if self.pre_test_logic == constants.ExitStatus.NVNETPERF_SUCCESS.value else self.pre_test_logic
            self.logger.info("Summary: System Node Testing Results")
            self.logger.info(f"\n{tabulate(self.all_servers_result, headers='keys', tablefmt='orgtbl')}\n")

        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            self.logger.error(message)
            self.logger.error(f"Error backtrace: {traceback.format_exc()}")
            return self.test_status

        finally:
            self.post_test_logic()

        if self.test_status == constants.ExitStatus.NVNETPERF_SUCCESS.value:
            self.logger.info("\x1b[6;30;42m Test Results: SUCCESS \x1b[0m")
            if self.post_status == constants.ExitStatus.NVNETPERF_SUCCESS.value:
                self.logger.info("\x1b[6;30;42m Post test Status: PASSED \x1b[0m")
            elif self.post_status == constants.ExitStatus.NVNETPERF_DISABLED.value:
                self.logger.warn("\x1b[6;30;43m Post test Status: DISABLED \x1b[0m")
            else:
                self.logger.warn("\x1b[6;30;43m Post test Status: FAILED \x1b[0m")
        else:
            self.logger.error(f"\x1b[6;30;41m Test Results: FAILED exit code #{self.test_status} \x1b[0m")
            if self.post_status == constants.ExitStatus.NVNETPERF_SUCCESS.value:
                self.logger.info("\x1b[6;30;42m Post test Status: PASSED \x1b[0m")
            elif self.post_status == constants.ExitStatus.NVNETPERF_DISABLED.value:
                self.logger.warn(f"\x1b[6;30;41m Test Results: FAILED exit code #{self.test_status}. Post Test Status: DISABLED \x1b[0m")
            else:
                self.logger.warn(f"\x1b[6;30;43m Post test Status: FAILED \x1b[0m")
        return self.test_status