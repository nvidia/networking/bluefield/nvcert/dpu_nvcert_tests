#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

import sys
import copy
from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    import logging
    from tabulate import tabulate
    from tests.traffic_tests import rdma_test, tcp_test, gpu_direct_rdma_test
    from tests.performance_configuration_readiness import performance_configuration_readiness_test
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


logger = logging.getLogger("nvnetperf")


def merge_test_suites_config_with_defaults(user_config):
    """
    Set the Default Test Suites and merge it with the user-provided Test suite config "YAML"
    """

    final_test_suite = {}
    default_test_suite = {}
    selected_tests = []

    for test_name in user_config["tests"].keys():
        if user_config["tests"][test_name]["execute_test"]:
            selected_tests.append(test_name)
    if selected_tests:
        default_test_suite = set_default_test_suite(selected_tests)
        final_test_suite = copy.deepcopy(default_test_suite)
    else:
        logger.error(f"Error: No tests to execute in the test suite file. Please fix the test_suite file")
        sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR)
    # Update general parameters with user values
    for key, value in user_config.get("general", {}).items():
        final_test_suite["general"][key] = value
    

    final_test_suite["tests"] = {}
    for test_name in selected_tests:
        if test_name in selected_tests:
            final_test_suite["tests"][test_name] = copy.deepcopy(default_test_suite["tests"][test_name])
            final_test_suite["tests"][test_name].update(user_config.get("tests", {}).get(test_name, {}))
    return final_test_suite

def set_default_test_suite(selected_tests):
    default_test_suite = {}
    nvnetperf_tests = {
        "rdma": lambda: rdma_test.RdmaTest(),
        "tcp": lambda: tcp_test.TcpTest(),
        "gpu": lambda: gpu_direct_rdma_test.GpuDirectRdmaTest(),
        "performance": lambda: performance_configuration_readiness_test.PerformanceConfigurationReadiness()
    }
    default_test_suite["general"] = {}
    default_test_suite["tests"] = {}
    for test_name in selected_tests:
        if test_name.split('_')[0] in nvnetperf_tests:
            if default_test_suite["general"] == {}:
                default_test_suite["general"] = nvnetperf_tests[test_name.split('_')[0]]().general_test_suite
            default_test_suite["tests"][test_name] = copy.deepcopy(nvnetperf_tests[test_name.split('_')[0]]().test_suite)
    return default_test_suite

def get_nvnetperf_tests(inventory, params):

    nvnetperf_tests = {
        "rdma_bw_lat_test": lambda: rdma_test.RdmaTest().execute_test("rdma_bw_lat_test", inventory, params),
        "rdma_ipsec_test": lambda: rdma_test.RdmaTest().execute_test("rdma_ipsec_test", inventory, params),
        "tcp_bw_test": lambda: tcp_test.TcpTest().execute_test("tcp_bw_test", inventory, params),
        "tcp_ipsec_test": lambda: tcp_test.TcpTest().execute_test("tcp_ipsec_test", inventory, params),
        "gpu_direct_rdma_bw_lat_test": lambda: gpu_direct_rdma_test.GpuDirectRdmaTest().execute_test("gpu_direct_rdma_bw_lat_test", inventory, params),
        "performance_configuration_readiness": lambda: performance_configuration_readiness_test.PerformanceConfigurationReadiness().execute_test("performance_configuration_readiness", inventory, params)
    }

    return nvnetperf_tests





