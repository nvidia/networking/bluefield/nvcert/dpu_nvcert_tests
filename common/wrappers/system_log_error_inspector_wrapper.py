#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

import sys
from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    from tabulate import tabulate
    import logging
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


# Keywords indicating errors in system logs
system_logs_fail_keywords = ["error", "critical", "fatal", "panic", "segfault", "segmentation fault",
                            "memory corruption", "overheat", "power failure", "overcurrent", "overvoltage",
                            "undervoltage", "temperature critical", "thermal shutdown", "crash", "fault",
                            "bus error", "pci error", "pcie error", "not syncing", "spurious", "dump",
                            "core dumped", "overflow", "kill", "oomkiller", "abort", "fail"]

logger = logging.getLogger("nvnetperf")
MAX_ERROR_LINES = 10

def test_system_log_error_inspector(captured_time, host):
    system_output = []
    dmesg_output = run_error_check(host, constants.SystemLogs.DMESG.value, captured_time)
    syslog_output = run_error_check(host, constants.SystemLogs.SYSLOG.value, captured_time)

    # Print summary table from system output results.
    if syslog_output[1] or dmesg_output[1]:
        system_output = dmesg_output[1] + syslog_output[1]
        length = min(MAX_ERROR_LINES, len(system_output))
        table = tabulate(system_output[:length], headers="keys", tablefmt="orgtbl")
        logger.info(f"[{host.oob_address}] Summary Table :\n{table}\All information are in these logs files\nDmesg File: {dmesg_output[2]}\nSyslog File: {syslog_output[2]}\n")

    if dmesg_output[0] == constants.ExitStatus.NVNETPERF_SUCCESS.value and syslog_output[0] == constants.ExitStatus.NVNETPERF_SUCCESS.value:
        logger.info(f"[{host.oob_address}] System status: OK. \n")
        return constants.ExitStatus.NVNETPERF_SUCCESS.value
    else:
        logger.error(f"[{host.oob_address}] System status: Critical.\n")
        return constants.ExitStatus.NVNETPERF_FAIL.value


def run_error_check(host, check_type, captured_time):
    result_path = "/tmp/"

    try:
        cmd = "journalctl"
        time_stamp_attribute = f"--since '{captured_time}'"
        cmd_args = ""
        grep = ""

        if check_type == constants.SystemLogs.DMESG.value:
            result_path = f"{result_path}dmesg_logs_{host.oob_address}.txt"
            cmd_args = " -k "
        else:
            result_path = f"{result_path}syslog_logs_{host.oob_address}.txt"
            grep = "| grep -v 'kernel:' "
        cmd = f"{cmd} {cmd_args} {time_stamp_attribute} {grep} > {result_path}"
        host.remove_path(result_path)
        host.sudo(cmd)

    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        logger.error(message)
        logger.error("Failed to check system logs")
        return constants.ExitStatus.NVNETPERF_FAIL.value, []

    finally:
        return check_system_logs(host, check_type, result_path)

def check_system_logs(host, log_type, log_file):
    """
    Check system logs for error-related phrases and compare with reference logs if provided.

    Parameters:
    - log_type (str): Type of log to check ('dmesg' or 'syslog').
    - current_file (str): Path to the current log file.
    - reference_file (str, optional): Path to a reference log file for comparison.
    - diff_file (str, optional): Path to a file for storing log differences.
    """
    error_count = 0
    failed_keywords = []
    failed_keywords_set = set()  # To track unique entries

    logs = []
    host.get(log_file, log_file)
    with open(log_file, 'r') as file:
        logs = file.readlines()

    if len(logs):
        for log in logs:
            if log.strip():
                status = constants.TestStatus.PASSED.value
                found_phrases = [phrase for phrase in system_logs_fail_keywords if phrase in log.lower()]

                if len(found_phrases):
                    error_count += 1
                    status = constants.TestStatus.FAILED.value
                    line = log.split(']')[1] if ']' in log else log
                    entry = {
                        "Host": host.oob_address,
                        "Name": log_type,
                        "Failing phrase": ','.join(found_phrases),
                        "Status": status,
                        "Sentence": line.strip()
                    }
                    # Convert the dictionary to a frozenset to check for uniqueness
                    entry_hashable = frozenset(entry.items())
                    if entry_hashable not in failed_keywords_set:
                        failed_keywords_set.add(entry_hashable)
                        failed_keywords.append(entry)
    else:
        logger.warning(f"file is empty: {log_file}")

    if error_count > 0:
        logger.error(f"[{host.oob_address}] Setup shows Critical/Error status in the system logs.")
        status = constants.ExitStatus.NVNETPERF_FAIL.value
    else:
        logger.info(f"[{host.oob_address}] Setup status OK.")
        status = constants.ExitStatus.NVNETPERF_SUCCESS.value
    return status, failed_keywords, log_file


