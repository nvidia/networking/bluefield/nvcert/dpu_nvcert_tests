#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

import sys
from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    import logging
    from tabulate import tabulate
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


logger = logging.getLogger("nvnetperf")

def is_pause_frames_enabled(net_dev, host):
    """
    Checks if Pause Frames are enabled (both RX and TX) on a given network device.

    This function uses the `ethtool` command to verify if RX and TX pause frames are enabled for a specified network interface.
    It logs the results and returns a boolean indicating the status.

    Args:
        net_dev (str): Network device/interface.
        host (object): Host data.

    Returns:
        bool: True if both RX and TX pause frames are enabled, False otherwise.
    """
    try:
        rx_status = host.sudo(f"ethtool -a {net_dev} | grep -i 'rx' | awk '{{print $2}}'")
        tx_status = host.sudo(f"ethtool -a {net_dev} | grep -i 'tx' | awk '{{print $2}}'")
        if "on" in rx_status.strip() and "on" in tx_status.strip():
            logger.info(f"[{host.oob_address}] {net_dev} Pause Frames is On.")
            return True
        else:
            logger.error(f"[{host.oob_address}] {net_dev} Pause Frames is Off.")
            return False
    except Exception as e:
        logger.error(f"[{host.oob_address}] Failed to determine Pause Frames status for {net_dev}: {e}")
        return False

def check_pause_frames_status(dev, host):
    """
    Checks the current pause frames statistics (RX and TX) for a specified network device on a remote host.

    This function uses the `ethtool -S` command to retrieve the number of RX and TX pause frames 
    for a given network interface on the remote host. It returns the results in a dictionary.

    Args:
        dev (str): Network device/interface.
        host (object): Host data.

    Returns:
        dict: A dictionary containing the host's out-of-band address, network device name, 
              and the RX/TX pause frame counts. For example:
    """
    try:
        # Get RX pause frames count
        rx_count = host.sudo(f"sudo ethtool -S {dev} | grep -i 'rx_pause_ctrl_phy' | awk '{{print $2}}'")
        # Get TX pause frames count
        tx_count = host.sudo(f"sudo ethtool -S {dev} | grep -i 'tx_pause_ctrl_phy' | awk '{{print $2}}'")
        # Log the result
        logger.info(f"[{host.oob_address}] {dev}: RX Pause Frames = {rx_count}, TX Pause Frames = {tx_count}")

        return {"host": host.oob_address, 
                "netdev": dev, 
                "rx_pause_frames": rx_count, 
                "tx_pause_frames": tx_count}
    except Exception as e:
        logger.error(f"[{host.oob_address}] Failed to retrieve RX or TX pause frame statistics for {dev}: {e}")
        return {
            "Host": host.oob_address,
            "netdev": dev,
            "rx_pause_frames": None,
            "tx_pause_frames": None
        }

def get_pause_frames_status(pause_frames_results, pre_results):
    """
    Compares pause frames statistics between two sets of results (current and previous) and determines the status.

    This function calculates the differences in RX and TX pause frames for each network device and host,
    logs the results in a tabulated format, and returns a status based on the comparison.

    Args:
        pause_frames_results (list of dict): The current pause frame statistics. Each dictionary contains:
            - "host" (str): The host address.
            - "netdev" (str): The network device name.
            - "rx_pause_frames" (str or int): The current RX pause frame count.
            - "tx_pause_frames" (str or int): The current TX pause frame count.
        pre_results (list of dict): The previous pause frame statistics in the same format as `pause_frames_results`.

    Returns:
        pause frames status SUCCESS/FAIL from constants.ExitStatus
    """
    comparison_results = []
    comparision_count = 0

    for idx,item in enumerate(pause_frames_results):
        
        comparison_results.append({
            "host": item["host"],
            "netdev": item["netdev"],
            "rx_pause_frames": int(item["rx_pause_frames"]) - int(pre_results[idx]["rx_pause_frames"]),
            "tx_pause_frames": int(item["tx_pause_frames"]) - int(pre_results[idx]["tx_pause_frames"]),
        })
        
        # Check for changes in rx_pause_frames or tx_pasue_frames
        if (
            comparison_results[idx]["rx_pause_frames"] > 0 or
            comparison_results[idx]["tx_pause_frames"] > 0
        ):
            comparision_count += 1

    logger.info(f"Pause Frames Results:\n{tabulate(comparison_results, headers='keys', tablefmt='orgtbl')}")

    if comparision_count:
        return constants.ExitStatus.NVNETPERF_FAIL.value
    return constants.ExitStatus.NVNETPERF_SUCCESS.value