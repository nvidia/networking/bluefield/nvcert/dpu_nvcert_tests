#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


from enum import Enum
from pathlib import Path

NVNETPERF_ROOT = str(Path(__file__).parent.parent.absolute())
DEFAULT_ROOT_DIR = "."
VERBOSE_LEVEL = "DEBUG"
VERSION = "2.10.0002"

class TestsPaths(Enum):
    FILES_DIR = "/tmp/nvnetperf"
    DOCA_DIR = "/opt/mellanox/doca"
    JSON_DIR = "/tmp/nvnetperf/test_json_output/"
    JSON_FOLDER = "test_json_output/"

class TestStatus(Enum):
    PASSED = "Passed"
    FAILED = "Failed"

class LinkLayerType(Enum):
    ETHERNET = "ETH"
    INFINIBAND = "IB"

class TrafficMode(Enum):
    OVS   = "ovs"
    HBN   = "hbn"
    IPSEC = "ipsec"

class HostNodeTopology(Enum):
    B2B = "back-to-back"
    Loopback = "loopback"
    
class TestThroughputPassRatio(Enum):
    HBN_PASS_THROUGHPUT = 90
    OVS_PASS_THROUGHPUT = 90
    IPSEC_PASS_THROUGHPUT = 90
    FIO_PASS_THROUGHPUT = 80
    DOCA_BENCH_THROUGHPUT = 80

class SystemLogs(Enum):
    SYSLOG = "syslog"
    DMESG  = "dmesg"

class TestState(Enum):
    STEADY = "steady"
    STRESS = "stress"

class ExitStatus(Enum):
    NVNETPERF_SUCCESS = 0
    # General fail
    NVNETPERF_FAIL = 1
    # Wrong parameter
    NVNETPERF_PARAM_ERROR = 2
    # Test ran successfully but the result is not sufficient
    NVNETPERF_PERF_ERROR = 3
    # No communication between servers
    NVNETPERF_COMM_ERROR = 4
    # Test did not run, Disabled by user
    NVNETPERF_DISABLED = 5
    # Timeout
    NVNETPERF_TIMEOUT = 6

class CardVersion(Enum):
    CONNECTX = "connectx",
    BLUEFIELD = "bluefield"

class CardGenType(Enum):
    CX6 = "Gen 4",
    CX7 = "Gen 5"
    CX8 = "Gen 5",
    BF3 = "Gen 5",
    BF2 = "Gen 4"

class TrafficTestType(Enum):
    BW  = "BW"
    LAT = "LAT"

