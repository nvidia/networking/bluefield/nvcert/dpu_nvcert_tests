#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

from common import constants

import sys
import time
import logging
from enum import Enum
from string import Template

available_table_id = 9

IP_ADDR_NETMASK = "255.255.255.0"
PORT_UP_CHECK_TIMEOUT = 5
IP_CMD = "/opt/mellanox/iproute2/sbin/ip"  # OFED's iproute2 ip cmd
ARG_PLACEHOLDER = "$arg"

logger = logging.getLogger("nvnetperf")


def _get_table_id():
    """
    get an available routing table_id
    """
    global available_table_id
    available_table_id += 1
    return available_table_id


class LINK_TYPE(Enum):
    ETH = "ETH"
    IB = "IB"


class PortConfig:
    def __init__(self, node, mlxdev, interface, link_type, ip=None, advanced_config=False):
        self.node = node
        self.mlxdev = mlxdev
        self.mlxdev_num = int(mlxdev.split("_")[1])
        self.interface = interface
        self.deconfigure_port_commands = []
        self.link_type = link_type
        self.advanced_config = advanced_config
        if link_type == LINK_TYPE.ETH.value:
            self.ip = ip
            self.ip_subnet = ip.rsplit('.', 1)[0] + '.0'
            self.table_id = _get_table_id()

        # initial values
        if link_type == LINK_TYPE.ETH.value:
            self.initial_port_ip = "0.0.0.0"
            self.initial_port_netmask = None
            self.initial_port_up = False
            self.initial_rp_filter = None
            self.initial_accept_local = None
            self.initial_arp_filter = None
            self.initial_arp_ignore = None
            self.initial_arp_announce = None


    def _is_port_up(self):
        """
        Returns True if port is up
        """
        if self.link_type == LINK_TYPE.ETH.value:
            output = self.node.sudo(f"ibdev2netdev | grep {self.interface}")
            if "down" in output.lower():
                return False
        elif self.link_type == LINK_TYPE.IB.value:
            port_state = self.node.sudo(f"ibstatus {self.mlxdev} | grep 'state' | awk '{{print $3}}' | head -1")
            if "active" not in port_state.lower():
                return False
        return True


    def _wait_for_port_up(self):
        """
        Waits for port up; busy-wait
        """
        start_time = time.time()
        while time.time() - start_time < PORT_UP_CHECK_TIMEOUT:
            if self._is_port_up():
                return True
        return False


    def backup_initial_values(self):
        """
        backup the initial values so they can be restored at the end of the run
        (relevant only in case of link type is ETH)
        """
        if self.link_type == LINK_TYPE.ETH.value:
            ip_address_info = self.node.sudo(f"ifconfig {self.interface} | grep 'inet '")
            if ip_address_info:
                ip_address_info = ip_address_info.split()
                self.initial_port_ip = ip_address_info[1]
                self.initial_port_netmask = ip_address_info[3]

            if self._is_port_up():
                self.initial_port_up = True

            if self.advanced_config:
                self.initial_rp_filter = self.node.sudo(f"cat /proc/sys/net/ipv4/conf/{self.interface}/rp_filter").strip()
                self.initial_accept_local = self.node.sudo(f"cat /proc/sys/net/ipv4/conf/{self.interface}/accept_local").strip()
                self.initial_arp_filter = self.node.sudo(f"cat /proc/sys/net/ipv4/conf/{self.interface}/arp_filter").strip()
                self.initial_arp_ignore = self.node.sudo(f"cat /proc/sys/net/ipv4/conf/{self.interface}/arp_ignore").strip()
                self.initial_arp_announce = self.node.sudo(f"cat /proc/sys/net/ipv4/conf/{self.interface}/arp_announce").strip()


    def _execute_config_and_queue_deconfig(self, command_template: str, config_arg: str = "add", deconfig_arg: str = "del"):
        """
        Executes the given configuration command and stores the corresponding deconfiguration command
        in the deconfigure_port_commands list

        Args:
            command_template:   A string containing the command template with a placeholder ($arg)
            config_arg:         The value to be substituted into the command template for configuration
            deconfig_arg:       The value to be substituted into the command template for deconfiguration
        """
        template = Template(command_template)
        # Apply the configuration argument to the template and execute the command
        config_command = template.substitute(arg=config_arg)
        output = self.node.sudo(config_command)
        if not output.return_code:
            # Append the deconfiguration command to the list
            deconfig_command = template.substitute(arg=deconfig_arg)
            self.deconfigure_port_commands.append(deconfig_command)


    def configure_port(self):
        """
        configure the port, store the de-configure commands and verify port is up
        """
        self.backup_initial_values()
        if self.link_type == LINK_TYPE.ETH.value:
            if self.advanced_config:
                # 0 - No source validation.
                # 1 - Strict Reverse Path mode
                # RX packet=>kernel swap src and dst IP addr fields,try route fake packet.
                # If chosen route goes out through the interface where packet came from,
                # the check is ok. Otherwise, the packet is dropped.
                # 2 - Loose Reverse Path mode
                # RX packet=>tested against the route table, dropped if the src addr
                # is not routable through any interface.
                # Value 1 will prevent from source based routing to work. Use 0 or 2.
                required_rp_filter = 0
                self._execute_config_and_queue_deconfig(
                    command_template=f"bash -c 'echo {ARG_PLACEHOLDER} > /proc/sys/net/ipv4/conf/{self.interface}/rp_filter'",
                    config_arg=required_rp_filter,
                    deconfig_arg=self.initial_rp_filter,
                )

                # If set to 1 - accept packets with local source addr.
                # Needed for routing traffic between 2 local interfaces.
                required_accept_local = 1
                self._execute_config_and_queue_deconfig(
                    command_template=f"bash -c 'echo {ARG_PLACEHOLDER} > /proc/sys/net/ipv4/conf/{self.interface}/accept_local'",
                    config_arg=required_accept_local,
                    deconfig_arg=self.initial_accept_local,
                )

                # 1 is used for source based routing
                # in order to force ARPs for each interface be answered based on whether or
                # not the kernel would route a packet from ARP’d IP out that interface
                required_arp_filter = 1
                self._execute_config_and_queue_deconfig(
                    command_template=f"bash -c 'echo {ARG_PLACEHOLDER} > /proc/sys/net/ipv4/conf/{self.interface}/arp_filter'",
                    config_arg=required_arp_filter,
                    deconfig_arg=self.initial_arp_filter,
                )

                # Modes for reply in response to RX ARP that resolve local target IP addr
                # 1 - reply only if target IP addr is local configured on incoming interface
                required_arp_ignore = 1
                self._execute_config_and_queue_deconfig(
                    command_template=f"bash -c 'echo {ARG_PLACEHOLDER} > /proc/sys/net/ipv4/conf/{self.interface}/arp_ignore'",
                    config_arg=required_arp_ignore,
                    deconfig_arg=self.initial_arp_ignore,
                )

                # Restriction levels for announcing local src IP addr from IP packets in
                # ARP requests sent on interface
                # 2 - Use best local addr for target. Src addr in IP packet is ignored,
                # try to select local preferred addr for communicating with target.
                # Check all IP addrs on all subnets on TX interfaces that include target IP
                required_arp_announce = 2
                self._execute_config_and_queue_deconfig(
                    command_template=f"bash -c 'echo {ARG_PLACEHOLDER} > /proc/sys/net/ipv4/conf/{self.interface}/arp_announce'",
                    config_arg=required_arp_announce,
                    deconfig_arg=self.initial_arp_announce,
                )


             # Configure the port's interface with the required IP address
            self.node.sudo(f"ifconfig {self.interface} {self.ip} netmask {IP_ADDR_NETMASK} up")

            if self.advanced_config:
                # Add rule for local routing of iterface under highest prio
                self._execute_config_and_queue_deconfig(
                    command_template=f"{IP_CMD} rule {ARG_PLACEHOLDER} iif {self.interface} lookup local proto kernel pref 0"
                )

                # Source based routing
                # Each interface has its own routing table
                # Add source based route based on interface's IP addr to the table
                self._execute_config_and_queue_deconfig(
                    command_template=f"{IP_CMD} route {ARG_PLACEHOLDER} {self.ip_subnet}/24 dev {self.interface} src {self.ip} table {self.table_id}"
                )

                # Add local route based on interfaces' IP addr to the table
                self._execute_config_and_queue_deconfig(
                    command_template=f"{IP_CMD} route {ARG_PLACEHOLDER} local {self.ip} dev {self.interface} src {self.ip} table {self.table_id}"
                )

                # Add rule for local source based routing per specific interface to relevant table
                self._execute_config_and_queue_deconfig(
                    command_template=f"{IP_CMD} rule {ARG_PLACEHOLDER} from {self.ip} table {self.table_id} pref 11"
                )

                # Add ip rule to self with priority 12
                self._execute_config_and_queue_deconfig(
                    command_template=f"{IP_CMD} rule {ARG_PLACEHOLDER} to {self.ip} table {self.table_id} pref 12"
                )

                self._execute_config_and_queue_deconfig(
                    command_template=f"{IP_CMD} rule {ARG_PLACEHOLDER} from all oif {self.interface} table {self.table_id} pref 10"
                )

            # wait for the port to come up
            if not self._wait_for_port_up():
                raise Exception(f"port {self.interface} is still down")

        elif self.link_type == LINK_TYPE.IB.value:
            guid = self.node.sudo(f"ibstat -d {self.mlxdev} | grep -i 'Port GUID' | awk '{{print $3}}'")
            self.node.sudo(f"nohup opensm -g {guid} > /dev/null 2>&1 &")


    def deconfigure_port(self):
        """
        deconfigure the port
        """
        for command in reversed(self.deconfigure_port_commands):
            self.node.sudo(command)


    def restore_port_info(self):
        """
        restore port info according to the initial values
        """
        restore_port_cmd = f"ifconfig {self.interface} {self.initial_port_ip}"
        restore_port_cmd += f" netmask {self.initial_port_netmask}" if self.initial_port_netmask else ""
        restore_port_cmd += " up" if self.initial_port_up else " down"
        self.node.sudo(restore_port_cmd)


class HostConfig:
    def __init__(self, node, link_type = LINK_TYPE.ETH.value, topology=""):
        self.node = node
        self.mlxdevs = node.mlxdevs
        self.pf_names = node.pf_names
        self.configured_ports = []  # PortConfig
        self.link_type = link_type
        self.topology = topology
        self.advanced_config = self._is_advanced_config_required()
        # initial values
        self.initial_rp_filter = None

        if link_type == LINK_TYPE.ETH.value:
            self.uplink_networks = [uplink.split('/')[0] for uplink in node.uplink_networks]
            logger.info(f"mlxdevs: {self.mlxdevs}, uplink_networks: {self.uplink_networks}, pf_names: {self.pf_names}")

        elif link_type == LINK_TYPE.IB.value:
            self.uplink_networks = None
            logger.info(f"mlxdevs: {self.mlxdevs}, pf_names: {self.pf_names}")

    def _is_advanced_config_required(self):
        """
        determine if advanced config is required based on the setup topology
        """
        # required only if the server and client will run on the same OS
        return self.topology == constants.HostNodeTopology.Loopback.value

    def _is_rule_exist(
        self, priority: str, from_clause: str = "", lookup_clause: str = ""
    ) -> bool:
        """
        check if a specific routing rule exists in the Linux system

        Args:
            priority        priority of the rule, e.g. "100"
            from_clause     source specification of the rule, e.g. "all"
            lookup_clause   table lookup specification, e.g. "local"

        Return:
            True if the rule exists, False otherwise.
        """
        output = self.node.sudo(f"{IP_CMD} rule show")
        for line in output.splitlines():
            if priority == line.split(":")[0]:
                if not from_clause and not lookup_clause:
                    return True
                elif (f"from {from_clause}" in line and f"lookup {lookup_clause}" in line):
                    return True
        return False


    def _eth_pre_configuration(self):
        """
        pre-steps before configuring the ports (for ETH)
        """
        # Change priority of the global loopback from 0 to 100.
        # This causes lower prio than prio of interface rules that will be added
        # for source based routing.
        # Relevant only in case internal loopback is needed.
        if not self._is_rule_exist(priority="100", from_clause="all", lookup_clause="local"):
            self.node.sudo(f"{IP_CMD} rule add from all lookup local proto kernel pref 100")
        if self._is_rule_exist(priority="0"):
            self.node.sudo(f"{IP_CMD} rule del pref 0")
        # backup initial all.rp_filter value
        initial_rp_filter = self.node.sudo(f"cat /proc/sys/net/ipv4/conf/all/rp_filter")
        self.initial_rp_filter = initial_rp_filter.strip()
        # set all.rp_filter to 0
        self.node.sudo(f"bash -c 'echo 0 > /proc/sys/net/ipv4/conf/all/rp_filter'")


    def _eth_post_configuration(self):
        """
        post-steps after configuring the ports (for ETH)
        """
        # clear neigh and arp cache
        self.node.sudo(f"{IP_CMD} -s -s neigh flush all")
        self.node.sudo(f"{IP_CMD} route flush cache")


    def _check_all_ib_ports_up(self):
        num_ports = len(self.mlxdevs)
        ports_up_num = 0
        for mlxdev, interface in zip(self.mlxdevs, self.pf_names):
            port_config = PortConfig(node=self.node, mlxdev=mlxdev, interface=interface, link_type=self.link_type)
            ports_up_num = ports_up_num + 1 if port_config._is_port_up() else ports_up_num
        return ports_up_num == num_ports


    def configure_all_ports(self):
        """
        configure all the required ports
        """
        if self.link_type == LINK_TYPE.ETH.value:
            if self.advanced_config:
                self._eth_pre_configuration()
            for mlxdev, interface, ip in zip(self.mlxdevs, self.pf_names, self.uplink_networks):
                port_config = PortConfig(node=self.node, mlxdev=mlxdev, interface=interface, link_type=self.link_type, ip=ip, advanced_config=self.advanced_config)
                self.configured_ports.append(port_config)
                port_config.configure_port()
            if self.advanced_config:
                self._eth_post_configuration()

        elif self.link_type == LINK_TYPE.IB.value:
            if not self._check_all_ib_ports_up():
                self.node.sudo(f"pkill -9 opensm")
                for mlxdev, interface in zip(self.mlxdevs, self.pf_names):
                    port_config = PortConfig(node=self.node, mlxdev=mlxdev, interface=interface, link_type=self.link_type)
                    port_config.configure_port()
            if not self._check_all_ib_ports_up():
                logger.error("Not all IB ports are Active, opensm failed")
                sys.exit(1)


    def _eth_post_deconfiguration(self):
        """
        post-steps after deconfiguring the ports
        """
        if not self._is_rule_exist(priority="0"):
            self.node.sudo(f"{IP_CMD} rule add from all lookup local proto kernel pref 0")
        time.sleep(2)
        if self._is_rule_exist(priority="100"):
            self.node.sudo(f"{IP_CMD} rule del pref 100")
        # restore initial all.rp_filter value
        if self.initial_rp_filter:
            self.node.sudo(f"sysctl -w net.ipv4.conf.all.rp_filter={self.initial_rp_filter}")


    def deconfigure_all_ports(self):
        """
        deconfigure all the required ports
        """
        if self.link_type == LINK_TYPE.ETH.value:
            [port.deconfigure_port() for port in self.configured_ports]
            if self.advanced_config:
                self._eth_post_deconfiguration()
            [port.restore_port_info() for port in self.configured_ports]
        elif self.link_type == LINK_TYPE.IB.value:
            self.node.sudo(f"pkill -9 opensm")
