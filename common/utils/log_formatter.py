#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

import logging


def init_logger(log_name, log_level, log_file):
    """
    Initialize logger, containing 2 handlers: a stream handler for terminal output and a file handler for log file
    """

    # Create a logger
    logger = logging.getLogger(log_name)
    logger.setLevel(logging.DEBUG)  # Set logger to lowest level

    # Create handlers
    stream_handler = logging.StreamHandler()
    file_handler = logging.FileHandler(log_file)

    # Set levels for handlers
    level = logging.INFO if log_level == "DEBUG" else log_level
    stream_handler.setLevel(level)
    file_handler.setLevel(logging.DEBUG)

    # Create formatters and add them to handlers
    formatter = logging.Formatter("[%(asctime)s][%(levelname)s]: %(message)s")
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)


    # Add handlers to logger
    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)

    return logger