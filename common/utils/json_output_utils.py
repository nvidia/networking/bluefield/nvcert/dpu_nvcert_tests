#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


import sys

from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")
try:
    import os
    import json
    from datetime import datetime
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc


JSON_OUT_DIR = f"{constants.TestsPaths.JSON_DIR.value}"
subtest_list = {}

def create_json_out_dir(log_path, local_host):
    global JSON_OUT_DIR
    JSON_OUT_DIR = f"{os.path.dirname(log_path)}/{constants.TestsPaths.JSON_FOLDER.value}"
    if not os.path.exists(JSON_OUT_DIR) and not os.path.isdir(JSON_OUT_DIR):
        local_host.create_dir(JSON_OUT_DIR)

def create_json_subtest(test_type="", data="", threshold="", test_success_status=True, node_idx=1):

    subtest = {
        "test_type"          : test_type,
        "data"               : data,
        "threshold"          : threshold,
        "test_success_status": test_success_status
    }
    # Initialize the list for the given node_idx if not already present
    if node_idx not in subtest_list:
        subtest_list[node_idx] = []
    # Append the subtest to the list
    subtest_list[node_idx].append(subtest)
    return subtest_list

def generate_test_json_file(start_time, test_name, subtest_list, test_status, log_path):
    global JSON_OUT_DIR
    total_passed_count = 0
    total_failed_count = 0
    end_time = datetime.now()
    json_path = f"{JSON_OUT_DIR}/{test_name}_{start_time.strftime('%Y-%m-%d_%H-%M-%S')}.json"
    
    tests_json_output = {
        "system": [],
        "summary": {},
        "system_info": {}
    }
    for idx, subtests in subtest_list.items():
        for case in subtests:
            if case["test_success_status"] == True:
                total_passed_count += 1
            else:
                total_failed_count += 1
        tests_json_output["system"].append({
            "node_group": idx,
            "tests_run": [
                {
                    "test": test_name,
                    "subtests": subtests
                }
            ]
        })

    status_name = get_status_name_by_value(test_status)
    # Add summary metadata
    tests_json_output["summary"] = {
        "date": start_time.strftime("%d-%m-%Y"),
        "start_time": start_time.strftime("%H:%M:%S"),
        "end_time": end_time.strftime("%H:%M:%S"),
        "total_passed_cases": total_passed_count,
        "total_failed_cases": total_failed_count,
        "rc_code": test_status,
        "rc_status": status_name,
        "log_path": log_path,
    }
    save_json(tests_json_output, json_path)

def save_json(data, filename):
    with open(filename, "w") as f:
        json.dump(data, f, indent=4)

def get_status_name_by_value(value):
    for member in constants.ExitStatus:
        if member.value == value:
            return member.name
    return None
