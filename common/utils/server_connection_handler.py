

#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2024-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#


import sys
from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import logging
    from common.utils import node
    from common.utils import remote_access_setup
    from tests.traffic_tests.hbn import hbn_evpn
    from fabric2 import Connection
except ImportError as exc:
    raise ImportError (str(exc) + "- import module missing") from exc

logger = logging.getLogger("nvnetperf")
local_node = node.LocalNode()


def setup_dpu_ssh_keys(node_group_dict):
    """
    Some tests (e.g.: IPSec) require passwordless access to the DPUs. Setup ssh keys on the
    test-server and copy to the DPU
    """
    home_dir = local_node.expand_user()
    rsa_file = home_dir + "/.ssh/id_rsa"
    rsa_pub_file = home_dir + "/.ssh/id_rsa.pub"
    new_keys = False
    if not local_node.check_file_exists(rsa_file) or not local_node.check_file_exists(rsa_pub_file):
        cmd = f"ssh-keygen -f {rsa_file} -N '' > /dev/null"
        try:
            local_node.sudo(cmd)
            new_keys = True
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            logger.error(message)
    # Copy the key to the DPUs
    for node_group in node_group_dict.values():
        for host in node_group.hosts.values():
            for dpu in host.dpus.values():
                cmd = f"sshpass -p {dpu.password} ssh-copy-id\
                    -o StrictHostKeyChecking=no -i {rsa_pub_file} {dpu.uname}@{dpu.oob_address}"
                logger.info(f"Running: {cmd}")
                try:
                    local_node.sudo(cmd)
                except Exception as ex:
                    template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                    message = template.format(type(ex).__name__, ex.args)
                    logger.error(message)
    if new_keys:
        # if new keys were generated also copy to the hosts
        setup_host_ssh_keys(node_group_dict)

def test_validate_repo(root_dir=constants.DEFAULT_ROOT_DIR):
    """
    Check if the repo has been cloned correctly and if the patches have been
    applied
    """
    test_dir = f"{root_dir}/ngc_multinode_perf"
    num_files = len(local_node.list_dir(test_dir))
    if not num_files:
        logger.critical("Sub-modules were not setup correctly, please follow the instructions in the README.md to clone the repo.")
        raise Exception("Sub-modules were not setup correctly, please follow the instructions in the README.md to clone the repo.")


def test_setup_resources(inventory, config_ports=False, loopback_supported=False, ib_supported=False, root_dir=constants.DEFAULT_ROOT_DIR):
    """
    Set up the resources needed for running the tests
    """
    # Check if the test repo was set up correctly
    test_validate_repo(root_dir)

    data = remote_access_setup.validate_test_setup(inventory, root_dir)

    # Parse node access info provided in inventory file
    system_data = data["system"]
    node_group_dict = {}
    loopback = False
    is_ib = False

    for key in system_data.keys():
        node_group_data = system_data[key]
        idx = key.split("_")[-1]
        topology = node_group_data.get('topology')
        hosts_data = node_group_data.get('hosts')
        logger.info(f"Creating Node Group #{idx}")
        node_group = node.NodeGroup(idx=idx, topology=topology, config_ports=config_ports, hosts_data=hosts_data)
        node_group_dict[key] = node_group
        # Create FILES_DIR in the hosts
        for host in node_group.hosts.values():
            host.run(f"mkdir -p {constants.TestsPaths.FILES_DIR.value}")
            is_ib += ("IB" in host.link_type)
        loopback += (constants.HostNodeTopology.Loopback.value in topology)

    if loopback and not loopback_supported:
        clear_test_setup_resources(node_group_dict)
        raise Exception("Test isn't supported for loopback topology, please review the connections in the inventory file")

    if is_ib and not ib_supported:
        clear_test_setup_resources(node_group_dict)
        raise Exception("Test isn't supported for IB, please review your setup configurations")

    return node_group_dict


def clear_test_setup_resources(node_group_dict, disable_ports_deconfig=False):
    """
    revert back to the initial configurations
    """
    if not disable_ports_deconfig:
        logger.info("Cleanup port configuration")
        for key in node_group_dict.keys():
            for host in node_group_dict[key].hosts.values():
                if host.config_ports:
                    host.host_config.deconfigure_all_ports()


def setup_host_ssh_keys(node_group_dict):
    """
    Setup ssh keys on the test-server and copy to the x86-host
    """
    home_dir = local_node.expand_user()
    rsa_file = home_dir + "/.ssh/id_rsa"
    rsa_pub_file = home_dir + "/.ssh/id_rsa.pub"
    if not local_node.check_file_exists(rsa_file) or not local_node.check_file_exists(rsa_pub_file):
        cmd = f"ssh-keygen -f {rsa_file} -N '' > /dev/null"
        try:
            local_node.sudo(cmd)
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            logger.error(message)

    # if new keys were generated copy to the hosts
    for key in node_group_dict.keys():
        for host in node_group_dict[key].hosts.values():
            cmd = f"sshpass -p {host.password} ssh-copy-id\
                -o StrictHostKeyChecking=no -i {rsa_pub_file} {host.uname}@{host.oob_address}"
            try:
                local_node.sudo(cmd)
            except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logger.error(message)


def create_vteps(node_group):
    """
    Setup EVPN configuration on the HBN containers running on each of the
    DPUs
    """
    vteps = []
    dpus = []
    for host in node_group.hosts.values():
        if not host.dpus:
            raise Exception(f"Cannot run HBN test on CX, please add DPUs section to both hosts")
        for dpu in host.dpus.values():
            dpus.append(dpu)
    idx = 1
    for dpu in dpus:
        oob_address = dpu.oob_address
        uname = dpu.uname
        password = dpu.password
        uplink_port = dpu.uplink_port
        vtep = hbn_evpn.Vtep(idx=idx, oob_address=oob_address, uname=uname, password=password, uplink_port=uplink_port)
        if vtep.setup():
            logger.info(f"VTEP {oob_address} is ready")
        else:
            logger.critical(f"VTEP {oob_address} is not ready")
            raise Exception(f"VTEP {oob_address} is not ready")
        idx += 1
        vteps.append(vtep)
    return vteps


def get_host_macs(host_1, host_2):
    """
    get the hw addresses of the hosts
    """
    macs = []
    macs.append(host_1.netdevs[0].hwaddress)
    macs.append(host_2.netdevs[0].hwaddress)
    return macs