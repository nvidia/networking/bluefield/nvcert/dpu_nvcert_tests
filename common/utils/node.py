#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2023-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

# pylint: disable=too-few-public-methods
# pylint: disable=too-many-function-args
# pylint: disable=too-many-arguments
# pylint: disable=too-many-instance-attributes

"""
Defines the methods for running commands on remote and local nodes
"""
import sys
import os
from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import ipaddress
    import logging
    import re
    import time
    from fabric2 import Connection, Config
    from fabric2.transfer import Transfer
    from invoke.exceptions import UnexpectedExit, CommandTimedOut
    from invoke import run, sudo
    from paramiko.ssh_exception import SSHException
    from common.utils import ports_configuration
except ImportError as e:
    raise ImportError (str(e) + "- import module missing") from e



logger = logging.getLogger("nvnetperf")


class NodeError(Exception):
    """
    Exception handling - node related failures
    """

class SSHConnectError(Exception):
    """
    Exception handling - ssh related failures
    """

class CommandExecutionError(Exception):
    """
    Exception handling - command failures
    """

class CommandTimeout(Exception):
    """
    Exception handling - command timeout
    """

class TestError(Exception):
    """
    Incorrect API usage in the test
    """


class AttrStr(str):
    """
    Fetch string
    """
    @property
    def stdout(self):
        """
        Define attribute output
        """
        return str(self)


class RemoteNode:
    """
    A device that needs to be accessed via ssh for configuring and running
    tests. Python fabric2 module is used for this remote access.
    """
    def __init__(self, **kwargs):
        self.idx = kwargs.pop("idx", 0)
        self.oob_address = kwargs.pop("oob_address", "")
        self.uname = kwargs.pop("uname", "root")
        self.password = kwargs.pop("password", None)
        self.node_string = self.uname + "@" + str(self.oob_address)
        self.config = None
        self.connect_kwargs = {}

        if self.password:
            self.connect_kwargs['look_for_keys'] = False
            self.connect_kwargs['password'] = self.password
            self.config = Config(
                    overrides={'sudo': {'password': self.password}}, lazy=True)


    def format_output(self, output):
        """
        Format the output into a string and store the metadata as
        attributes
        """
        filtered_out = AttrStr(output.stdout.rstrip("\n"))
        setattr(filtered_out, "return_code", output.return_code)
        setattr(filtered_out, "failed", output.failed)
        setattr(filtered_out, "succeeded", output.ok)
        setattr(filtered_out, "command", output.command)
        setattr(filtered_out, "stderr", output.stderr)

        return filtered_out


    def sudo(self, *args, **kwargs):
        """
        remote sudo-bash command
        """
        kwargs["warn"] = True
        kwargs["hide"] = False
        kwargs["pty"] = False
        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                r_obj = conn.sudo(*args, password=self.password, **kwargs)
            except UnexpectedExit as un_exc:
                if args:
                    raise CommandExecutionError(
                            f"Got the following error trying to execute {args[0]}: {un_exc}")\
                    from un_exc
                raise CommandExecutionError(
                        f"'sudo' method failed with the error: {un_exc}") from un_exc
            except (CommandTimedOut, TimeoutError) as tout_exc:
                raise CommandTimeout(
                        f"Command Timed Out: {tout_exc}") from tout_exc
            except SSHException as conn_exc:
                raise SSHConnectError(
                        f"Unable to connect to {self.oob_address}: {conn_exc}") from conn_exc
            finally:
                conn.close()

        logger.debug(f"\n   {self.format_output(r_obj)}")
        return self.format_output(r_obj)


    def run(self, *args, **kwargs):
        """
        remote bash command
        """
        kwargs["hide"] = False
        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                r_obj = conn.run(*args, **kwargs)
            except UnexpectedExit as un_exc:
                if args:
                    raise CommandExecutionError(
                            f"Got the following error trying to execute {args[0]}: {un_exc}")\
                            from un_exc
                raise CommandExecutionError(
                        f"'run' method failed with the error: {un_exc}") from un_exc
            except (CommandTimedOut, TimeoutError) as tout_exc:
                raise CommandTimeout(
                        f"Command Timed Out: {tout_exc}") from tout_exc
            except SSHException as conn_exc:
                raise SSHConnectError(
                        f"Unable to connect to {self.oob_address}: {conn_exc}") from conn_exc
            finally:
                conn.close()
        logger.debug(f"\n   {self.format_output(r_obj)}")
        return self.format_output(r_obj)


    def put(self, src_file, dst_file, final_remote_file, **kwargs):
        """
        Copy file from the test server to the remote node
        """
        outside_container = kwargs.pop("outside_container", False)
        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                Transfer(conn).put(src_file, dst_file, **kwargs)
            except (FileNotFoundError, OSError) as no_file:
                raise CommandExecutionError(
                        f"File not found error {no_file}") from no_file
            except UnexpectedExit as un_exc:
                raise CommandExecutionError(
                        f"File transfer failed, {dst_file}: {un_exc}") from un_exc
            finally:
                conn.close()

        if final_remote_file:
            cmd = f"cp {dst_file} {final_remote_file}"
            self.sudo(cmd, outside_container = outside_container)


    def get(self, src_file, dst_file, **kwargs):
        """
        Copy file from the remote node to the test server
        """
        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                Transfer(conn).get(src_file, dst_file, **kwargs)
            except (FileNotFoundError, OSError) as no_file:
                raise CommandExecutionError(
                        f"File not found error {no_file}") from no_file
            except UnexpectedExit as un_exc:
                raise CommandExecutionError(
                        f"File transfer failed, {src_file}: {un_exc}") from un_exc
            finally:
                conn.close()

    def remove_path(self, path, **kwargs):
        """
        Remove a file or directory from the remote node
        """
        force = kwargs.pop("force", True)
        recursive = kwargs.pop("recursive", False)

        rm_flags = "-f" if force else ""
        rm_flags += " -r" if recursive else ""

        with Connection(self.node_string,
                connect_kwargs=self.connect_kwargs, config=self.config) as conn:
            try:
                conn.sudo(f"rm {rm_flags} {path}", **kwargs)
            except UnexpectedExit as un_exc:
                raise CommandExecutionError(
                    f"Failed to remove {path}: {un_exc}") from un_exc
            finally:
                conn.close()


class LocalNode:
    """
    Script is being run on this device i.e. script has local bash access
    """
    def __init__(self, **kwargs):
        pass


    def format_output(self, output):
        """
        Format command output and stash metadata
        """
        filtered_out = AttrStr(output.stdout.rstrip("\n"))
        setattr(filtered_out, "return_code", output.return_code)
        setattr(filtered_out, "failed", output.failed)
        setattr(filtered_out, "succeeded", output.ok)
        setattr(filtered_out, "command", output.command)
        setattr(filtered_out, "stderr", output.stderr)

        return filtered_out


    def sudo(self, command, **kwargs):
        """
        local sudo-bash command
        """
        kwargs["hide"] = False
        kwargs["pty"] = False
        output = self.format_output(sudo(command, **kwargs))
        logger.debug(output)
        return output


    def run(self, command, **kwargs):
        """
        local bash command
        """
        kwargs["hide"] = False
        output = self.format_output(run(command, **kwargs))
        logger.debug(output)
        return output

    def check_file_exists(self, file_path):
        res = False
        try:
            res = os.path.exists(file_path)
        except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logger.error(message)
        if res:
            return True
        else:
            return False

    def remove_file(self, file_path):
        try:
            os.remove(file_path)
        except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logger.error(message)

    def remove_dir(self, path):
        try:
            self.sudo(f"rm -rf {path}")
        except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logger.error(message)

    def create_dir(self, path):
        try:
            os.makedirs(path)
        except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logger.error(message)

    def expand_user(self):
        path = ""
        try:
            path = os.path.expanduser("~")
        except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logger.error(message)
        return path

    def list_dir(self, path):
        dir_list = []
        try:
            dir_list = os.listdir(path)
        except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                logger.error(message)
        return dir_list

    def read_file(self, file_path):
        logs = ""
        with open(file_path, 'r') as file:
            logs = file.readlines()
        return logs

class NetDev:
    """
    Properties of the host device used for the test
    """
    _INTERFACE_DIR = "/sys/class/net/"
    _INTERFACE_SPEED_DIR = "speed"
    _INTERFACE_HW_ADDR_DIR = "address"
    _IP_ADDR_PREFIX = "172.16"
    _IP_ADDR_NETMASK = "24"

    def __init__(self, node, mlxdev):
        self.node = node
        self.mlxdev = mlxdev
        self.mlxdev_num = int(mlxdev.split("_")[1])
        self.name = None
        self.speed = None
        self.hwaddress = None
        self.ipv4_addrs = []
        self.test_server_files_dir =  f"{constants.TestsPaths.FILES_DIR.value}/host_{self.node.idx}/"
        # find the device and pull the properties
        self._get_netdev()

    def __str__(self):
        output = "\n"
        output += f"{self.name} {self.mlxdev} speed {self.speed} mac {self.hwaddress}\n"
        output += "    ipv4"
        for addr in self.ipv4_addrs:
            output += f" {str(addr.ip)}"
        output += "\n"
        return output

    def _parse_netdev_inet(self):
        output = self.node.sudo(f"ip addr show {self.name}")
        addr_pat = re.compile(r"inet (?P<addr>\S+)")
        return addr_pat.findall(output)

    def _is_link_up(self):
        """
        Returns True if link is operationally up
        """
        logger.debug(f"Enable device {self.name}")
        output = self.node.sudo(f"ip link show {self.name}")
        return "LOWER_UP" in output

    def _wait_for_link_up(self):
        """
        Waits for link up; backing off and checking carrier status
        periodically
        """
        attempts = 3
        while attempts:
            attempts = attempts - 1
            if self._is_link_up():
                return
            time.sleep(10)

    def _get_netdev(self):
        output = self.node.sudo("ibdev2netdev")
        dev_pat = re.compile(fr"{self.mlxdev} port 1 ==> (?P<name>\S+)")
        obj = dev_pat.search(output)
        if obj:
            self.name = obj.group("name")

        if not self.name:
            return

        # Admin up the netdev
        self.node.sudo(f"ip link set {self.name} up")
        self._wait_for_link_up()

        # speed
        output = self.node.sudo(
                f"cat {self._INTERFACE_DIR}{self.name}/{self._INTERFACE_SPEED_DIR}")
        self.speed = int(output)

        # mac
        output = self.node.sudo(
                f"cat {self._INTERFACE_DIR}{self.name}/{self._INTERFACE_HW_ADDR_DIR}")
        self.hwaddress = output

        # IPv4 addresses
        addrs = self._parse_netdev_inet()

        for addr in addrs:
            self.ipv4_addrs.append(ipaddress.IPv4Interface(addr))


class HostNode(RemoteNode):
    """
    Server hosting the DPU
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.oob_address = kwargs.pop("oob_address", None)
        self.uplink_networks = kwargs.pop("uplink_networks", None)
        self.mlxdevs = kwargs.pop("devs", None)
        self.config_ports = kwargs.pop("config_ports", True)
        self.topology = kwargs.pop("topology", None)
        self.pcie_address = kwargs.pop("pcie_address", None)
        self.link_type = kwargs.pop("link_type", None)
        self.nic_mode = kwargs.pop("nic_mode", None)
        dpus_data = kwargs.pop("dpus_data", None)
        self.test_server_files_dir =  f"{constants.TestsPaths.FILES_DIR.value}/host_{self.idx}/"
        self.pf_names = self._get_net_dev_names()
        self.was_nvidia_peermem_loaded = True
        self.dpus = {}
        if self.config_ports:
            # Sanity check for uplink_network list (In case of ETH)
            if self.link_type == constants.LinkLayerType.ETHERNET.value:
                if self.uplink_networks:
                    if len(self.mlxdevs) != len(self.uplink_networks):
                        raise Exception("Number of devices and uplink_network doesn't match, please review your inventory file")
                else:
                    raise Exception("Link type is ETH, please add uplink_networks in inventory file")
            # Ports configuration
            self.host_config = ports_configuration.HostConfig(self, self.link_type, self.topology)
            self.host_config.configure_all_ports()
        self.netdevs = []
        [self.netdevs.append(NetDev(self, mlxdev)) for mlxdev in self.mlxdevs]
        self.__create_dpus(dpus_data, self.nic_mode)

    def __str__(self):
        return f"{self.oob_address} {','.join([netdev.name for netdev in self.netdevs])}"

    def _get_net_dev_names(self):
        """
        get all net_dev's names by mlx_dev
        """
        pf_names = []
        output = self.sudo("ibdev2netdev")
        for mlxdev in self.mlxdevs:
            dev_pat = re.compile(fr"{mlxdev} port 1 ==> (?P<name>\S+)")
            obj = dev_pat.search(output)
            if obj:
                pf_names.append(obj.group("name"))
            else:
                logger.error(f"No matching net_dev for mlxdev: {mlxdev}")
                # constants.ExitStatus.NVNETPERF_FAIL.value
                raise Exception(f"No matching net_dev for mlxdev: {mlxdev}")
        return pf_names

    def __create_dpus(self, dpus_data, nic_mode=False):
        """
        Instantiate DPUs
        """
        if dpus_data and not nic_mode:
            for dpu_id in dpus_data.keys():
                logger.info(f"Adding DPU {dpu_id}")
                dpu_data = dpus_data[dpu_id]
                oob_address = dpu_data.get("oob_address")
                uname = dpu_data.get("user")
                password = dpu_data.get("password")
                self.dpus[dpu_id] = DpuNode(oob_address=oob_address,
                        uname=uname, password=password)

    def all_dpus_trusted_mode(self):
        """
        Checks if all DPUs are in trusted mode (none of the DPUs are in zero-trust mode)
        """
        for pcie in self.pcie_address:
            card = self.sudo(f"mst status -v | grep {pcie} | awk '{{print $1}}'").strip()
            if "bluefield" in card.lower():
                level = self.sudo(f"mlxprivhost -d {pcie} q |grep level")
                level_result = level.split(":")[1].strip()
                if level_result.lower() == "restricted":
                    return False
        return True

    def check_gpus_available(self):
        """
        Check if there's a GPU hosted by the server
        """
        cmd = "lspci -nn | grep -i 10de"
        output = self.sudo(cmd, warn=True, hide="stderr")
        if output == "":
            logger.critical(f"No GPU available on host {self.oob_address}, cannot run this test")
            return False
        return True

    def get_link_layer(self, pci):
        """
        Get the link type of a given PCIe (IB or ETH)
        """
        mst_dev = self.sudo(f"mst status -v | grep {pci} | awk '{{print $2}}'").strip()
        link_layer = self.sudo(f"mlxconfig -d {mst_dev} q | grep -i 'link_type'").strip().split(" ")[-1]
        return link_layer

    def get_net_device(self, pci):
        """
        Get the network device of a given PCIe
        """
        device = self.sudo(f"mst status -v | grep -i {pci} | awk '{{print $5}}'").strip().split("-")[1]
        return device

    def set_mtu(self):
        """
        Set MTU on the ports to 4200
        """
        for port in self.pf_names:
           self.sudo(f"ifconfig {port} mtu 4200")

    def _is_nvidia_peermem_loaded(self):
        """
        Checks if the nvidia_peermem module is loaded
        """
        output = self.sudo("lsmod | grep nvidia_peermem")
        if output.return_code:
            self.was_nvidia_peermem_loaded = False
            return False
        return True

    def load_nvidia_peermem(self):
        """
        Loads the nvidia_peermem module if it is not already loaded (for GPU-direct usage)
        """
        if not self._is_nvidia_peermem_loaded():
            self.sudo("modprobe nvidia-peermem")

    def restore_nvidia_peermem(self):
        """
        Unloads the nvidia_peermem module if it was not originally loaded
        """
        if not self.was_nvidia_peermem_loaded:
            self.sudo("modprobe -r nvidia-peermem")

class DpuNode(RemoteNode):
    """
    DPU (Arm subsystem)
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.mstdev = self._get_mstdev()
        self.privileged = self._is_dpu_level_privileged()
        # This is bit of a hack that simply identifies the number of mlx devices
        out = super().sudo("mst status -v | grep BlueField")
        self.num_uplinks = len(out.splitlines())
        # uplink port being tested
        self.uplink_port = kwargs.pop("uplink_port", 0)

    def _get_mstdev(self):
        """
        Find mst device name on the DPU
        """
        super().sudo("mst start")
        return super().sudo("find /dev/mst/ | grep -G 'pciconf0$'")


    def _is_dpu_level_privileged(self):
        """
        Check the DPU level
        """
        level = super().sudo(f"mlxprivhost -d {self.mstdev} q |grep level")
        priv = False
        level_result = level.split(":")[1].strip()
        if level_result == "PRIVILEGED":
            priv = True

        logger.info("DPU is in %s mode", "privileged" if priv else "restricted")
        return priv


    def set_mtu_on_uplinks(self):
        """
        Locate all the uplinks on the dpu and change their mtu to 9000
        """
        for i in range(self.num_uplinks):
            uplink = f"p{i}"
            cmd = f"ip link set {uplink} mtu 9216"
            logger.debug(f"Running {cmd}")
            super().sudo(cmd)

            host_rep = f"pf{i}hpf"
            cmd = f"ip link set {host_rep} mtu 9216"
            logger.debug(f"Running {cmd}")
            super().sudo(cmd)


    def _parse_crictl_ps_(self):
        """
        This method finds the mapping from to container name to container id.
        It returns a dictionary where the keys are the container names and
        the values are the container ids
        """
        data = {}
        output = super().sudo("crictl ps")
        lines = output.splitlines()
        container_idx = 0
        name_idx = 0
        len_check = 8
        for line in lines:
            info = [ e.replace("_", "").lstrip() for e in line.split("  ")  if e ]
            if info and re.search("[A-Z]+", info[0]):
                for i, eobj in enumerate(info):
                    if eobj == "CONTAINER":
                        container_idx = i
                        len_check = len(info)
                    elif eobj == "NAME":
                        name_idx = i
            else:
                if info and (len(info) == len_check):
                    data[info[name_idx]] = info[container_idx]
                elif info and (len(info) != 8 ):
                    raise NodeError("Unknown format from \"crictl\"")

        return data


    def get_container_id(self, container_name):
        """
        Maps a container name to container id
        """
        container_id = None
        containers = self._parse_crictl_ps_()

        cnames = list(containers.keys())
        for k in cnames:
            if container_name in k:
                container_id = containers.get(k)
                break

        return container_id


class DpuContainer(DpuNode):
    """
    Service container running on the DPU e.g. HBN
    """
    def __init__(self, container_name, **kwargs):
        super().__init__(**kwargs)
        self._container_id = None
        self.container_name = container_name
        self.dpu_files_dir = f"{constants.TestsPaths.FILES_DIR.value}/" + container_name
        super().run(f"mkdir -p {self.dpu_files_dir}")
        logger.debug(f"Created directory for storing files {self.dpu_files_dir}")


    @property
    def container_id(self):
        """
        Fetch the container id using the container name
        """
        if not self._container_id:
            self._container_id = self.get_container_id(self.container_name)

        return self._container_id


    def sudo(self, *args, **kwargs):
        """
        These commands are run within a container and need to be wrapped
        with crictl.
        """
        if "outside_container" in kwargs:
            run_outside_cont = kwargs.pop("outside_container")
            if run_outside_cont:
                return super().sudo(*args, **kwargs)

        return super().sudo(
                f"crictl exec {self.container_id} bash -c \"{args[0]}\"", **kwargs)


class NodeGroup():
    """
    Node group represents a subsystem of the whole setup, it includes 1 or 2 hosts,
    and may contains any number of NICs: DPUs or CXs
    """
    def __init__(self, **kwargs):
        self.idx = kwargs.pop("idx", 0)
        self.topology = kwargs.pop("topology", None)
        config_ports = kwargs.pop("config_ports", True)
        hosts_data = kwargs.pop("hosts_data", None)
        self.hosts = {}
        self.__create_hosts(hosts_data, config_ports, self.topology)

    def __str__(self):
        return f"Node group #{self.idx}, hosts:{','.join([host.oob_address for host in self.hosts])}"


    def __create_hosts(self, hosts_data, config_ports, topology):
        for i, host in enumerate(hosts_data.keys()):
            host_data = hosts_data[host]
            oob_address = host_data.get("host_ip")
            username = host_data.get("user")
            password = host_data.get("password")
            dpus_data = host_data.get("dpus", None)
            uplink_networks = host_data.get("uplink_network", None)
            mlx_devs = host_data.get("mlx_dev")
            link_type = host_data.get("link_type")
            nic_mode = host_data.get("nic_mode")
            pcie_address = host_data.get("pcie_bdf_address", None).split(",")
            pcie_address = [pcie_add.strip() for pcie_add in pcie_address]

            # If mlx_dev has duplications, we can't proceed
            if len(mlx_devs) != len(set(mlx_devs)):
                raise Exception(f"PCIe list of {oob_address} has duplications, please review your inventory file")

            self.hosts[host] = HostNode(idx=i, oob_address=oob_address, devs=mlx_devs, uname=username,
                                        password=password, uplink_networks=uplink_networks, pcie_address=pcie_address,
                                        config_ports=config_ports, topology=topology, dpus_data=dpus_data,
                                        link_type=link_type, nic_mode=nic_mode)
