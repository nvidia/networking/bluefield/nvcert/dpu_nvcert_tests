#!/usr/bin/env python3

#
# SPDX-FileCopyrightText: NVIDIA CORPORATION & AFFILIATES
# Copyright (c) 2023-2025 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: LicenseRef-NvidiaProprietary
#
# NVIDIA CORPORATION, its affiliates and licensors retain all intellectual
# property and proprietary rights in and to this material, related
# documentation and any modifications thereto. Any use, reproduction,
# disclosure or distribution of this material and related documentation
# without an express license agreement from NVIDIA CORPORATION or
# its affiliates is strictly prohibited.
#

# pylint: disable=too-few-public-methods
# pylint: disable=too-many-function-args
# pylint: disable=too-many-arguments

"""
1. This modules identifies the network devices on the host associated with
the DPU.
2. The DPU OOB address is used for this purpose.
"""

import sys
from common import constants
sys.path.insert(0, constants.NVNETPERF_ROOT + "/")

try:
    import logging
    import yaml
    from common.utils import node
    import json
    import jsonschema
    from jsonschema import validate
except ImportError as e:
    raise ImportError (str(e) + "- import module missing") from e

logger = logging.getLogger("nvnetperf")


def populate_net_dev(host_data, host):
    """
    Translates the uplink port number to the mlx_dev name
    """
    # user must specify the PCIEs
    pcies = host_data.get("pcie_bdf_address", None)
    if pcies:
        host_data["mlx_dev"] = []
        host_pcies = pcies.split(",")
        for id, pcie in enumerate(host_pcies, start=0):
            cmd = f"mst status -v | grep {pcie.strip()} | awk '{{print $1 \" \" $4}}'"
            output = host.sudo(cmd)
            card = output.split()[0].strip()
            if "BlueField3" not in card and "ConnectX" not in card:
                logger.critical(f"PCIe {pcie} is not connected to Bluefield3 or a ConnectX")
                sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)
            mlx_dev = output.split()[1].strip()
            host_data["mlx_dev"].append(mlx_dev)
            logger.info(f"host mlx_dev_{id} {mlx_dev}")
        return
    else:
        logger.critical("No PCIEs found, please specify PCIes of the cards and rerun")
        sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)


def setup_host(host_data):
    """
    Instantiate the host class for running commands on the host
    """
    oob_address = host_data.get("host_ip")
    uname = host_data.get("user")
    password = host_data.get("password")

    host = node.RemoteNode(oob_address=oob_address, uname=uname, password=password)
    return host


def check_nic_mode(host, host_data):
    """
    Check if the DPUs of the given host are in NIC mode / the cards are CX
    """
    pcies = host_data.get("pcie_bdf_address", None)
    if pcies:
        host.sudo("mst start")
        p0_pcie = pcies.split(",")[0].strip()
        card = host.sudo(f"mst status -v | grep {p0_pcie} | awk '{{print $1}}'").strip()
        if "ConnectX" in card:
            logger.info("Identified card as CX")
            return True
        mlx_dev = host.sudo(f"mst status -v | grep {p0_pcie} | awk '{{print $4}}'").strip()
        if mlx_dev:
            mst_dev = host.sudo(f"mst status -v | grep {mlx_dev}").split()[1].strip()
            res = host.sudo(f"mlxconfig -d {mst_dev} q INTERNAL_CPU_OFFLOAD_ENGINE | grep INTERNAL_CPU_OFFLOAD_ENGINE")
            if res:
                res = res.split()[1].strip()
                logger.info("Identified card as Bluefield3")
                return (res == "DISABLED(1)")
    else:
        logger.critical("No PCIes provided in inventory file, please fix")
        sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)


def copy_uplink_from_host_to_dpu(host_data, dpu_data):
    """
    Copy the zero based uplink port id from host to DPU
    """
    uplink_port = host_data.get("uplink_port", 0)
    dpu_data["uplink_port"] = uplink_port


def get_link_type(host, host_data):
    """
    Check the link type of the given host - IB or ETH
    """
    pcies = host_data.get("pcie_bdf_address", None)
    oob_address = host_data.get("host_ip")
    if not pcies:
        logger.critical("No PCIes provided in inventory file, please fix")
        sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)


    host.sudo("mst start")
    pcies = pcies.split(",")
    link_types = set()
    for pci in pcies:
        output = host.sudo(f"mlxconfig -d {pci} q LINK_TYPE_P1 | grep LINK_TYPE_P1").strip().split(" ")[-1]
        if output:
            link_type = output.split()[-1]
            logger.info(f"Link type of {oob_address}, {pci} is {link_type}")
            link_types.add(link_type)

    if len(link_types) > 1:
        logger.critical(f"Mismatch in link types across PCIes on Host-{oob_address}. Link types found: " + ", ".join(link_types))
        sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)

    # If all link types are the same, return the type
    link_type = link_types.pop()
    for link_layer_type in constants.LinkLayerType:
        if link_layer_type.value in link_type:
            return link_layer_type.value
    
    logger.critical(f"Unknown link type detected on Host-{oob_address}: {link_type}")
    sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)


def validate_inventory_file(data, root_dir):
    """
    Validate the given inventory file against the schema
    """
    schema_path = f'{root_dir}/common/templates/inventory_schema.json'
    with open(schema_path, 'r') as schema_file:
        schema = json.load(schema_file)
    try:
        validate(instance=data, schema=schema)
        logger.info("Inventory file validated successfully")
    except jsonschema.exceptions.ValidationError as ve:
        logger.error(f"Inventory file validation error: {ve}")
        sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)
    # Print the detailed validation error
    except jsonschema.exceptions.SchemaError as se:
        logger.error(f"Schema error: {se}")
        sys.exit(constants.ExitStatus.NVNETPERF_PARAM_ERROR.value)


def validate_test_setup(inventory, root_dir):
    """
    Parse the inventory file and check if all the nodes are accessible from
    the test-server
    """
    # Load the YAML file
    with open(inventory, 'r') as yaml_file:
        try:
            data = yaml.safe_load(yaml_file)
        except yaml.YAMLError as e:
            logger.error(f"Error parsing inventory file: {e}")
            sys.exit(1)
    validate_inventory_file(data, root_dir)

    node_group_data = data['system']

    for node_group_k in node_group_data.keys():
        # Check if node group is in NIC mode (or has CX)
        for host_key in node_group_data[node_group_k]['hosts'].keys():
            host_data = node_group_data[node_group_k]['hosts'].get(host_key)
            host = setup_host(host_data)

            # Check host's link type and dpu/nic mode
            link_type = get_link_type(host, host_data)
            node_group_data[node_group_k]['hosts'][host_key]['link_type'] = link_type
            nic_mode = check_nic_mode(host, host_data)
            node_group_data[node_group_k]['hosts'][host_key]['nic_mode'] = nic_mode

            if not nic_mode:
                for dpu_data in host_data['dpus'].values():
                    # Copy uplink port from host to DPU
                    copy_uplink_from_host_to_dpu(host_data, dpu_data)

        for host_data in node_group_data[node_group_k]['hosts'].values():
            host = setup_host(host_data)
            # Populate network devices for running the tests
            populate_net_dev(host_data, host)

    logger.info(data)

    return data
