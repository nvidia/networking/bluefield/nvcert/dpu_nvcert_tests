# -*- mode: python ; coding: utf-8 -*-

import os
import pkg_resources
import site
import yaml
import glob
import sysconfig

block_cipher = None
added_files = [
    ('./common/.', './common'),
    ('./ngc_multinode_perf/.', './ngc_multinode_perf'),
    ('./tests/.', './tests'),
    ('requirements.txt', '.'),
    ]

# Include PyYAML C extensions
yaml_c_extensions = glob.glob(os.path.join(os.path.dirname(yaml.__file__), '_yaml*.so'))
for ext in yaml_c_extensions:
    added_files.append((ext, 'yaml/'))

packages = ['tabulate', 'fabric2', 'decorator', 'Mako', 'requests', 'six', 'pyrsistent', 'jsonschema', 'mako', 'invoke', 'pyyaml', 'paramiko', 'pkg_resources', 'prettytable', 'multiprocessing', 'PIL._tkinter_finder', 'json', 'cryptography' ]
packages_locations = site.getsitepackages()
packages_locations.append(sysconfig.get_path("purelib"))
packages_locations.append(sysconfig.get_path("platlib"))

for requirement in packages:
    package_name = requirement
    for loc in packages_locations:
        package_path = os.path.join(loc, package_name)
        if os.path.exists(package_path):
            added_files.append((package_path, package_name))


a = Analysis(['nvnetperf.py'],
             pathex=[],
             binaries=[],
             datas=added_files,
             hiddenimports=packages,
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False,
             python='/usr/local/bin/python3')

a.binaries = [x for x in a.binaries if not x[0].startswith('libselinux')]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='nvnetperf',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )